<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {

    return view('index');
});

$router->get('testing','Controller@testing');

/**
 * Routes for resource search-controller
 */
$router->group(['prefix' => 'api/v1'], function($ui)
{
    $ui->get('keyword','HomeController@getKeywordBySearchKey');
    $ui->get('count/GetCount','HomeController@getCount');
    $ui->get('keyword/FindContractor','HomeController@FindContractor');
    $ui->get('categories/{id}','HomeController@getCategoryById');
    $ui->get('certificates','HomeController@certificates');
    $ui->post('Location','HomeController@location');
    $ui->get('userservice/getLoggedInUser','UserController@getLoggedInUser');
    $ui->get('topServices','HomeController@getTopService');
    $ui->post('imageservice','HomeController@ImageUpload');
    $ui->post('imagesave','HomeController@ImageSave');
    $ui->get('getContractors','HomeController@getContractors');
    $ui->get('getBuyers','HomeController@getBuyers');
    $ui->get('getTopCompanies','HomeController@getTopCompanies');

    $ui->get('mail','HomeController@mail');

    $ui->get('/','Controller@abcd');

    $ui->get('userservice/ConfirmEmail','UserController@ConfirmEmail');
    $ui->get('userservice/ForgotPassword','UserController@sendResetLink');
    $ui->post('userservice/ResetPassword','UserController@resetPassword');

    /* LOGIN CONTROLLER */

    $ui->group(['prefix' => 'login'], function($log){

        $log->post('check','UserController@LoginCheck');
        $log->get('{uId}','UserController@LoginById');
        $log->post('register','UserController@Register');
        $log->get('contractors/GetContractorProfile','UserController@GetContractorProfile');

    });


    $ui->get('contractors/{uId}','ContractorsController@contractors');

    $ui->group(['prefix' => 'contractor'],function($con){

        $con->get('facilities','ContractorsController@getFacilities');
        $con->get('AffAssociation','ContractorsController@Affassociation');
        $con->get('package','ContractorsController@packages');
        $con->get('keyword/GetKeywordsPrice','ContractorsController@GetKeywordsPrice');
        $con->post('saveData','ContractorsController@ContractorSaveData');
        $con->post('AddContractorCategoryKeywords','ContractorsController@AddContractorCategoryKeywords');
        $con->post('addContractorPackage','ContractorsController@addContractorPackage');
        $con->post('userservice/edit','ContractorsController@userEdit');
        $con->get('getContractorProgress/{id}','ContractorsController@getContractorProgress');
        $con->post('changePassword','ContractorsController@changePassword');
        $con->post('ContractorListingLog','ContractorsController@contractorLog');
        $con->get('Count/GetTrendingKeyword','ContractorsController@trendingKeywordsData');

        $con->get('getrating','ContractorsController@getContractorRatings');
        $con->post('rating','ContractorsController@saveContractorRatings');

        $con->get('GetTopContractorRating','ContractorsController@getTopRatings');

        // $con->post('contractorRating/ReviewRemainder','ContractorsController@sendReviewMail');
    });

    $ui->group(['prefix' => 'buyer'],function($buyer){

        $buyer->get('buyerinformation/{id}','BuyersController@buyerinformation');
        $buyer->post('buyerinformation','BuyersController@updateInfo');
        $buyer->post('ContactMeInfo','BuyersController@contactme');
        $buyer->get('searchBuyer','BuyersController@searchBuyer');
        $buyer->post('sendRatingMail','BuyersController@sendMails');
        $buyer->post('ReviewRemainder','BuyersController@sendReviewMail');


    });

    $ui->group(['prefix' => 'admin'], function($admin){

        $admin->get('categories','AdminsController@categories');
        $admin->post('categories/Edit','AdminsController@CategoryEdit');
        $admin->post('categories/save','AdminsController@CategorySave');
        $admin->get('categories/Delete','AdminsController@CategoryDelete');

        $admin->get('getServices','AdminsController@getServices');
        $admin->get('setTopService','AdminsController@setTopService');
        $admin->get('getTopContractors','AdminsController@getTopContractors');
        $admin->get('setTopContractor','AdminsController@setTopContractor');


        $admin->get('keyword','AdminsController@keyword');
        $admin->post('keyword/save','AdminsController@KeywordSave');
        $admin->post('keyword/Edit','AdminsController@KeywordEdit');
        $admin->get('keyword/Delete','AdminsController@KeywordDelete');

        $admin->post('facility/save','AdminsController@FacilitySave');
        $admin->post('facility/edit','AdminsController@FacilityEdit');
        $admin->post('facility/delete','AdminsController@FacilityDelete');

        $admin->post('AffAssociation/save','AdminsController@AffAssociationSave');
        $admin->post('AffAssociation/edit','AdminsController@AffAssociationEdit');
        $admin->get('AffAssociation/delete','AdminsController@AffAssociationDelete');

        $admin->get('PriceModel','AdminsController@PriceModel');
        $admin->post('PriceModel','AdminsController@PriceModelSave');

        $admin->get('keyword/GetKeywordAdmin','AdminsController@GetKeywordAdmin');
        $admin->get('setBasePrice','AdminsController@setBasePrice');

        $admin->get('count/TopContactedContractor','AdminsController@TopContactedContractor');
        $admin->get('count/CountrywiseWebsiteHits','AdminsController@getCountrywiseWebsiteHits');
        $admin->get('count/PendingReviewList','AdminsController@PendingReviewList');
        
        $admin->get('userservice/UserCountCountrywise','AdminsController@UserCountCountrywise');

        $admin->get('keyword/FindBuyer','AdminsController@FindBuyer');

        $admin->get('getVideo','AdminsController@video');
        $admin->get('video','AdminsController@videoUpload');

        $admin->post('package','AdminsController@savePackage');
        $admin->post('package/edit','AdminsController@EditPackage');
        $admin->post('package/delete','AdminsController@deletePackage');
    });

    
    // $ui-group(['prefix' => 'certificate'], function($certi){
        $ui->post('uploadSliderImage/{name}/{ext}/{id}','ImagesController@uploadSlideImage');
        $ui->post('uploadCertificate/{name}/{id}','ImagesController@uploadCerti');
        
        $ui->post('editCertificate','ImagesController@editCerti');
        $ui->post('editSliderImage','ImagesController@editSlide');
        
        $ui->post('deleteCertificate','ImagesController@deleteCerti');
        $ui->post('deleteSliderImage','ImagesController@deleteSlide');
        
        $ui->post('addCertificate','ImagesController@addCerti');
        $ui->post('addSliderImage','ImagesController@addSlide');

        $ui->get('sliderimages','ImagesController@getAllImages');
    // });    
        


});


<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<table class='MsoTableGrid' style='border-collapse: collapse; border: none; mso-border-alt: dashed #00B050 1.5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: 1.5pt dashed #00B050; mso-border-insidev: 1.5pt dashed #00B050;' border='1' cellspacing='0' cellpadding='0'>
<tbody>
<tr style='mso-yfti-irow: 0; mso-yfti-firstrow: yes; mso-yfti-lastrow: yes;'>
<td style='width: 478.8pt; border: dashed #00B050 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt;' valign='top' width='638'>
<p class='MsoNormal' style='margin-bottom: 6.0pt; line-height: 144%;'><span style='font-size: 12.0pt; line-height: 144%; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #6aa84f; background: white; mso-highlight: white;'>&nbsp;</span></p>
<p class='MsoNormal' style='margin-bottom: 17.0pt; line-height: 144%;'><span style='font-size: 12.0pt; line-height: 144%; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #6aa84f; background: white; mso-highlight: white;'>Hello, <?= $bfname; ?> <?= $blname; ?>,</span></p>
<p class='MsoNormal' style='line-height: normal;'><span style='font-size: 13.5pt; color: #3d4145; background: white; mso-highlight: white;'>&nbsp;</span></p>
<p class='MsoNormal' style='text-align: center; line-height: normal;' align='center'><span style='font-size: 13.5pt; color: #3d4145; background: white; mso-highlight: white;'>We are dedicated to improve the troublesome process of getting the right contractor for your requirement. </span></p>
<p class='MsoNormal' style='text-align: center; line-height: normal;' align='center'><span style='font-size: 13.5pt; color: #3d4145; background: white; mso-highlight: white;'>&nbsp;</span></p>
<p class='MsoNormal' style='text-align: center; line-height: normal;' align='center'><span style='font-size: 13.5pt; color: #3d4145; background: white; mso-highlight: white;'>We have received your request to contact <?= $cfname; ?> <?= $clname; ?></span><span style='font-size: 13.5pt; color: #6aa84f; background: white; mso-highlight: white;'>.</span></p>
<p class='MsoNormal' style='text-align: center; line-height: normal;' align='center'><strong style='mso-bidi-font-weight: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #6aa84f; background: white; mso-highlight: white;'>&nbsp;</span></strong></p>
<p class='MsoNormal' style='text-align: center; line-height: normal;' align='center'><strong style='mso-bidi-font-weight: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #6aa84f; background: white; mso-highlight: white;'>&nbsp;</span></strong></p>
<p class='MsoNormal' style='text-align: center; line-height: normal;' align='center'><strong style='mso-bidi-font-weight: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #6aa84f; background: white; mso-highlight: white;'>An executive from our experience team will give you a call shortly to understand your requirement and assist you further to fulfil all your outsourcing needs.</span></strong></p>
<p class='MsoNormal' style='line-height: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #2a2e2e;'>&nbsp;</span></p>
<p class='MsoNormal' style='line-height: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #3d4145; background: white; mso-highlight: white;'>&nbsp;</span></p>
<p class='MsoNormal' style='line-height: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #3d4145; background: white; mso-highlight: white;'>&nbsp;</span></p>
<p class='MsoNormal' style='line-height: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #3d4145; background: white; mso-highlight: white;'>&nbsp;</span></p>
<p class='MsoNormal' style='line-height: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #3d4145; background: white; mso-highlight: white;'>For any queries,</span></p>
<p class='MsoNormal' style='line-height: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #3d4145; background: white; mso-highlight: white;'>You can reach us instantly at </span><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #6aa84f; background: white; mso-highlight: white;'>+918779941985 / +918779938494</span></p>
<p class='MsoNormal' style='line-height: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #3d4145; background: white; mso-highlight: white;'>Thank you for your time,</span></p>
<p class='MsoNormal' style='line-height: normal;'><span style='font-size: 12.0pt; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #cc0000; background: white; mso-highlight: white;'>&nbsp;</span></p>
<p class='MsoNormal' style='margin-bottom: 17.0pt; line-height: 144%;'><span style='font-size: 12.0pt; line-height: 144%; font-family: Roboto; mso-fareast-font-family: Roboto; mso-bidi-font-family: Roboto; color: #6aa84f; background: white; mso-highlight: white;'>Experience Team</span></p>
<img src='http://www.outchem.com/assets/images/main-logo.png' alt='outchem'/>
</td>
</tr>
</tbody>
</table>


</body>
</html>
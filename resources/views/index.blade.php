<!doctype html>
<html>
<head>
  <base href="/">
  <meta charset="utf-8">
  <title>Outchem</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="icon" type="image/png" href="/assets/images/favicon.png" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!--<link href="assets/css/hover.css" rel="stylesheet" />-->
  <!-- build:css({.tmp,src}) styles/vendor.css -->
  <!-- bower:css -->
  <link rel="stylesheet" href="../bower_components/angular-material/angular-material.css" />
  <link rel="stylesheet" href="../bower_components/angular-toastr/dist/angular-toastr.css" />
  <link rel="stylesheet" href="../bower_components/nvd3/build/nv.d3.css" />
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" href="../bower_components/angularMultipleSelect/build/multiple-select.min.css">
  <!-- endbower -->
  <!-- endbuild -->

  <!-- build:css({.tmp,src}) styles/app.css -->
  <!-- inject:css -->
  <link rel="stylesheet" href="index.css">
  <link rel="stylesheet" href="assets/css/bootstrap-theme.css">
  <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="assets/css/bootstrap.css">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/custom.css">
  <link rel="stylesheet" href="assets/css/font-awesome.css">
  <link rel="stylesheet" href="assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/hover.css">
  <link rel="stylesheet" href="assets/css/jquerysctipttop.css">
  <link rel="stylesheet" href="assets/css/star-rating.css">
  <link rel="stylesheet" href="assets/css/ng-img-crop.css">
  <!-- endinject -->
  <!-- endbuild -->
  <script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>    
</head>


<body ng-app="app">
  <div ng-controller="HeaderController as headerController">    
    <div ng-include src="'./app/default-header.html'"></div>
  </div>
  <div ng-controller="MainCtrl as m" bs-loading-overlay>     
   <ui-view autoscroll="true"></ui-view>     
 </div>
 <div ng-controller="HeaderController as headerController">
  <div ng-include src="'./app/default-footer.html'"></div>
</div>
<div>
  <script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=56724964"></script>
</div>
</body>

<!-- build:js({.tmp,src}) scripts/vendor.js -->
<!-- bower:js -->
<script src="../bower_components/jquery/dist/jquery.js"></script>
<script src="../bower_components/angular/angular.js"></script>
<script src="../bower_components/angular-ui-router/release/angular-ui-router.js"></script>
<script src="../bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
<script src="../bower_components/angular-animate/angular-animate.js"></script>
<script src="../bower_components/angular-aria/angular-aria.js"></script>
<script src="../bower_components/angular-messages/angular-messages.js"></script>
<script src="../bower_components/angular-material/angular-material.js"></script>
<script src="../bower_components/angular-toastr/dist/angular-toastr.tpls.js"></script>
<script src="../bower_components/checklist-model/checklist-model.js"></script>
<script src="../bower_components/a0-angular-storage/dist/angular-storage.js"></script>
<script src="../bower_components/angular-loading-overlay/dist/angular-loading-overlay.js"></script>
<script src="../bower_components/d3/d3.js"></script>
<script src="../bower_components/nvd3/build/nv.d3.js"></script>
<script src="../bower_components/angular-nvd3/dist/angular-nvd3.js"></script>
<script src="../bower_components/moment/moment.js"></script>
<script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="../bower_components/angular-daterangepicker/js/angular-daterangepicker.js"></script>
<script src="../bower_components/ngstorage/ngStorage.js"></script>
<script src="../bower_components/topojson/topojson.js"></script>
<script src="../bower_components/datamaps/dist/datamaps.all.js"></script>
<script src="../bower_components/angular-datamaps/dist/angular-datamaps.min.js"></script>
<script src="../bower_components/angular-scroll/angular-scroll.js"></script>
{{--  <script src="../bower_components/angular-sanitize/angular-sanitize.js"></script>  --}}
<script src="../bower_components/angularMultipleSelect/build/multiple-select.min.js"></script>

<!-- endbower -->
<!-- endbuild -->


<!-- build:js({.tmp,src}) scripts/app.js -->
<!-- inject:js -->
<script src="index.js"></script>
<script src="assets/js/ng-img-crop.js"></script>
<script src="app/searchKeyword/searchKeywordController.js"></script>
<script src="app/resetPassword/resetPassword.js"></script>
<script src="app/misc/utils.js"></script>
<script src="app/login/login.js"></script>
<script src="app/home/homePageController.js"></script>
<script src="app/certificates/certificatesController.js"></script>
<script src="app/emailVerification/emailVerification.js"></script>
<script src="app/contractorRatings/contractorRatingsController.js"></script>
<script src="app/contractorProfile/viewContractorProfileController.js"></script>
<script src="app/contractorProfile/contractProfController.js"></script>
<script src="app/changePassword/changePasswordController.js"></script>
<script src="app/contractRegistration/contractRegistration.js"></script>
<script src="app/companyDashboard/CompanyDashboardController.js"></script>
<script src="app/buyerRegistration/buyerRegistration.js"></script>
<script src="app/buyerProfile/buyerProfileController.js"></script>
<script src="app/adminProfile/adminProfileController.js"></script>
<script src="app/adminDashboard/adminDashboardController.js"></script>
<script src="app/MultipleSelect/multiselect.js"></script>
<script src="app/headerController.js"></script>
<script src="app/footer.js"></script>
<script src="app/data.services.js"></script>
<script src="app/data.constants.js"></script>
<script src="user.services.js"></script>
<script src="routes.js"></script>
<script src="data.interceptor.js"></script>
<!-- endinject -->
<!-- inject:partials -->
<!-- angular templates will be automatically converted in js and inserted here -->
<!-- endinject -->
<!-- endbuild -->


</html>


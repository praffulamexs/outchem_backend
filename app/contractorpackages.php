<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class contractorpackages extends Model {

    protected $table = "contractorpackages";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

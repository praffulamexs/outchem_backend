<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class contactmeinfo extends Model {

    protected $table = "contactmeinfo";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

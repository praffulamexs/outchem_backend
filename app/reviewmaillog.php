<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class reviewmaillog extends Model {

    protected $table = "reviewmaillog";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

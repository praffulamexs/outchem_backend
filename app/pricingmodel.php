<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class pricingmodel extends Model {

    protected $table = "pricingmodel";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

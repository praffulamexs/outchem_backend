<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class keywordsearchlog extends Model {

    protected $table = "keywordsearchlog";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

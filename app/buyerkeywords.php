<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class buyerkeywords extends Model {

    protected $table = "buyerkeywords";

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;

    // Relationships

}

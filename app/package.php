<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class package extends Model {

    protected $table = "package";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

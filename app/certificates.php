<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class certificates extends Model {

    protected $table="certificates";

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

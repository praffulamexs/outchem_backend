<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class contractorcategorykeyword extends Model {

    protected $table = "contractorcategorykeyword";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

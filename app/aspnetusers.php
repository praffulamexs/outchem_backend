<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class aspnetusers extends Model {

    protected $table="aspnetusers";

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;

    // Relationships

}

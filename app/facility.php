<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class facility extends Model {

    protected $table = "facility";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

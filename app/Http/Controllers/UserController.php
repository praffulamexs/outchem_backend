<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\aspnetusers;
use App\contractorinformation;
use App\keyword;
use App\buyerinformation;
use App\contractorcategorykeyword;
use App\category;
use App\contractorpackages;
use Mail;
use Illuminate\Mail\Mailer;


class UserController extends Controller {




    public function LoginCheck(Request $request)
    {
        $username=$request['userId'];
        $password=$request['userPassword'];

        $user=aspnetusers::where('UserName',$username)->first();

        if($user)
        {
            if(Hash::check($password,$user->PasswordHash))
            {  
                if($user->EmailConfirmed == 1) 
                {
                    return $user->Id;
                }
                else{
                    return "Email Not Verified";
                }
            }   
            else
            {
                return "Invalid Password";
            }
        }
        else
        {
            return "Invalid Username";
        }
    }

    public function LoginById($id)
    {
        $user = aspnetusers::where('Id',$id)
        ->select('Id as id',
           'FirstName as firstName',
           'LastName as lastName',
           'Country as country',
           'City as city',
           'HasAcceptedTerms as hasAcceptedTerms',
           'IsContractor as isContractor',
           'Email as email',
           'EmailConfirmed as emailConfirmed',
           'PasswordHash as passwordHash',
           'SecurityStamp as securityStamp',
           'PhoneNumber as phone',
           'PhoneNumberConfirmed as phoneNumberConfirmed',
           'TwoFactorEnabled as twoFactorEnabled',
           'LockoutEndDateUtc as lockoutEndDateUtc',
           'LockoutEnabled as lockoutEnabled',
           'AccessFailedCount as accessFailedCount',
           'UserName as userName',
           'Image as image')
        ->first();
        if ($user->userName == "admin@outchem.com")
        {
            $user['isAdmin']=1;
            return $user;
        }
        else
        {
            return $user;
        }
    }

    public function Register(Request $request)
    {

        if(count(aspnetusers::where('Email',$request['email'])->get()))
        {
            return "Email Is Already Taken";
        }

        else
        {
            $city=$request['City'];
            $country=$request['Country'];
            $firstName=$request['FirstName'];
            $isContractor=$request['IsContractor'];
            $lastName=$request['LastName'];
            $password=$request['password'];
            $confPassword=$request['confPassword'];
            $email=$request['email'];
            $phone=$request['phone'];
            $company = $request['companyName'];
    
            $user = new aspnetusers;
            $user->City=$city;
            $user->Country=$country;
            $user->FirstName=$firstName;
            $user->IsContractor=$isContractor;
            $user->LastName=$lastName;
            $user->PasswordHash=Hash::make($password);
            $user->Email=$email;
            $user->SecurityStamp = 'blank';
            $user->UserName=$email;
            $user->PhoneNumber=$phone;
            $user->HasAcceptedTerms=1;
            $user->save();
    
            if($isContractor == 0){
                $buyer = new buyerinformation;
                $buyer->BuyerInformationId = $user->id;
                $buyer->CompanyName = $company;
                $buyer->save();
            }
            else {
                $contractor = new contractorinformation;
                $contractor->ContractorId = $user->id;
                $contractor->CompanyName = $company;
                $contractor->save();
            }
    
    
            $this->verifyEmail($firstName,$lastName,$user->id,$email);
    
            return "Registered";
        }
        
    }



    function verifyEmail($firstName,$lastName,$userId,$email)
    {
        // $base_url = "http://localhost:8010/#!";
        $base_url = "http://ec2-13-126-14-209.ap-south-1.compute.amazonaws.com/#!";


        $code = Hash::make($email); //generated random code using email

        aspnetusers::where('Id',$userId)->update([
            'SecurityStamp' => $code,
            ]);

        $confirmLink = $base_url.'emailVerification?userId='.$userId.'&code='.$code;
        // $confirmLink = $base_url+'emailVerification?userId='+$userId;

        $data=[];
        $data = array(
            'firstName' => $firstName,
            'lastName' => $lastName,
            'confirmLink' => $confirmLink,
            'email' => $email
            );
        Mail::send('emailConfirmTemplate', $data, function($message) use ($data)
        {   
            // dd("hello");
            $message->from('doNotReply@outchem.com', 'outchem');
            $message->to($data['email'])->bcc('raj.outchem@gmail.com')->subject('Confirm your account!!');
        });

        return "mail sent success";
        
    }

    public function ConfirmEmail(Request $request){


        $userId = $request['userId'];
        $code = $request['code'];
        $user = aspnetusers::where('Id',$userId)->first();

        if($user['EmailConfirmed'] == 1){
            return "confimed already";
        }
        else if($code == $user['SecurityStamp'])
        {

         aspnetusers::where('Id',$userId)->update([
            'EmailConfirmed' => 1,
            ]);  

         // return "Email Confirmed";
         $this->welcomeMail($userId); 
         $this->newUserAdminMail($userId);

         return "Email Confirmed";
     }
     else{
        return "code not matched";
    }


}

function welcomeMail($userId){

    $user = aspnetusers::where('Id',$userId)->first();

    $firstName = $user['FirstName'];
    $lastName = $user['LastName'];
    $email = $user['Email'];
    $isContractor = $user['IsContractor'];

    $data=[];
    $data = array(
        'firstName' => $firstName,
        'lastName' => $lastName,
        'email' => $email
        );
    if($isContractor == 1){
        Mail::send('welcomeContractorTemplate', $data, function($message) use ($data)
        {   
            $message->from('doNotReply@outchem.com', 'outchem');
            $message->to($data['email'])->bcc('raj.outchem@gmail.com')->subject('Welcome To Outchem');
        });

    }
    else{
        Mail::send('welcomeBuyerTemplate', $data, function($message) use ($data)
        {   
            $message->from('doNotReply@outchem.com', 'outchem');
            $message->to($data['email'])->bcc('raj.outchem@gmail.com')->subject('Welcome To Outchem');
        });

    }
    
    

}


function newUserAdminMail($userId){

    $user = aspnetusers::where('Id',$userId)->first();

    if($user['IsContractor'] == 1 ){
        $type = 'Contractor';
        $company = contractorinformation::where('ContractorId',$userId)->pluck('CompanyName')->first();
    } else {
     $type = 'Buyer';
     $company = buyerinformation::where('BuyerInformationId',$userId)->pluck('CompanyName')->first();
 }

 $firstName = $user['FirstName'];
 $lastName = $user['LastName'];
 $email = $user['Email'];
 $phone = $user['PhoneNumber'];
 $country = $user['Country'];
 $city = $user['City'];

 $data=[];
 $data = array(
    'firstName' => $firstName,
    'lastName' => $lastName,
    'email' => $email,
    'phone' => $phone,
    'country' => $country,
    'company' => $company,
    'city' => $city,
    'type' => $type,
    );
 Mail::send('newUserTemplate', $data, function($message) use ($data)
 {   
    $message->from('doNotReply@outchem.com', 'outchem');
    $message->to('raj.outchem@gmail.com')->subject('New User Registered');
});

 return "New user mail sent to admin";

}


public function sendResetLink(Request $request){

    // $base_url = "http://localhost:8010/#!";
        $base_url = "http://ec2-13-126-14-209.ap-south-1.compute.amazonaws.com/#!";
    
    
    $userId = $request['userId'];

    $user = aspnetusers::where('Email',$userId)->first();
    $firstName = $user['FirstName'];
    $lastName = $user['LastName'];
    $email = $user['Email'];
    $code = md5(uniqid(rand(), true));

    aspnetusers::where('Email',$userId)->update([
        'SecurityStamp' => $code,
        ]);

    $resetLink = $base_url.'forgotPassword?userId='.$userId.'&code='.$code;
        // $confirmLink = $base_url+'emailVerification?userId='+$userId;

    $data=[];
    $data = array(
        'firstName' => $firstName,
        'lastName' => $lastName,
        'resetLink' => $resetLink,
        'email' => $email
        );
    Mail::send('forgetPasswordTemplate', $data, function($message) use ($data)
    {   
            // dd("hello");
        $message->from('doNotReply@outchem.com', 'outchem');
        $message->to($data['email'])->subject('Forgot Password');
    });

    return "Reset link sent success";

}


public function resetPassword(Request $request){

  $userId = $request['userId'];  
  $code = $request['code'];  
  $password = $request['password'];


  $user = aspnetusers::where('Email',$userId)->first();


  if($code == $user['SecurityStamp']){

    aspnetusers::where('Email',$userId)->update([
        'PasswordHash' => Hash::make($password),
        ]);
    return 'Password Updated';
}
else{
    return "code not matched";
}


}

public function GetContractorProfile(Request $request)
{
    $contractor_id=$request['cId'];

    $contractors=contractorinformation::where('ContractorId',$contractor_id)
    ->select('AffiliatedAssociations as affiliatedAssociations',
        'AvgRating as avgRating',
        'BusinessPicture as businessPicture',
        'CompanyAddress as companyAddress',
        'CompanyDescription as companyDescription',
        'CompanyLogo as companyLogo',
        'CompanyName as companyName',
        'ContractorId as contractorId',
        'ContractorInformationId as contractorInformationId',
        'Facilities as facilities',
        'MaxCapacity as maxCapacity',
        'MinCapacity as minCapacity',
        'PackagingServices as packagingServices',
        'Rating as rating',
        'RegulatoryBodies as regulatoryBodies',
        'Review as review',
        'TechnicalSpecs as technicalSpecs',
        'Transportation as transportation')
    ->get();

    foreach($contractors as $contractor)
    {
        if($contractor->maxCapacity == null || $contractor->maxCapacity == "")
        {
            $contractor->maxCapacity = 0;
        }
        if($contractor->minCapacity == null || $contractor->minCapacity == "")
        {
            $contractor->minCapacity = 0;
        }
        if($contractor->facilities == null || $contractor->facilities == "")
        {
            $contractor->facilities = "No Facilities";
        }
        if($contractor->regulatoryBodies == null || $contractor->regulatoryBodies == "")
        {
            $contractor->regulatoryBodies = "";
        }
        if($contractor->affiliatedAssociations == null || $contractor->affiliatedAssociations == "")
        {
            $contractor->affiliatedAssociations = "Not Available";
        }
        if($contractor->technicalSpecs == null || $contractor->technicalSpecs == "")
        {
            $contractor->technicalSpecs = "Not Available";
        }

        $keyword['image']=aspnetusers::where('Id',$contractor->contractorId)->pluck('Image')->first();
        $keyword_ids=contractorcategorykeyword::where('ContractorId',$contractor->contractorId)->pluck('KeywordId');
        $keywords=keyword::whereIn('KeywordId',$keyword_ids)
        ->select('KeywordId as keywordId',
            'CategoryId as categoryId',
            'KeywordName as keywordName',
            'KeywordDescription as keywordDescription',
            'Count as count',
            'ActiveFlag as activeFlag',
            'CreatedAt as createdAt',
            'CreatedBy as createdBy',
            'lastModifiedAt as lastModifiedAt',
            'LastModifiedBy as lastModifiedBy')
        ->get();

        foreach($keywords as $keyword)
        {
            $keyword['categoryName']=category::where('CategoryId',$keyword->categoryId)
            ->select('CategoryId as categoryId','CategoryName as categoryName')->first();
        }

        $contractor['keywords']=$keywords;

        $contractor['packages']=contractorpackages::where('ContractorId',$contractor->contractorId)->get();

    }

    return $contractors;

}

public function getLoggedInUser()
{
        // return "Successful";
}

}

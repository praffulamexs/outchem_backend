<?php 
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\aspnetusers;
use App\contractorinformation;
use App\keyword;
use App\buyerinformation;
use App\contractorcategorykeyword;
use App\category;
use App\ranking;
use App\contractorpackages;
use App\facility;
use App\affassociation;
use App\package;
use App\pricingmodel;
use App\buyerkeywords;
use Carbon\Carbon;
use App\mstlocation;
use App\video;
use App\contactmeinfo;
use Illuminate\Support\Facades\DB;

class AdminsController extends Controller {

    public function categories()
    {
        $categories=category::select(
            'CategoryId as id',
            'CategoryName as name',
            'CategoryDescription as description',
            'ActiveFlag as activeFlag',
            'CreatedAt as createdAt'
            )->get();
        $final['categories']=$categories;

        return $final;
    }

    public function keyword()
    {
        $keywords = keyword::select('ActiveFlag as activeFlag',
            'CategoryId as categoryId',
            'Count as count',
            'CreatedAt as createdAt',
            'KeywordDescription as keywordDescription',
            'KeywordId as keywordId',
            'KeywordName as keywordName')
        ->get();

        foreach($keywords as $keyword)
        {
            $keyword['categoryName']=category::where('CategoryId',$keyword['categoryId'])->pluck('CategoryName')->first();
        }

        $final['keywords']=$keywords;

        return $final;
    }

    public function PriceModel()
    {
        $pricingmodel=ranking::where('ActiveFlag',1)->where('KeywordId','!=',1)->get();
        $pricingmodel['basePrice'] = ranking::where('Id',1)->pluck('BasePrice')->first();
        if(count($pricingmodel) == 0){
            foreach ($pricingmodel as $p) {
                $p['keywordName'] = keyword::where('KeywordId',$p->KeywordId)->pluck('KeywordName')->first();
                $p['price'] = keyword::where('KeywordId',$p->KeywordId)->pluck('Price')->first();
            }
        }
        $final['pricingModel']=$pricingmodel;
        return $final;
    }

    public function PriceModelSave(Request $request)
    {
        $PricingModels=$request['lstPricingModel'];

        foreach($PricingModels as $pricingmodel)
        {
            $savepricingmodel=new pricingmodel;
            $savepricingmodel->From=$pricingmodel['from'];
            $savepricingmodel->To=$pricingmodel['to'];
            $savepricingmodel->Price=$pricingmodel['price'];
            // $savepricingmodel->BasePrice=$pricingmodel['basePrice'];
            $savepricingmodel->save();
        }

        return "Pricing Model Saved";
    }

    public function getServices()
    {
        $services_id=category::where('CategoryName','Services')->pluck('CategoryId')->first();
        $services=keyword::where('CategoryId',$services_id)->orderBy('Top','desc')->get();
        return $services;
    }

    public function setTopService(Request $request)
    {
        $service_id=$request['serviceId'];
        $services_id=category::where('CategoryName','Services')->pluck('CategoryId')->first();

        $top_service_count=count(keyword::where('CategoryId',$services_id)->where('Top',1)->get());


        if(keyword::where('KeywordId',$service_id)->pluck('Top')->first() == 1)
        {
            keyword::where('KeywordId',$service_id)->update([
                'Top' => 0
                ]);

            $final['set']=2;
            $final['message']="Successfully Removed";
            return $final;
        }

        if($top_service_count < 10)
        {
            keyword::where('KeywordId',$service_id)->update([
                'Top' => 1
                ]);

            $final['set']=1;
            $final['message']="Successfully Added";
            return $final;
        }
        else
        {
            $final['set']=0;
            $final['message']="Limit Exceeded";
            return $final;
        }
    }

    public function setTopContractor(Request $request)
    {
        $contractor_id=$request['contractorId'];

        $contractor_count=count(contractorinformation::where('Top',1)->get());
        

        if( contractorinformation::where('ContractorId',$contractor_id)->pluck('Top')->first() == 1)
        {
            contractorinformation::where('ContractorId',$contractor_id)->update([
                'Top' => 0
                ]);
            $final['set']=2;
            $final['message']="Successfully Removed";
            return $final;
        }

        if($contractor_count < 10)
        {
            contractorinformation::where('ContractorId',$contractor_id)->update([
                'Top' => 1
                ]);

            $final['set']=1;
            $final['message']="Successfully Added";
            return $final;
        }
        else
        {
            $final['set']=0;
            $final['message']="Limit Exceeded";
            return $final;
        }

    }

    public function getTopContractors()
    {
        $final = JSON_encode(contractorinformation::orderBy('Top','desc')->get());
        return $final;
    }

    public function GetKeywordAdmin(Request $request)
    {
        $keywords=keyword::select(
            'KeywordId as keywordId',    
            'KeywordName as keywordName',
            'KeywordDescription as keywordDescription',
            'CategoryId as categoryId',
            'Count as count',
            'Price as price'
        )->orderBy('count','desc')->get();

        $final=array();
        $count=0;
        $contractors=0;

        foreach($keywords as $keyword)
        {
            $keyword['categoryName']=category::where('CategoryId',$keyword['categoryId'])->pluck('CategoryName')->first();

            $keyword['contractorCount']=count(contractorcategorykeyword::where('KeywordId',$keyword['keywordId'])->get());

            $keyword['buyerCount']=count(buyerkeywords::where('KeywordId',$keyword['keywordId'])->get());

            array_push($final,$keyword);

            $count=$count+$keyword['count'];

            $contractors=$contractors+$keyword['contractorCount'];
        }

        $keyword_length=count($keywords);

        for ($i=0;$i<$keyword_length-1;$i++)
        {
            for($j=$i+1;$j<$keyword_length;$j++)
            {
                $first_count=$final[$i]['count'];
                $first_count=($first_count*60)/$count;

                $first_contractors=$final[$i]['contractorCount'];
                $first_contractors=($first_contractors*40)/$contractors;

                $first_total=$first_count+$first_contractors;

                $next_count=$final[$j]['count'];
                $next_count=($next_count*60)/$count;

                $next_contractors=$final[$j]['contractorCount'];
                $next_contractors=($next_contractors*40)/$contractors;

                $next_total=$next_count+$next_contractors;

                if ($first_total < $next_total)
                {
                    $temp=$final[$i];
                    $final[$i]=$final[$j];
                    $final[$j]=$temp;
                }

            }
        }

        //TO SAVE RANK  
        $rank=1;

        foreach($final as $single)
        {
            $single['rank']=$rank;
        
            if ( count(ranking::where('KeywordId',$single['keywordId'])->get())>0 )
            {
                if ( count(ranking::where('KeywordId',$single['keywordId'])->where('Rank',$single['rank'])->get()) >0 )
                {
                    //
                }
                else
                {
                    ranking::where('KeywordId',$single['keywordId'])->update([
                        'Rank' => $single['rank']
                    ]);
                }
            }
            else
            {
                $ranking = new ranking;
                $ranking->KeywordId=$single['keywordId'];
                $ranking->Rank=$single['rank'];
                $ranking->ActiveFlag=0;
                $ranking->BasePrice=ranking::pluck('BasePrice')->first();
                $ranking->save();
            }

            $rank++;            
        }


        array_splice($final,30);
        return $final;

    }    

    public function CategoryEdit(Request $request)
    {
        $id=$request['categoryId'];
        $name=$request['categoryName'];
        $desc=$request['categoryDescription'];
        $today=Carbon::today();

        if(count(category::where('CategoryName',$name)->get())>0)
        {
            return "Already Exists";
        }

        else{
            category::where('CategoryId',$id)->update([
                'CategoryName' => $name ,
                'CategoryDescription' => $desc ,
                'LastModifiedAt' => $today
                ]);
            return "Category Updated";
        }
        
    }

    public function CategorySave(Request $request)
    {
        $category = new category;
        $category->CategoryName=$request['categoryName'];
        $category->CategoryDescription=$request['categoryDescription'];
        $category->ActiveFlag=1;
        $category->CreatedAt=Carbon::today();
        $category->save();

        return "Category Saved";
    }

    public function CategoryDelete(Request $request)
    {
        $id=$request['cId'];
        category::where('CategoryId',$id)->delete();

        return "Deleted Successfully";
    }

    public function KeywordSave(Request $request)
    {
        $id=$request['categoryId'];
        $name=$request['keywordName'];
        $desc=$request['keywordDescription'];
        $count=0;
        $activeFlag=true;

        $keyword=new keyword;
        $keyword->CategoryId=$id;
        $keyword->KeywordName=$name;
        $keyword->KeywordDescription=$desc;
        $keyword->ActiveFlag=$activeFlag;
        $keyword->Count=$count;
        $keyword->Price=0;
        $keyword->CreatedAt=Carbon::today();
        $keyword->CreatedBy="admin";
        $keyword->save();

        return "Keyword Saved Successfully";

    }

    public function KeywordEdit(Request $request)
    {
        $keywordId=$request['keywordId'];
        $categoryId=$request['categoryId'];
        $keywordName=$request['keywordName'];
        // $keywordDescription=$request['keywordDescription'];    
        
        
        keyword::where('KeywordId',$keywordId)->update([
            'KeywordName' => $keywordName,
            'CategoryId' => $categoryId
            ]);

        return "Keyword Updated Successfully";
        
    }

    public function KeywordDelete(Request $request)
    {
        $id=$request['kId'];
        keyword::where('KeywordId',$id)->delete();

        return "Keyword Deleted Successfully";
    }

    public function FacilitySave(Request $request)
    {
        $name=$request['facilityName'];

        $facility=new facility;
        $facility->FacilityName=$name;
        $facility->save();

        return "Facility Saved Successfully";
    }

    public function FacilityEdit(Request $request)
    {
        $id=$request['facilityId'];
        $name=$request['facilityName'];
        facility::where('FacilityId',$id)->update([
            'FacilityName' => $name
            ]);

        return "Facility Edit Successful";
    }

    public function FacilityDelete(Request $request)
    {
        $id=$request['facilityId'];
        facility::where('FacilityId',$id)->delete();

        return "Deleted Successfully";
    }

    public function AffAssociationSave(Request $request)
    {
        $name=$request['affAssociationName'];
        $affAssociation=new affAssociation;
        $affAssociation->AffAssociationName=$name;
        $affAssociation->save();

        return "Affiliated Association Created Successfully";
    }

    public function AffAssociationEdit(Request $request)
    {
        $id=$request['affAssociationID'];
        $name=$request['affAssociationName'];

        affassociation::where('AffAssociationID',$id)->update([
            'AffAssociationName' => $name
            ]);

        return "Affiliated Association Updated Successfully";
    }

    public function AffAssociationDelete(Request $request)
    {
        $id=$request['aId'];
        affassociation::where('AffAssociationId',$id)->delete();

        return "Affiliated Association Deleted Successfully";
    }

    public function TopContactedContractor()
    {
        return DB::select(DB::raw('
            select contractorinformation.CompanyName as companyName, count(*) as contactCount
            from contactmeinfo , contractorinformation
            where contractorinformation.ContractorId = contactmeinfo.ContractorId
            group by contactmeinfo.ContractorId
            order by count(*) desc
            limit 50
            '));
    }

    public function getCountrywiseWebsiteHits()
    {
        $countries = DB::select(DB::raw('
            select Country as country , sum(Hits) as hits
            from mstlocation 
            group by country
            '));

        $final=array();
        // return $countries;
        foreach ($countries as $country)
        {
            $obj=null;
            $hits = $country->hits;
            $coun = $country->country;

            $all_countries=json_decode(file_get_contents("country-list.json"));
            $count_name;
            foreach($all_countries as $count)
            {
                if($count->name==$coun)
                {
                    $col="alpha-3";
                    $count_name=strtolower($count->$col);
                }
            }
            $obj[$count_name]=$hits;

            array_push($final,$obj);
            // $final[$coun]=$hits;
        }

        return $final;
    }

    public function UserCountCountrywise()
    {
        $contractors = DB::select(DB::raw('
            select Country as country,count(*) as count,IsContractor as isContractor
            from aspnetusers
            where IsContractor = 1
            group by country
            '));

        $buyers = DB::select(DB::raw('
            select Country as country,count(*) as count
            from aspnetusers
            where IsContractor = 0
            group by country
            '));

        $total=array();

        foreach($contractors as $contractor)
        {
            array_push($total,$contractor);
        }

        foreach($buyers as $buyer)
        {
            if($buyer->country!=null)
            {
                array_push($total,$buyer);
            }
        }

        return $total;
    }

    public function FindBuyer(Request $request)
    {
        $id=$request['searchKey'];

        $buyer_ids=buyerkeywords::where('KeywordId',$id)->select('BuyerId as buyerId')->get();

        $buyers=buyerinformation::whereIn('BuyerInformationId',$buyer_ids)
        ->select('BuyerInformationId as buyerId',
         'ProfilePicture as profilePicture',
         'CompanyName as companyName',
         'AddressOfBusiness as addressOfBusiness')
        ->get();
        return $buyers;
    }

    public function video()
    {
        return video::pluck('url')->first();
    }

    public function videoUpload(Request $request)
    {
        $url=$request['video'];
        video::where('id',1)->update([
            'url' => $url
            ]);
        return "Video Updated";
    }

    public function PendingReviewList()
    {
        $final = [];

        $contactmeinfo = contactmeinfo::where('Mail',0)->where('Rated',0)->get();

        foreach ($contactmeinfo as $con) {
            $data =[];
            $data['buyerCompanyName'] = buyerinformation::where('BuyerInformationId',$con->BuyerId)->pluck('CompanyName')->first();

            $data['buyerId'] = $con->BuyerId;

            $data['contractorCompanyName'] = contractorinformation::where('ContractorId',$con->ContractorId)->pluck('CompanyName')->first();

            $data['contractorId'] = $con->ContractorId;

            $data['keywordName'] = keyword::where('KeywordId',$con->KeywordId)->pluck('KeywordName')->first();
            $data['keywordId'] = $con->KeywordId;


            array_push($final,$data);        
        }

        return $final;
    }

    public function savePackage(Request $request)
    {
        if ( count(package::where('PackageName',$request['packageName'])->get())>0 )
        {
            return "Invalid Package";
        }
        else
        {
            $package = new package;
            $package->PackageName = $request['packageName'];
            $package->PackageDays = $request['packageDays'];
            $package->ActiveFlag=1;
            $package->CreatedAt=1;
            $package->save();

            return "Package Saved";
        } 
    }

    public function EditPackage(Request $request)
    {
        $packageId = $request['packageId'];
        $packageName = $request['packageName'];
        $packageDays = $request['packageDays'];
        $activeFlag = $request['activeFlag'];

        if ( count(package::where('PackageName',$packageName)->where('PackageId','!=',$packageId)->get())>0 )
        {
            return "Name Already Exists";
        }
        else
        {
            package::where('PackageId',$packageId)->update([
                'PackageName' => $packageName,
                'PackageDays' => $packageDays
                ]);

            return "Updated Successfully";
        }

    }

    public function deletePackage(Request $request)
    {
        $pId=$request['pId'];

        package::where('PackageId',$pId)->update([
            'ActiveFlag' => 0
            ]);

        return "Deleted Successfully";
    }

    public function setBasePrice(Request $request)
    {
        $basePrice = $request['basePrice'];
        
        DB::select(DB::raw('
            update keywordranking set BasePrice="'.$basePrice.'"
        '));

        $keyword_ids = ranking::where('ActiveFlag',0)->pluck('KeywordId');

        foreach($keyword_ids as $keyword_id)
        {
            keyword::where('KeywordId',$keyword_id)->update([
                'Price' => $basePrice
            ]);
        }

        return "1";

    }
    
}

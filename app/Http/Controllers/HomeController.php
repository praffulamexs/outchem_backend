<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\keyword;
use App\buyerinformation;
use App\contractorinformation;
use App\contractorcategorykeyword;
use App\category;
use App\contractorpackages;
use App\certificates;
use App\mstlocation;
use App\aspnetusers;
use App\contractorlistinglog;
use Illuminate\Support\Facades\Input;
use Mail;
use Illuminate\Mail\Mailer;

//use Aws\S3\S3Client;
use Carbon\Carbon;
use App\keywordsearchlog;

class HomeController extends Controller {

    public function getKeywordBySearchKey(Request $request)
    {
        $searchKey=$request['searchKey'];
        $categoryId=$request['cId'];
        if ($categoryId == 0)
        {
            $search=keyword::where('KeywordName','like',$searchKey.'%')
            ->select('ActiveFlag as activeFlag',
             'CategoryId as categoryId',
             'Count as count',
             'CreatedAt',
             'KeywordDescription as description',
             'KeywordId as keywordId',
             'KeywordName as keywordName')
            ->get();
        }
        else
        {
            $search=keyword::where('KeywordName','like',$searchKey.'%')
            ->where('CategoryId',$categoryId)
            ->select('ActiveFlag as activeFlag',
             'CategoryId as categoryId',
             'Count as count',
             'CreatedAt',
             'KeywordDescription as description',
             'KeywordId as keywordId',
             'KeywordName as keywordName')
            ->get();
        }
        
        foreach($search as $each_search)
        {
            $each_search['categoryName']=null;

            if($each_search['activeFlag']==0)
            {
                $each_search['activeFlag']=false;
            }
            else
            {
                $each_search['activeFlag']=true;
            }
        }

        $final['keywords']=$search;
        return $final;
    }

    public function getCount()
    {
        $final['buyerCnt']=count(buyerinformation::all());
        $final['contractorCnt']=count(contractorinformation::all());
        $final['productCnt']=count(keyword::where('CategoryId','2')->get());
        $final['serviceCnt']=count(keyword::where('CategoryId','1')->get());

        return $final;
    }

    public function FindContractor(Request $request)
    {
        $searchKey=$request['searchKey'];
        $paid=$request['paid'];
        $location=$request['location'];
        $number=$request['number']; //PAID USER is 1

        $count=keyword::where('KeywordId',$searchKey)->pluck('Count')->first();
        $count=$count+$number;

        //CODE FOR PAID USERS
        $paid_user_ids=array();
        
        $paid_users=contractorlistinglog::where('KeywordId',$searchKey)->pluck('ContractorId')->unique('ContractorId');
        foreach($paid_users as $paid_user)
        {
            array_push($paid_user_ids,$paid_user);
        }
        //CODE FOR PAID USERS


        //CODE FOR KEYWORD LOG
        keyword::where('KeywordId',$searchKey)->update([
            'Count' => $count ,
            'LastModifiedAt' => Carbon::now()
            ]);

        if($number == 1)
        {
            $keywordsearchlog=new keywordsearchlog;
            $keywordsearchlog->KeywordId=$searchKey;
            $keywordsearchlog->Date=Carbon::now();
            $keywordsearchlog->save();
        }
        //CODE FOR KEYWORD LOG

        $keywordName=keyword::where('KeywordId',$searchKey)->pluck('KeywordName')->first();
        $contractor_ids=contractorcategorykeyword::where('KeywordId',$searchKey)->pluck('ContractorId');

        $contractors;

        if($number == 1)
        {
            $contractors=contractorinformation::whereIn('ContractorId',$paid_user_ids)
            ->select('AffiliatedAssociations as affiliatedAssociations',
             'AvgRating as avgRating',
             'BusinessPicture as businessPicture',
             'CompanyAddress as companyAddress',
             'CompanyDescription as companyDescription',
             'CompanyLogo as companyLogo',
             'CompanyName as companyName',
             'ContractorId as contractorId',
             'ContractorInformationId as contractorInformationId',
             'Facilities as facilities',
             'MaxCapacity as maxCapacity',
             'MinCapacity as minCapacity',
             'PackagingServices as packagingServices',
             'Rating as rating',
             'RegulatoryBodies as regulatoryBodies',
             'Review as review',
             'TechnicalSpecs as technicalSpecs',
             'Transportation as transportation')
            ->get();

            return $this->returnContractors($contractors);
        }

        else
        {
            $contractors=contractorinformation::whereNotIn('ContractorId',$paid_user_ids)->whereIn('ContractorId',$contractor_ids)
            ->select('AffiliatedAssociations as affiliatedAssociations',
             'AvgRating as avgRating',
             'BusinessPicture as businessPicture',
             'CompanyAddress as companyAddress',
             'CompanyDescription as companyDescription',
             'CompanyLogo as companyLogo',
             'CompanyName as companyName',
             'ContractorId as contractorId',
             'ContractorInformationId as contractorInformationId',
             'Facilities as facilities',
             'MaxCapacity as maxCapacity',
             'MinCapacity as minCapacity',
             'PackagingServices as packagingServices',
             'Rating as rating',
             'RegulatoryBodies as regulatoryBodies',
             'Review as review',
             'TechnicalSpecs as technicalSpecs',
             'Transportation as transportation')
            ->get();

            return $this->returnContractors($contractors);            
        }
        
    }

    public function returnContractors($contractors)
    {
        foreach($contractors as $contractor)
        {

            $contractor['image']=aspnetusers::where('Id',$contractor->contractorId)->pluck('Image')->first();

            $keyword_ids=contractorcategorykeyword::where('ContractorId',$contractor->contractorId)->pluck('KeywordId');
            $keywords=keyword::whereIn('KeywordId',$keyword_ids)
            ->select('KeywordId as keywordId',
             'CategoryId as categoryId',
             'KeywordName as keywordName',
             'KeywordDescription as keywordDescription',
             'Count as count',
             'ActiveFlag as activeFlag',
             'CreatedAt as createdAt',
             'CreatedBy as createdBy',
             'lastModifiedAt as lastModifiedAt',
             'LastModifiedBy as lastModifiedBy')
            ->get();

            foreach($keywords as $keyword)
            {
                $keyword['categoryName']=category::where('CategoryId',$keyword->categoryId)
                ->select('CategoryId as categoryId')->first();
            }

            $contractor['keywords']=$keywords;

            $contractor['packages']=contractorpackages::where('ContractorId',$contractor->contractorId)->get();
            // $contractor['perRating']=null;
            
            $product_id=category::where('Categoryname','Products')->pluck('CategoryId')->first();
            $products=keyword::whereIn('KeywordId',$keyword_ids)->where('CategoryId',$product_id)->pluck('KeywordName');

            $final_products=null;

            foreach($products as $product)
            {
                if($final_products==null)
                {
                    $final_products=$product;
                }
                else
                {
                    $final_products=$final_products.", ".$product;
                }
            }

            // $contractor['products']=$final_products;

            $service_id=category::where('Categoryname','Services')->pluck('CategoryId')->first();
            $services=keyword::whereIn('KeywordId',$keyword_ids)->where('CategoryId',$service_id)->pluck('KeywordName');

            $final_services=null;

            foreach($services as $service)
            {
                if($final_services==null)
                {
                    $final_services=$service;
                }
                else
                {
                    $final_services=$final_services.", ".$service;
                }
            }



            // $contractor['services']=$final_services;
            // $contractor['tempUrl']="https://s3.ap-south-1.amazonaws.com/outchem/images/".$contractor['contractorId'].".jpg";
        }

        return $contractors;
    }

    public function getCategoryById($id)
    {
        $keywords=keyword::where('CategoryId',$id)
        ->select('KeywordId as keywordId',
            'CategoryId as categoryId',
            'KeywordName as keywordName',
            'KeywordDescription as keywordDescription',
            'Count as count',
            'ActiveFlag as activeFlag',
            'CreatedAt as createdAt',
            'CreatedBy as createdBy',
            'lastModifiedAt as lastModifiedAt',
            'LastModifiedBy as lastModifiedBy')
        ->get();
        return $keywords;
    }

    public function certificates()
    {
        return certificates::all();
    }

    public function getTopCompanies(Request $request)
    {
        $imageUrl=$request['imageUrl'];
        $contractors = contractorinformation::where('Top',1)->get();
        $i=1;
        foreach($contractors as $contractor)
        {
            $user_image=aspnetusers::where('Id',$contractor->ContractorId)->pluck('Image')->first();
            $contractor['Image'] = $imageUrl.$user_image.".jpg";
            $contractor['Id']=$i;
            $i++;
        }
        return $contractors;
    }

    public function location(Request $request)
    {

        if ( mstlocation::where('Country',$request['country'])
            ->where('RegionName',$request['regionName'])
            ->where('City',$request['city'])
            ->where('Zip',$request['zip'])
            ->first() )
        {
            $hits=mstlocation::where('Country',$request['country'])
            ->where('RegionName',$request['regionName'])
            ->where('City',$request['city'])
            ->where('Zip',$request['zip'])
            ->pluck('Hits')
            ->first();

            $hits=$hits+1;

            mstlocation::where('Country',$request['country'])
            ->where('RegionName',$request['regionName'])
            ->where('City',$request['city'])
            ->where('Zip',$request['zip'])
            ->update([
                'Hits' => $hits
                ]);
        }

        else
        {
            $location=new mstlocation;
            $location->Country=$request['country'];
            $location->CountryCode=$request['countryCode'];
            $location->RegionName=$request['regionName'];
            $location->Region=$request['region'];
            $location->City=$request['city'];
            $location->Zip=$request['zip'];
            $location->Hits=1;
            $location->save();
        }
    }

    public function getTopService(){

        $service_id=category::where('Categoryname','Services')->pluck('CategoryId')->first();
        $services = keyword::where('CategoryId',$service_id)->where('Top',1)->orderBy('count','desc')->limit(10)
        ->select('KeywordName as keywordName',
         'KeywordId as keywordId',
         'KeywordDescription as keywordDescription',
         'ActiveFlag as activeFlag',
         'CreatedAt as createdAt')->get();

        return $services;
    }

    public function ImageUpload(Request $request)
    {
        // return $request;
        $uploadpath = 'assets/images/profiles';
        $file = Input::file('attachment');
        $filename = rand().'.jpg';
        $file->move($uploadpath,$filename);
        chmod('assets/images/profiles/'.$filename, 0777);
        return $filename;
    }

    public function ImageSave(Request $request){

        $userId = $request['userId'];
        $image = $request['image'];

        $imagename = aspnetusers::where('Id',$userId)->pluck('Image')->first();


        if($imagename){
        $imageuri = 'assets/images/profiles/'.$imagename.'.jpg';
            unlink($imageuri);
        }

        aspnetusers::where('Id',$userId)->update([
            'Image'=> $image,
            ]);
        return 'Image saved';
    }

    public function mail(Request $req)
    {

        $data=[];
        $data = array(
            'comment' => "hello",
            'status' => "true",);
        Mail::send('check', $data, function($message) use ($data)
        {   
            // dd("hello");
            $message->from('chinar.amexs@gmail.com', 'test mail');
            $message->to('saraogipraveen@gmail.com')->subject('Your Ticket Status On Discover India');
        });

        return "hello";
        
    }

    public function getContractors()
    {
        return contractorinformation::all();
    }
    
    public function getBuyers()
    {
        return buyerinformation::all();        
    }

} //End of Class

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\buyerinformation;
use App\buyerkeywords;
use App\keyword;
use App\category;
use App\aspnetusers;
use App\contactmeinfo;
use Carbon\Carbon;
use DB;
use Mail;
use Illuminate\Mail\Mailer;

class BuyersController extends Controller {

    public function buyerinformation($id)
    {
        $info=buyerinformation::where('BuyerInformationId',$id)
        ->select('BuyerInformationId as buyerId',
           'ProfilePicture as profilePicture',
           'CompanyName as companyName',
           'AddressOfBusiness as addressOfBusiness')
        ->first();


        $keys=buyerkeywords::where('BuyerId',$id)->get();
        $key_ids=buyerkeywords::where('BuyerId',$id)->pluck('KeywordId');

        $info['keywords']=$keys;

        $keywords=keyword::whereIn('KeywordId',$key_ids)
        ->select('KeywordId as keywordId',
           'CategoryId as categoryId',
           'KeywordName as keywordName',
           'KeywordDescription as keywordDescription',
           'Count as count',
           'ActiveFlag as activeFlag')
        ->get();
        
        foreach($keywords as $keyword)
        {
            $keyword['categoryName']=category::where('CategoryId',$keyword['categoryId'])->pluck('CategoryName')->first();
        }

        $info['addedKeywords']=$keywords;

        $info['contactCount']=count(contactmeinfo::where('BuyerId',$id)->get());

        return $info;
    }

    public function updateInfo(Request $request)
    {
        buyerinformation::where('BuyerInformationId',$request['buyerId'])
        ->update([
            'CompanyName' => $request['companyName'],
            'AddressOfBusiness' => $request['addressOfBusiness']
            ]);

        buyerkeywords::where('BuyerId',$request['buyerId'])->delete();

        foreach($request['keywords'] as $keyword)
        {
            // if ( buyerkeywords::where('BuyerId',$request['buyerId'])->where('KeywordId',$keyword['keywordId'])->get() )
            // {

            // }
            // else
            // {
            $buyerkeywords = new buyerkeywords;
            $buyerkeywords->BuyerId=$request['buyerId'];
            $buyerkeywords->KeywordId=$keyword['keywordId'];
            $buyerkeywords->save();
            // }
        }

        return "Updated Successfully";
    }

    public function contactme(Request $request){
        $contractorId = $request['contractorId'];
        $buyerId = $request['buyerId'];
        $keywordId = $request['keywordId'];

        $current_date = Carbon::now();

        $entry = contactmeinfo::where('BuyerId',$buyerId)->where('ContractorId',$contractorId)->where('KeywordId',$keywordId)->get();


        if(count($entry) > 0){
            return "Entry Existed";
        }
        else{

            $contactmeinfo = new contactmeinfo;
            $contactmeinfo->BuyerId = $buyerId;
            $contactmeinfo->ContractorId = $contractorId;
            $contactmeinfo->KeywordId = $keywordId;
            $contactmeinfo->Mail = 0;
            $contactmeinfo->Rated = 0;
            $contactmeinfo->CreatedDate = Carbon::now();
            $contactmeinfo->save();
            $this->mailAdminContactDetail($buyerId,$contractorId);
            return "contact info saved";
        }

    }


    function mailAdminContactDetail($bId,$cId){


        $buyer = aspnetusers::where('Id',$bId)->first();
        $contractor = aspnetusers::where('Id',$cId)->first();

        $cfname = $contractor['FirstName'];
        $clname = $contractor['LastName'];
        $cemail = $contractor['Email'];
        $cphone = $contractor['PhoneNumber'];

        $bfname = $buyer['FirstName'];
        $blname = $buyer['LastName'];
        $bemail = $buyer['Email'];
        $bphone = $buyer['PhoneNumber'];

        $data=[];
        $data = array(
            'cfname' => $cfname,
            'clname' => $clname,
            'cemail' => $cemail,
            'cphone' => $cphone,
            'bfname' => $bfname,
            'blname' => $blname,
            'bemail' => $bemail,
            'bphone' => $bphone,
            );
        Mail::send('adminForContactDetailTemplate', $data, function($message) use ($data)
        {   
            $message->from('doNotReply@outchem.com', 'outchem');
            $message->to('raj.outchem@gmail.com')->subject('New Contact Request');
        });

        $this->mailBuyerForContacting($bId,$cId);


        return "mail sent to admin";

    }

    function mailBuyerForContacting($bId,$cId){

        $buyer = aspnetusers::where('Id',$bId)->first();
        $contractor = aspnetusers::where('Id',$cId)->first();

        $cfname = $contractor['FirstName'];
        $clname = $contractor['LastName'];

        $bfname = $buyer['FirstName'];
        $blname = $buyer['LastName'];
        $bemail = $buyer['Email'];


        $data=[];
        $data = array(
            'cfname' => $cfname,
            'clname' => $clname,
            'bfname' => $bfname,
            'blname' => $blname,
            'bemail' => $bemail,
            );
        Mail::send('buyerForContactDetailTemplate', $data, function($message) use ($data)
        {   
            $message->from('doNotReply@outchem.com', 'outchem');
            $message->to($data['bemail'])->bcc('raj.outchem@gmail.com')->subject('Request Received');
        });

        return "mail sent to buyer";
    }

    public function searchBuyer()
    {

      // $data = buyerinformation::select('BuyerInformationId as id', 'CompanyName as name')->get();
        $buyerIds = buyerinformation::pluck('BuyerInformationId');
        $final = [];
        foreach ($buyerIds as $id) {
            $data = [];
            $data['id'] = $id;
            $companyName = buyerinformation::where('BuyerInformationId',$id)->pluck('CompanyName')->first();
            $email = aspnetusers::where('Id',$id)->pluck('Email')->first();
            $data['email'] = $email;
            $data['companyName'] = $companyName.'('.$email.')';
            array_push($final,$data); 
        }

        return $final;
    }

    public function sendMails(Request $request)
    {   
        foreach ($request['data'] as $r) {
            $contacts = DB::select(DB::raw('
            select  *
            from contactmeinfo
            where BuyerId = '.$r['id'].' 
            group by ContractorId
                '));
         if(count($contacts) > 0){
            foreach ($contacts as $c) {
                $this->sendReviewMail($r['id'],$c->ContractorId);
                    /*$data =[];
                    $data['b'] = $r['id'];
                    $data['c'] = $c->ContractorId;
                    array_push($final,$data);*/
                }
            }
            else{
            }   
        }
        return "Mails Sent to Buyers"; 
    }

    public function sendReviewMail(Request $request)
    {

        $buyerId = $request['buyerId'];
        $contractorId = $request['contractorId'];
        $keywordId = $request['keywordId'];

        contactmeinfo::where('BuyerId',$buyerId)->where('ContractorId',$contractorId)->where('KeywordId',$keywordId)->update([
            'Mail' => 1,
            ]);

        $this->sendRatingMail($buyerId,$contractorId);

        
    }

    function sendRatingMail($bId,$cId){

        // $base_url = "http://localhost:8010/#!";
        $base_url = "http://ec2-13-126-14-209.ap-south-1.compute.amazonaws.com/#!";


        $buyer = aspnetusers::where('Id',$bId)->first();
        $contractor = aspnetusers::where('Id',$cId)->first();

        $cfname = $contractor['FirstName'];
        $clname = $contractor['LastName'];

        $bfname = $buyer['FirstName'];
        $blname = $buyer['LastName'];
        $bemail = $buyer['Email'];

        $reviewLink = $base_url.'contractorRatings?contractorId='.$cId;

        $data=[];
        $data = array(
            'cfname' => $cfname,
            'clname' => $clname,
            'bfname' => $bfname,
            'blname' => $blname,
            'bemail' => $bemail,
            'reviewLink' => $reviewLink,
            );
        Mail::send('reviewTemplate', $data, function($message) use ($data)
        {   
            $message->from('doNotReply@outchem.com', 'outchem');
            $message->to($data['bemail'])->bcc('raj.outchem@gmail.com')->subject('Feedback Request');
        });



        return "mail sent to buyer";
    }

}

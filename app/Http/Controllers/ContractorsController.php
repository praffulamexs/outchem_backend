<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\aspnetusers;
use App\contractorinformation;
use App\keyword;
use App\buyerinformation;
use App\contractorcategorykeyword;
use App\category;
use App\contractorpackages;
use App\facility;
use App\affassociation;
use App\package;
use App\contractorrating;
use App\contractorlistinglog;
use App\keywordsearchlog;
use Carbon\Carbon;
use App\contactmeinfo;
use Illuminate\Support\Facades\DB;

class ContractorsController extends Controller {

    public function contractors($uId)
    {

        $contractors=contractorinformation::where('ContractorId',$uId)
        ->select('AffiliatedAssociations as affiliatedAssociations',
            'AvgRating as avgRating',
            'BusinessPicture as businessPicture',
            'CompanyAddress as companyAddress',
            'CompanyDescription as companyDescription',
            'CompanyLogo as companyLogo',
            'CompanyName as companyName',
            'ContractorId as contractorId',
            'ContractorInformationId as contractorInformationId',
            'Facilities as facilities',
            'MaxCapacity as maxCapacity',
            'MinCapacity as minCapacity',
            'PackagingServices as packagingServices',
            'Rating as rating',
            'RegulatoryBodies as regulatoryBodies',
            'Review as review',
            'TechnicalSpecs as technicalSpecs',
            'Transportation as transportation')
        ->get();

        foreach($contractors as $contractor)
        {
            $keyword_ids=contractorcategorykeyword::where('ContractorId',$contractor->contractorId)->pluck('KeywordId');
            $keywords=keyword::whereIn('KeywordId',$keyword_ids)
            ->select('KeywordId as keywordId',
                'CategoryId as categoryId',
                'KeywordName as keywordName',
                'KeywordDescription as keywordDescription',
                'Count as count',
                'ActiveFlag as activeFlag',
                'CreatedAt as createdAt',
                'CreatedBy as createdBy',
                'lastModifiedAt as lastModifiedAt',
                'LastModifiedBy as lastModifiedBy')
            ->get();

            foreach($keywords as $keyword)
            {
                $keyword['categoryId']=category::where('CategoryId',$keyword->categoryId)
                ->pluck('CategoryId as categoryId')->first();
                $keyword['categoryName']=category::where('CategoryId',$keyword->categoryId)
                ->pluck('CategoryName as categoryName')->first();
            }

            $contractor['keywords']=$keywords;

            $contractor['packages']=contractorpackages::where('ContractorId',$contractor->contractorId)->get();

            return $contractor;

        }

        // return $contractors;
    }

    public function getFacilities()
    {
        $facilities=facility::select('FacilityId as facilityId','FacilityName as facilityName')->get();
        $final['facilities']=$facilities;
        return $final;
    }

    public function AffAssociation()
    {
        $affassociation=affassociation::select('AffAssociationID as affAssociationID',
         'AffAssociationName as affAssociationName')
        ->get();

        $final['affAsso']=$affassociation;
        return $final;
    }

    public function packages()
    {
        $packages=package::select('PackageId as pId',
          'PackageName as name',
          'PackageDays as days',
          'ActiveFlag as activeFlag',
          'CreatedAt as createdAt')->where('activeFlag',1)->get();
        return $packages;
    }

    public function GetKeywordsPrice(Request $request)
    {
        $keyword_id=$request['kId'];
        return keyword::where('KeywordId',$keyword_id)->pluck('price')->first();
    }

    public function ContractorSaveData(Request $request)
    {
        contractorinformation::where('ContractorId',$request['contractorId'])
        ->update([
            'CompanyLogo' => $request['companyLogo'],
            'CompanyName' => $request['companyName'],
            'CompanyAddress' => $request['companyAddress'],
            'CompanyDescription' => $request['companyDescription'],
            'BusinessPicture' => $request['businessPicture']
            ]);
        
        return "Updated Successfully";
        
    }

    public function AddContractorCategoryKeywords(Request $request)
    {

        contractorinformation::where('ContractorId',$request['contractorId'])
        ->update([
            'RegulatoryBodies' => $request['regulatoryBodies'],
            'AffiliatedAssociations' => $request['affiliatedAssociations'],
            'Facilities' => $request['Facilities'],
            'TechnicalSpecs' => $request['TechnicalSpecs'],
            'Transportation' => $request['Transportation'],
            'PackagingServices' => $request['PackagingServices'],
            'MaxCapacity' => $request['MaxCapacity'],
            'MinCapacity' => $request['MinCapacity'],
            ]);

        $keywords=$request['keywords'];

        contractorcategorykeyword::where('ContractorId',$request['contractorId'])->delete();

        foreach($keywords as $keyword)
        {   
            // if ( count(contractorcategorykeyword::where('ContractorId',$request['contractorId'])
            //     ->where('KeywordId',$keyword['keywordId'])
            //     ->get())>0 )
            // {

            // }
            // else
            // {

            $contractorcategorykeyword=new contractorcategorykeyword;
            $contractorcategorykeyword->ContractorId=$request['contractorId'];
            $contractorcategorykeyword->KeywordId=$keyword['keywordId'];
            $contractorcategorykeyword->CategoryId=$keyword['categoryId'];
            $contractorcategorykeyword->save();
            // }
        }
        
        

        return "Updated Successfully";
    }

    public function addContractorPackage(Request $request)
    {
        
    }


    public function userEdit(Request $request)
    {
        aspnetusers::where('Id',$request['id'])
        ->update([
            'FirstName' => $request['firstName'],
            'LastName' => $request['lastName'],
            'Country' => $request['country'],
            'City' => $request['city'],
            'Email' => $request['email'],
            'EmailConfirmed' => $request['emailConfirmed'],
            'PhoneNumber' => $request['phone'],
            'PhoneNumberConfirmed' => $request['phoneNumberConfirmed'],
            'UserName' => $request['userName']
            ]);

        return "Successfully Updated";
    }

    public function getContractorProgress($id)
    {
        $progress=0;

        if (contractorinformation::where('ContractorId',$id)->pluck('CompanyAddress')->first()!="")
        {
            $progress = $progress + 5;
        }

        if (contractorinformation::where('ContractorId',$id)->pluck('CompanyDescription')->first()!="")
        {
            $progress = $progress + 5;
        }

        if (contractorinformation::where('ContractorId',$id)->pluck('RegulatoryBodies')->first()!="")
        {
            $progress = $progress + 10;
        }

        if (contractorinformation::where('ContractorId',$id)->pluck('AffiliatedAssociations')->first()!="")
        {
            $progress = $progress + 10;
        }

        if (contractorinformation::where('ContractorId',$id)->pluck('CompanyName')->first()!="")
        {
            $progress = $progress + 10;
        }

        if (contractorinformation::where('ContractorId',$id)->pluck('Facilities')->first()!="")
        {
            $progress = $progress + 10;
        }

        if (count(contractorinformation::where('ContractorId',$id)->pluck('TechnicalSpecs')->first())>0)
        {
            $progress = $progress + 10;
        }

        if (contractorinformation::where('ContractorId',$id)->pluck('Transportation')->first()!="")
        {
            $progress = $progress + 5;
        }

        if (contractorinformation::where('ContractorId',$id)->pluck('PackagingServices')->first()!="")
        {
            $progress = $progress + 5;
        }

        if (contractorinformation::where('ContractorId',$id)->pluck('MaxCapacity')->first()!="")
        {
            $progress = $progress + 5;
        }

        if (contractorinformation::where('ContractorId',$id)->pluck('MinCapacity')->first()!="")
        {
            $progress = $progress + 5;
        }

        $services_id=category::where('CategoryName','Services')->pluck('CategoryId')->first();

        if (count(contractorcategorykeyword::where('ContractorId',$id)->where('CategoryId',$services_id)->get()) > 0)
        {
            $progress = $progress + 5;
        }

        $product_id=category::where('CategoryName','Products')->pluck('CategoryId')->first();

        if (count(contractorcategorykeyword::where('ContractorId',$id)->where('CategoryId',$product_id)->get()) > 0)
        {
            $progress = $progress + 5;
        }


        $image=count(aspnetusers::where('Id',$id)->pluck('Image'));
        $phone=count(aspnetusers::where('Id',$id)->pluck('PhoneNumberConfirmed'));

        if($image>0)
        {
            $progress=$progress+5;
        }

        if($phone>0)
        {
            $progress=$progress+5;
        }

        return $progress;
    }

    public function changePassword(Request $request)
    {
        $contractorId=$request['userId'];
        $oldPassword=$request['oldPassword'];
        $newPassword=$request['newPassword'];
        
        aspnetusers::where('Id',$contractorId)->update([
            'PasswordHash' => Hash::make($newPassword)
            ]);

        return "Password Updated Successfully";

    }

    public function contractorLog(Request $request){

        $data = $request['data'];
        foreach ($data as $d) {
            $listinglog = new contractorlistinglog;
            $listinglog->ContractorId = $d['contractorId'];
            $listinglog->KeywordId = $d['keywordId'];
            $listinglog->Type = $d['type'];
            $listinglog->DateCreated = Carbon::now();
            $listinglog->save();
        }

        return "Contractor Logged";
    }

    public function trendingKeywordsData(Request $request)
    {
        $graphType=$request['graphType'];
        $search_by=$request['search_by'];
        $cId=$request['cId'];
        // $graphDate=$request['graphDate'];

        $today=Carbon::today();
        $tomorrow=Carbon::tomorrow();

        /* DAY */
        if($graphType == "D")
        {
            if($search_by == "all")
            {
                $keywords=DB::select(DB::raw('
                    select KeywordId as keywordId, count(*) as totalCount
                    from keywordsearchlog
                    where Date BETWEEN "'. $today .'" and "'.$tomorrow.'"
                    group by keywordId
                    '));
            }
            else
            {
                $keywords=DB::select(DB::raw('
                    select keywordsearchlog.KeywordId as keywordId, count(*) as totalCount
                    from keywordsearchlog, contractorcategorykeyword
                    where contractorcategorykeyword.ContractorId="'.$cId.'" and contractorcategorykeyword.KeywordId=keywordsearchlog.KeywordId and keywordsearchlog.Date BETWEEN "'. $today .'" and "'.$tomorrow.'"
                    group by keywordsearchlog.keywordId
                    '));
            }

            $final=array();
            
            foreach($keywords as $keyword)
            {
                $keyword=json_decode(json_encode($keyword),true);
                
                $keyword['keywordName']=keyword::where('KeywordId',$keyword['keywordId'])->pluck('KeywordName')->first();

                $value=array();
                
                for($i=0;$i<24;$i++)
                {
                    $start_time=Carbon::today();
                    $start_time->addHours($i);
                    $end_time = Carbon::today();
                    $end_time->addHours($i+1);
                    
                    $count=count(keywordsearchlog::where('KeywordId',$keyword['keywordId'])->whereBetween('Date',[$start_time,$end_time])->get());
                    $value[$i]=$count;

                }

                $keyword['value']=$value;
                array_push($final,$keyword);

            }

            return $final;
            
        }

        /* Week */
        if($graphType == "W")
        {
            $dayOfWeek=$today;
            $FirstDay=$dayOfWeek->subDays(6);

            if($search_by == "all")
            {
                $keywords=DB::select(DB::raw('
                    select KeywordId as keywordId, count(*) as totalCount
                    from keywordsearchlog
                    where Date between "'.$FirstDay.'" and "'.$tomorrow.'"
                    group by keywordId
                    '));                
            }
            else
            {
                $keywords=DB::select(DB::raw('
                    select keywordsearchlog.KeywordId as keywordId, count(*) as totalCount
                    from keywordsearchlog, contractorcategorykeyword
                    where contractorcategorykeyword.ContractorId="'.$cId.'" and contractorcategorykeyword.KeywordId=keywordsearchlog.KeywordId and keywordsearchlog.Date between "'.$FirstDay.'" and "'.$tomorrow.'"
                    group by keywordsearchlog.keywordId
                    '));
            }

            $final=array();

            foreach($keywords as $keyword)
            {
                $keyword=json_decode(json_encode($keyword),true);
                
                $keyword['keywordName']=keyword::where('KeywordId',$keyword['keywordId'])->pluck('KeywordName')->first();

                $value=array();
                
                for($i=1;$i<=7;$i++)
                {
                    $start_time=Carbon::today();
                    $start_time->subDays(7-$i);

                    $end_time=Carbon::tomorrow();
                    $end_time->subDays(7-$i);
                    $end_time->subDays(0);

                    $count=count(keywordsearchlog::where('KeywordId',$keyword['keywordId'])->whereBetween('Date',[$start_time,$end_time])->get());
                    
                    $value[strtolower($start_time->format('l'))]=$count;
                    
                    
                }
                
                $keyword['value']=$value;

                array_push($final,$keyword);
            }

            return $final;
            
        }

        /* Month */
        if($graphType == "M")
        {
            $firstDay=Carbon::today();
            $firstDay->day(1);
            // return $firstDay;

            $lastDay=Carbon::today();
            $lastDay->addMonth(1);
            $lastDay->day(1);

            $days_in_month=Carbon::today()->daysInMonth;

            if($search_by == "all")
            {
                $keywords=DB::select(DB::raw('
                    select KeywordId as keywordId, count(*) as totalCount
                    from keywordsearchlog
                    where Date between "'.$firstDay.'" and "'.$lastDay.'"
                    group by keywordId
                    '));
            }
            else
            {
                $keywords=DB::select(DB::raw('
                    select keywordsearchlog.KeywordId as keywordId, count(*) as totalCount
                    from keywordsearchlog, contractorcategorykeyword
                    where contractorcategorykeyword.ContractorId="'.$cId.'" and contractorcategorykeyword.KeywordId=keywordsearchlog.KeywordId and keywordsearchlog.Date between "'.$firstDay.'" and "'.$lastDay.'"
                    group by keywordsearchlog.keywordId
                    '));
            }


            $final=array();
            
            foreach($keywords as $keyword)
            {
                $keyword=json_decode(json_encode($keyword),true);
                
                $keyword['keywordName']=keyword::where('KeywordId',$keyword['keywordId'])->pluck('KeywordName')->first();

                $value=array();
                // return $today->daysInMonth;
                
                for($i=1;$i<=$days_in_month;$i++)
                {
                    $start=Carbon::today()->day($i);
                    $end=Carbon::today()->day($i+1);

                    $count=count(keywordsearchlog::where('KeywordId',$keyword['keywordId'])->whereBetween('Date',[$start,$end])->get());

                    $value[$i]=$count;
                    
                }
                
                $keyword['value']=$value;

                array_push($final,$keyword);
            }

            return $final;

            
        }

    }

    public function saveContractorRatings(Request $request)
    {
        $buyerId = $request['buyerId'];
        $contractorId = $request['contractorId'];
        $contractorRatings = $request['contractorRatings'];
        $contractorReview = $request['contractorReview'];

        $entry = contractorrating::where('ContractorId',$contractorId)->where('BuyerInformationId',$buyerId)->get();

        if(count($entry)>0){

            contractorrating::where('ContractorId',$contractorId)->where('BuyerInformationId',$buyerId)->update([
                'ContractorRatings' => $contractorRatings,
                'ContractorReview' => $contractorReview,
                'LastModifiedAt' => Carbon::now()
                ]);

        }
        else{
            $rating = new contractorrating;

            $rating->ContractorId = $contractorId;
            $rating->BuyerInformationId = $buyerId;
            $rating->ContractorRatings = $contractorRatings;
            $rating->ContractorReview = $contractorReview;
            $rating->CreatedAt = Carbon::now();
            $rating->LastModifiedAt = Carbon::now();
            $rating->save();    
        }

        contactmeinfo::where('ContractorId',$contractorId)->where('BuyerId',$buyerId)->update([
            'Rated' => 1,
            'Mail' => 1
            ]);

        $averageRating = contractorrating::where('ContractorId',$contractorId)->avg('ContractorRatings');

        contractorinformation::where('ContractorId',$contractorId)->update([
            'AvgRating' => $averageRating
            ]);

        return "Ratings Recorded";

        

    }

    public function getContractorRatings(Request $request)
    {

        $cId = $request['cId'];
        $final = [];
        $ratings = contractorrating::where('ContractorId',$cId)->
        select('ContractorRatings as contractorRatings',
            'ContractorReview as contractorReview',
            'BuyerInformationId as buyerInformationId')->get();
        foreach ($ratings as $r) {
        $r['companyName'] = buyerinformation::where('BuyerInformationId',$r['buyerInformationId'])->pluck('CompanyName')->first();
        $r['buyerImage'] = aspnetusers::where('Id',$r['buyerInformationId'])->pluck('Image')->first();
        }

        return $ratings;
    }

    public function getTopRatings()
    {

     return  contractorinformation::select(
        'ContractorId as id',
        'CompanyName as companyName',
        'AvgRating as avgRating'
        )->orderBy('AvgRating','desc')->limit(50)->get();

 }


}

<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\keyword;
use App\buyerinformation;
use App\contractorinformation;
use App\contractorcategorykeyword;
use App\category;
use App\contractorpackages;
use App\certificates;
use App\sliderimages;
use App\mstlocation;
use App\aspnetusers;
use App\contractorlistinglog;
use Illuminate\Support\Facades\Input;

class ImagesController extends Controller {

	public function uploadCerti(Request $request, $name, $id)
	{

		$certiImage = certificates::where('id',$id)->pluck('url')->first();
		if($certiImage){
			unlink($certiImage);
			
		}

		$uploadpath = 'assets/images/certificates';
		$file = Input::file('attachment');
		$filename = $name.'.jpg';
		$file->move($uploadpath,$filename);
		chmod('assets/images/certificates/'.$filename, 0777);
		return $filename;
	}

	public function uploadSlideImage(Request $request, $ext,$name,$id)
	{
		$slideImage = sliderimages::where('id',$id)->pluck('url')->first();
		if($slideImage){
			unlink($slideImage);
			
		}

		$uploadpath = 'assets/images/homepageslider';
		$file = Input::file('attachment');
		$filename = $name.'.'.$ext;
		$file->move($uploadpath,$filename);
		chmod('assets/images/homepageslider/'.$filename, 0777);
		return $filename;
	}

	public function editCerti(Request $request)
	{

		certificates::where('id',$request['id'])->update([
			'title' => $request['title'],
			'url' => $request['url']
			]);
		return "Certifcate updated";
	}

	public function editSlide(Request $request)
	{

		sliderimages::where('id',$request['id'])->update([
			'title' => $request['title'],
			'url' => $request['url']
			]);
		return "Slider updated";
	}

	public function deleteCerti(Request $request)
	{	
		$certiImage = certificates::where('id',$request['id'])->pluck('url')->first();
		if($certiImage){
			unlink($certiImage);
		}
		certificates::find($request['id'])->delete();
		$array = explode('/',$certiImage);
		$name = $array[3];
		$ext =	explode('.',$name);
		preg_match('~certi-(.*?).'.$ext[1].'~', $name, $output);
		$certiname = $output[1];
		$contdata = contractorinformation::where('RegulatoryBodies','!=',null)->where('RegulatoryBodies','!=','')->select('ContractorId','RegulatoryBodies')->get();
		foreach ($contdata as $c) {
			$regubodies = explode(',',$c->RegulatoryBodies);
			$pos = array_search($certiname,$regubodies);
			unset($regubodies[$pos]);
			$commaRegu = implode(',',$regubodies);

			contractorinformation::where('ContractorId',$c->ContractorId)->update([
				"RegulatoryBodies" => $commaRegu
				]);
		}

		return "Certificate Deleted";
	}

	public function deleteSlide(Request $request)
	{	
		$slideImage = sliderimages::where('id',$request['id'])->pluck('url')->first();
		if($slideImage){

			unlink($slideImage);
		}

		sliderimages::find($request['id'])->delete();

		return "Slider Deleted";
	}

	public function addCerti(Request $request)
	{
		$certi = new certificates;
		$certi->title = $request['title'];
		$certi->url = $request['url'];
		$certi->save();
		return "Certificate Added";
	}

	public function addSlide(Request $request)
	{
		$slide = new sliderimages;
		$slide->title = $request['title'];
		$slide->url = $request['url'];
		$slide->save();
		return "Slider Added";
	}


	public function getAllImages(Request $request)
	{	
		return sliderimages::all();
		
	}

}

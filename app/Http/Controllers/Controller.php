<?php

namespace App\Http\Controllers;
ini_set("max_execution_time", 0);

use Laravel\Lumen\Routing\Controller as BaseController;

use Illuminate\Http\Request;
use App\aspnetusers;
use App\contractorinformation;
use App\keyword;
use App\buyerinformation;
use App\contractorcategorykeyword;
use App\category;
use App\contractorpackages;
use App\buyerkeywords;
use App\contactmeinfo;
use App\contractorlistinglog;
use App\keywordsearchlog;
use App\buyerkeyword;
use App\package;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    public function xyz()
    {
        $users=aspnetusers::all();
        $categories=category::all();
        $keywords=keyword::all();
        $packages=package::all();

        foreach($users as $user)
        {

            buyerinformation::where('BuyerInformationId',$user->Id)->update([
                'BuyerInformationId' => $user->user_id
            ]);

            buyerkeywords::where('BuyerId',$user->Id)->update([
                'BuyerId' => $user->user_id
            ]);

            contactmeinfo::where('BuyerId',$user->Id)->update([
                'BuyerId' => $user->user_id
            ]);

            contactmeinfo::where('ContractorId',$user->Id)->update([
                'ContractorId' => $user->user_id
            ]);

            contractorcategorykeyword::where('ContractorId',$user->Id)->update([
                'ContractorId' => $user->user_id
            ]);

            contractorinformation::where('ContractorId',$user->Id)->update([
                'ContractorId' => $user->user_id
            ]);

            contractorlistinglog::where('ContractorId',$user->Id)->update([
                'ContractorId' => $user->user_id
            ]);

            contractorpackages::where('ContractorId',$user->Id)->update([
                'ContractorId' => $user->user_id
            ]);

        }

        foreach($keywords as $keyword)
        {
            buyerkeywords::where('KeywordId',$keyword->KeywordId)->update([
                'KeywordId' => $keyword->Id
            ]);

            contractorcategorykeyword::where('KeywordId',$keyword->KeywordId)->update([
                'KeywordId' => $keyword->Id
            ]);

            contractorlistinglog::where('KeywordId',$keyword->KeywordId)->update([
                'KeywordId' => $keyword->Id
            ]);

            contractorpackages::where('KeywordId',$keyword->KeywordId)->update([
                'KeywordId' => $keyword->Id
            ]);

            keywordsearchlog::where('KeywordId',$keyword->KeywordId)->update([
                'KeywordId' => $keyword->Id
            ]);

        }

        foreach($categories as $category)
        {
            contractorcategorykeyword::where('CategoryId',$category->CategoryId)->update([
                'CategoryId' => $category->Id
            ]);

            

        }

        foreach($packages as $package)
        {
            contractorpackages::where('PackageId',$package->PackageId)->update([
                'PackageId' => $package->Id
            ]);

        }


    }

    public function buyerscene()
    {
        $buyerkeywords=buyerkeywords::all();
        foreach($buyerkeywords as $keyword)
        {
            $buyerkeyword=new buyerkeyword;
            $buyerkeyword->BuyerId=$keyword->BuyerId;
            $buyerkeyword->KeywordId=$keyword->KeywordId;
            $buyerkeyword->save();
        }
    }

    public function abcd()
    {
        $categories=category::all();
        foreach($categories as $category)
        {
            keyword::where('CategoryId',$category->CategoryId)->update([
                'CategoryId' => $category->Id
            ]);
        }
    }

    public function testing()
    {
        $x= contractorinformation::where('ContractorId',10)->pluck('CompanyLogo')->first();
        if($x != "")
        return "empty";
        else
        return "not";
    }

}

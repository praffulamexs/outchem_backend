<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class keyword extends Model {

    protected $table = "keyword";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    public $timestamps=false;

    // Relationships

}

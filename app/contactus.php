<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class contactus extends Model {

    protected $table = "contactus";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class buyerkeyword extends Model {

    protected $table = "buyerkeyword";

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;

    // Relationships

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class contractorrating extends Model {

    protected $table = "contractorrating";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

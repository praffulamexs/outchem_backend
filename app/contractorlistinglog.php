<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class contractorlistinglog extends Model {

    protected $table = "contractorlistinglog";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class mstlocation extends Model {

    protected $table = "mstlocation";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

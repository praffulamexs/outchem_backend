<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ranking extends Model {

    protected $table = "keywordranking";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    public $timestamps = false;
    // Relationships

}

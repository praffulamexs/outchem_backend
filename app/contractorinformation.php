<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class contractorinformation extends Model {

    protected $table = "contractorinformation";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

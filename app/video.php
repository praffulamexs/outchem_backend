<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class video extends Model {

    protected $table = "video";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

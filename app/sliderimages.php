<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class sliderimages extends Model {

    protected $table="sliderimages";

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

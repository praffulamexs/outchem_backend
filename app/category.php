<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model {

    protected $table = "category";
    
    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
    
    public $timestamps = false;
    
    // Relationships

}

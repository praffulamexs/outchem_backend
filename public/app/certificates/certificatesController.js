(function () {
  'use strict';

  angular
  .module('app')
  .controller('certificatesController', ['$rootScope','$scope','$state', 'UserService', 'dataService', 'toastr',
  function ($rootScope,$scope, $state, UserService, dataService, toastr) {
    var vm = this;
    dataService.getAllCertificates().then(
      function(response){
        $scope.certificates = response.data;
        console.log($scope.certificates);
          /*
          $scope.urls = [];

          for (var i = 0; i < $scope.certificates.length; i++) {
            $scope.urls[i] = $scope.certificates[0].url;
          }
          console.log($scope.urls)*/

      }, function(err){
        console.log(err);
      });
  }
  ])

})();

(function() {
    'use strict';

    angular
    .module('app')
    .controller('AdminDashboardController', AdminDashboardController);

    AdminDashboardController.$inject = ['$scope', '$window', '$uibModal', '$state', '$filter', 'dataService', '$timeout', 'UserService', 'toastr', 'bsLoadingOverlayService'];

    function AdminDashboardController($scope, $window, $uibModal, $state, $filter, dataService, $timeout, UserService, toastr, bsLoadingOverlayService) {
        var vm = this;
        vm.currentPage = 0;
        vm.pageSize = 5;

        vm.topThirtyKeywordHeaders = ["#", "Keyword", "Categories", "Rank", "Count", "Price"];
        vm.topThirtyCompaniesHeaders = ["Company Names", "Contacted"];
        vm.topCompaniesByRatingsHeader = ["Company Name", "Average Ratings"];

        activate();

        function activate() {
            bsLoadingOverlayService.start();
            vm.sampleProperty = "sampleValue";
            vm.user = UserService.getCurrentUser();
            console.log(vm.user);
            if (vm.user) {
                bsLoadingOverlayService.start();
              /*  dataService.getLoggedInUser(vm.user.accessToken).then(function(response) {
                    if (response.status === 200) {
                        getPackageListingData();
                        getTopThirtyKeywords();
                        getTopThirtyCompaniesContacted();
                        getTopContactedCompaniesByRating();
                        getUsersMapData();
                        getHitsMapData();
                    }
                });*/
                getPackageListingData();
                getTopThirtyKeywords();
                getTopThirtyCompaniesContacted();
                getTopContactedCompaniesByRating();
                getUsersMapData();
                getHitsMapData();

                bsLoadingOverlayService.stop();
            }
        }

        vm.numberOfPages = function() {
            if (vm.packageListingData && vm.packageListingData.length) {
                return Math.ceil(vm.packageListingData.length / vm.pageSize);
            }
        }

        function getPackageListingData() {
            dataService.getPackageSummary().then(function(response) {
                if (response.status === 200) {
                    vm.packageListingData = response.data;
                    vm.packageListingData.map(function(element) {
                        element.url = dataService.getImageUrl() + element.contractorId + '.jpg';
                        /*dataService.getUser(element.contractorId).then(function(response) {
                            element.userDetails = response.data;
                        });*/
                        dataService.getUserInfo(element.contractorId, 
                            function(data){
                                console.log(data);
                                element.userDetails = data;
                                // getTopThirtyKeywords();
                            }, function(err){
                                console.log(err);
                            })
                        var packageExpiryDate = moment(element.dateCreated).add(element.packageDays, 'days');
                        if (moment() > packageExpiryDate) {
                            element.packageDaysRemaining = -1;
                        } else {
                            element.packageDaysRemaining = packageExpiryDate.diff(moment().startOf('day'), "days");
                        }
                    });
                    console.log(vm.packageListingData);
                }
            });
        }

        function getTopThirtyKeywords() {
            dataService.getKeywordDetails(true).then(function(response) {
                vm.topKeywordsData = response.data;
                // getTopThirtyCompaniesContacted();

            })
        }

        function getTopThirtyCompaniesContacted() {
            dataService.getCompaniesContacted().then(function(response) {
                console.log(response.data);
                vm.topCompaniesData = response.data;
                // getTopContactedCompaniesByRating();

            });
        }

        vm.openBuyerDisplayModal = function(companyDetails) {
            vm.selectedCompany = companyDetails;
            vm.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/adminDashboard/buyerListModal.html',
                scope: $scope
            });
        }

        vm.deleteListedPackage = function(pkg) {
            console.log(pkg);
            var answer = $window.confirm("Are you sure you want to delete package ?");
            if (answer) {
                var dataToSend = {
                    "contractorId": pkg.contractorId,
                    "keywordId": pkg.keywordId,
                    "packageId": pkg.packageId,
                    "days": pkg.packageDays,
                    "location": pkg.location,
                    "contractorPackageId": pkg.contractorPackageId,
                    "savedPrice": pkg.savedPrice,
                    "savedTotalPrice": pkg.savedTotalPrice,
                    "delete": true
                };
                dataService.addContractorPackage(dataToSend).then(function(response) {
                    if (response.status === 200) {
                        toastr.success("Successfully deleted package!");
                        getPackageListingData();
                    } else {
                        toastr.error("Something went wrong ! " + response.status);
                    }
                })
            }
        }

        function getTopContactedCompaniesByRating() {
            dataService.getCompaniesContactedByRatings().then(function(response) {
                vm.topCompaniesRatingsData = response.data;
                console.log(vm.topCompaniesRatingsData);
                        // getUsersMapData();

            });
        }

        function getUsersMapData() {
            // Helpful Link 1 : http://jsbin.com/yolehoziqo/edit?html,output
            // Helpful Link 2 : https://github.com/dmachat/angular-datamaps

            dataService.getUsersCountryWise().then(function(response) {
                if (response.status === 200) {
                    var usersCountData = response.data.filter(function(element) {
                        if (element.country != null) {
                            return element;
                        }
                    });
                    var dataset = {};
                    vm.registerMapData = [];
                    var cumulatedData = {};
                    usersCountData.forEach(function(element) {
                        cumulatedData.totalCount = element.count + (cumulatedData.totalCount || 0);
                        cumulatedData.country = cumulatedData.country || element.country;
                        if (element.isContractor) {
                            cumulatedData.contractorCount = element.count;
                        } else {
                            cumulatedData.buyerCount = element.count;
                        }
                    })
                    vm.registerMapData.push(cumulatedData);
                    var onlyValues = vm.registerMapData.map(function(obj) {
                        return obj.totalCount;
                    });

                    var minValue = Math.min.apply(null, [0]),
                    maxValue = Math.max.apply(null, onlyValues);

                    var paletteScale = d3.scale.linear()
                    .domain([minValue, maxValue])
                        .range(["#EFEFFF", "#02386F"]); // blue color

                    vm.registerMapData.forEach(function(item) { //
                        // item example value ["USA", 70]
                        var iso = "IND",
                        value = item.totalCount;
                        dataset[iso] = { count: value, fillColor: paletteScale(value), buyerCount: item.buyerCount, contractorCount: item.contractorCount };
                    });

                    vm.usersMapConfig = {
                        scope: 'world',
                        projection: 'mercator',
                        // responsive: true,
                        options: {
                            width: 900,
                            legendHeight: 60 // optionally set the padding for the legend
                        },
                        geographyConfig: {
                            highlighBorderColor: '#EAA9A8',
                            highlighBorderWidth: 2,
                            popupTemplate: function(geo, data) {
                                // don't show tooltip if country don't present in dataset
                                if (!data) {
                                    return ['<div class="hoverinfo">',
                                    '<strong>', geo.properties.name, '</strong>', '</div>'
                                    ].join('');
                                }
                                // tooltip content
                                return ['<div class="hoverinfo">',
                                '<strong>', geo.properties.name, '</strong>',
                                '<br>Buyer Count: <strong>', data.buyerCount, '</strong>',
                                '<br>Contractor Count: <strong>', data.contractorCount, '</strong>',
                                '</div>'
                                ].join('');
                            }
                        },
                        fills: {
                            defaultFill: "#EFEFFF"
                        },
                        data: dataset
                    };
                    // getHitsMapData();
                }
            });

            // var dataset = {};
            // var series = [
            //     ["BLR", 75],
            //     ["BLZ", 43],
            //     ["RUS", 50],
            //     ["RWA", 88],
            //     ["SRB", 21],
            //     ["TLS", 43],
            //     ["REU", 21],
            //     ["TKM", 19],
            //     ["TJK", 60],
            //     ["ROU", 4],
            //     ["TKL", 44],
            //     ["GNB", 38],
            //     ["GUM", 67],
            //     ["GTM", 2],
            //     ["SGS", 95],
            //     ["GRC", 60],
            //     ["GNQ", 57],
            //     ["GLP", 53],
            //     ["JPN", 59],
            //     ["GUY", 24],
            //     ["GGY", 4],
            //     ["GUF", 21],
            //     ["GEO", 42],
            //     ["GRD", 65],
            //     ["GBR", 14],
            //     ["GAB", 47],
            //     ["SLV", 15],
            //     ["GIN", 19],
            //     ["GMB", 63],
            //     ["GRL", 56],
            //     ["ERI", 57],
            //     ["MNE", 93],
            //     ["MDA", 39],
            //     ["MDG", 71],
            //     ["MAF", 16],
            //     ["MAR", 8],
            //     ["MCO", 25],
            //     ["UZB", 81],
            //     ["MMR", 21],
            //     ["MLI", 95],
            //     ["MAC", 33],
            //     ["MNG", 93],
            //     ["MHL", 15],
            //     ["MKD", 52],
            //     ["MUS", 19],
            //     ["MLT", 69],
            //     ["MWI", 37],
            //     ["MDV", 44],
            //     ["MTQ", 13],
            //     ["MNP", 21],
            //     ["MSR", 89],
            //     ["MRT", 20],
            //     ["IMN", 72],
            //     ["UGA", 59],
            //     ["TZA", 62],
            //     ["MYS", 75],
            //     ["MEX", 80],
            //     ["ISR", 77],
            //     ["FRA", 54],
            //     ["IOT", 56],
            //     ["SHN", 91],
            //     ["FIN", 51],
            //     ["FJI", 22],
            //     ["FLK", 4],
            //     ["FSM", 69],
            //     ["FRO", 70],
            //     ["NIC", 66],
            //     ["NLD", 53],
            //     ["NOR", 7],
            //     ["NAM", 63],
            //     ["VUT", 15],
            //     ["NCL", 66],
            //     ["NER", 34],
            //     ["NFK", 33],
            //     ["NGA", 45],
            //     ["NZL", 96],
            //     ["NPL", 21],
            //     ["NRU", 13],
            //     ["NIU", 6],
            //     ["COK", 19],
            //     ["XKX", 32],
            //     ["CIV", 27],
            //     ["CHE", 65],
            //     ["COL", 64],
            //     ["CHN", 16],
            //     ["CMR", 70],
            //     ["CHL", 15],
            //     ["CCK", 85],
            //     ["CAN", 76],
            //     ["COG", 20],
            //     ["CAF", 93],
            //     ["COD", 36],
            //     ["CZE", 77],
            //     ["CYP", 65],
            //     ["CXR", 14],
            //     ["CRI", 31],
            //     ["CUW", 67],
            //     ["CPV", 63],
            //     ["CUB", 40],
            //     ["SWZ", 58],
            //     ["SYR", 96],
            //     ["SXM", 31],
            //     ["IND", 105]
            // ];

            // var onlyValues = series.map(function(obj) {
            //     return obj[1];
            // });

            // var minValue = Math.min.apply(null, onlyValues),
            //     maxValue = Math.max.apply(null, onlyValues);

            // var paletteScale = d3.scale.linear()
            //     .domain([minValue, maxValue])
            //     .range(["#EFEFFF", "#02386F"]); // blue color

            // series.forEach(function(item) { //
            //     // item example value ["USA", 70]
            //     var iso = item[0],
            //         value = item[1];
            //     dataset[iso] = { count: value, fillColor: paletteScale(value) };
            // });

            // vm.mapConfig = {
            //     scope: 'world',
            //     projection: 'mercator',
            //     // responsive: true,
            //     options: {
            //         width: 900,
            //         legendHeight: 60 // optionally set the padding for the legend
            //     },
            //     geographyConfig: {
            //         highlighBorderColor: '#EAA9A8',
            //         highlighBorderWidth: 2,
            //         popupTemplate: function(geo, data) {
            //             // don't show tooltip if country don't present in dataset
            //             if (!data) {
            //                 return ['<div class="hoverinfo">',
            //                     '<strong>', geo.properties.name, '</strong>', '</div>'
            //                 ].join('');
            //             }
            //             // tooltip content
            //             return ['<div class="hoverinfo">',
            //                 '<strong>', geo.properties.name, '</strong>',
            //                 '<br>Count: <strong>', data.count, '</strong>',
            //                 '</div>'
            //             ].join('');
            //         }
            //     },
            //     fills: {
            //         defaultFill: paletteScale(minValue)
            //     },
            //     data: dataset
            // };
        }

        function getHitsMapData() {
            dataService.getCountrywiseWebsiteHits().then(function(response) {
                if (response.status === 200) {
                    bsLoadingOverlayService.stop();
                    var dataset = {};
                    var series = [];
                    //Creating Series Array
                    response.data.forEach(function(element) {
                        var ar = [];
                        for (var key in element) {
                            ar.push(key.toLocaleUpperCase());
                            ar.push(element[key]);
                        }
                        series.push(ar);
                    });

                    var onlyValues = series.map(function(obj) {
                        return obj[1];
                    });

                    var minValue = 0,
                    maxValue = Math.max.apply(null, onlyValues);

                    var paletteScale = d3.scale.linear()
                    .domain([minValue, maxValue])
                        .range(["#EFEFFF", "#02386F"]); // blue color

                    series.forEach(function(item) { //
                        // item example value ["USA", 70]
                        var iso = item[0],
                        value = item[1];
                        dataset[iso] = { count: value, fillColor: paletteScale(value) };
                    });

                    vm.usersHitsMapConfig = {
                        scope: 'world',
                        projection: 'mercator',
                        // responsive: true,
                        options: {
                            width: 900,
                            legendHeight: 60 // optionally set the padding for the legend
                        },
                        geographyConfig: {
                            highlighBorderColor: '#EAA9A8',
                            highlighBorderWidth: 2,
                            popupTemplate: function(geo, data) {
                                // don't show tooltip if country don't present in dataset
                                if (!data) {
                                    return ['<div class="hoverinfo">',
                                    '<strong>', geo.properties.name, '</strong>', '</div>'
                                    ].join('');
                                }
                                // tooltip content
                                return ['<div class="hoverinfo">',
                                '<strong>', geo.properties.name, '</strong>',
                                '<br>Hits: <strong>', data.count, '</strong>',
                                '</div>'
                                ].join('');
                            }
                        },
                        fills: {
                            defaultFill: paletteScale(minValue)
                        },
                        data: dataset
                    };
                }
            })
        }
    }

    angular
    .module('app')
    .filter('startFrom', function() {
        return function(input, start) {
            if (input && input.length) {
                    start = +start; //parse to int
                    return input.slice(start);
                }
            }
        });
})();

(function () {
    angular
    .module('app')
    .factory('Utils', Utils);

    Utils.$inject = ['$q', '$sessionStorage'];
    function Utils($q, $sessionStorage) {
        return {
            isImage: isImage,
            isLocationRecorded : $sessionStorage.isLocationRecorded || false,
            setLocationRecorded : setLocationRecorded
        };

        function isImage(src) {
            var deferred = $q.defer();
            var image = new Image();
            image.onerror = function () {
                deferred.resolve(false);
            };
            image.onload = function () {
                deferred.resolve(true);
            };
            image.src = src;
            //console.log($sessionStorage);
            return deferred.promise;
        }
        
        function setLocationRecorded() {
            if (!$sessionStorage.isLocationRecorded) {
                $sessionStorage.isLocationRecorded = true;
            } else{
                throw new Error("Someone is trying to manipulate session storage!");
            }
        }
    }
})();

(function () {
  'use strict';
  angular
    .module('app')
    .controller('BuyerRegController', BuyerRegController);

  BuyerRegController.$inject = ['$rootScope', '$uibModal', '$state', '$scope', 'dataService', 'bsLoadingOverlayService', 'toastr'];

  function BuyerRegController($rootScope, $uibModal, $state, $scope, dataService, bsLoadingOverlayService, toastr) {
    var vm = this;
    vm.contract = {};
    vm.cities = [
      {
        cityID: 1, name: "Mumbai"
      },
      {
        cityID: 2, name: "Delhi"
      },
      {
        cityID: 3, name: "Kolkota"
      },
      {
        cityID: 4, name: "Chennai"
      },
      {
        cityID: 5, name: "Banglore"
      },
      {
        cityID: 6, name: "Pune"
      },
      {
        cityID: 7, name: "Hyderabad"
      }
    ];
    vm.countries = [
      {
        countryID: 1, name: "India"
      }
    ];

    vm.contract.City = vm.cities[0].name;
    vm.contract.Country = vm.countries[0].name;
    vm.contract.IsContractor = 0;

    function doRegistration() {
      bsLoadingOverlayService.start();
      console.log(vm.contract);
      dataService.register(vm.contract).then(function (resp) {
        if (resp.status === 200) {
          if(resp.data=="Email Is Already Taken"){
            toastr.error(resp.data);
            $rootScope.$broadcast('registrationFailed');
          }
          else{
            $rootScope.$broadcast('registered');
            $state.go('app');  
          }
          } else if (resp.status === 500) {
          toastr.error(resp.data);
        } else {
          $rootScope.$broadcast('registrationFailed');
        }
        bsLoadingOverlayService.stop();
      });
    }

    vm.validateForm = function (_input) {
      var mobileRgx = /^[0]?[789]\d{9}$/;
      var emailRgx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if (_input === 'email') {
        if (emailRgx.test(vm.contract.email)) {
          vm.emailError = false;
        } else {
          vm.emailError = true;
        }
      }
      if (_input === 'phone') {
        if (mobileRgx.test(vm.contract.phone)) {
          vm.phoneError = false;
        } else {
          vm.phoneError = true;
        }
      }
      if (_input === 'passwrd') {
        if (vm.contract.password.length > 7) {
          vm.passwordError = false;
        } else {
          vm.passwordError = true;
        }
      }
      if (_input === 'cnfrmPass') {
        if (vm.contract.confPassword === vm.contract.password) {
          vm.confPasswordError = false;
        } else {
          vm.confPasswordError = true;
        }
      }
    };

    vm.submitForm = function () {
      if (angular.isUndefined(vm.contract.FirstName) || vm.contract.FirstName === null || vm.contract.FirstName === '') {
        vm.FirstNameError = true;
      } else {
        vm.FirstNameError = false;
      }
      if (angular.isUndefined(vm.contract.LastName) || vm.contract.LastName === null || vm.contract.LastName === '') {
        vm.LastNameError = true;
      } else {
        vm.LastNameError = false;
      }
      if (angular.isUndefined(vm.contract.companyName) || vm.contract.companyName === null || vm.contract.companyName === '') {
        vm.companyNameError = true;
      } else {
        vm.companyNameError = false;
      }
      if (angular.isUndefined(vm.contract.email) || vm.contract.email === null || vm.contract.email === '') {
        vm.emailError = true;
      } else {
        vm.emailError = false;
      }
      if (angular.isUndefined(vm.contract.phone) || vm.contract.phone === null || vm.contract.phone === '') {
        vm.phoneError = true;
      } else {
        vm.phoneError = false;
      }
      if (angular.isUndefined(vm.contract.password) || vm.contract.password === null || vm.contract.password === '') {
        vm.passwordError = true;
      } else {
        vm.passwordError = false;
      }
      if (angular.isUndefined(vm.contract.confPassword) || vm.contract.confPassword === null || vm.contract.confPassword === '') {
        vm.confPasswordError = true;
      } else {
        vm.confPasswordError = false;
      }

      if (!vm.companyNameError && !vm.FirstNameError && !vm.LastNameError && !vm.emailError && !vm.passwordError && !vm.phoneError && !vm.confPasswordError && vm.term) {
        doRegistration();
      }
    };

    vm.trmCondPopUp = function () {
      vm.modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/misc/terms.html',
                    scope: $scope
                });
    };
  }
})();

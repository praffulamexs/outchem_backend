(function () {
  'use strict';

  angular
  .module('app')
  .service('dataService', dataService);

  dataService.$inject = ['$http', 'toastr', 'bsLoadingOverlayService'];

  function dataService($http, toastr, bsLoadingOverlayService) {
    // var BASE_URL = "https://services.outchem.com/";
    // var BASE_URL = "http://localhost:8010/";
    var BASE_URL = "http://ec2-13-126-14-209.ap-south-1.compute.amazonaws.com/";
    var BASE_URL_API = "api/v1/";
    var registrationUrl = BASE_URL + BASE_URL_API + "login/register";
    // Login And Authorization
    //var identityUrl = "https://localhost:44365/identity/connect/";
    var identityUrl = "https://identity.outchem.com/identity/connect/";
    var tokenUrl = identityUrl + "token";

    // Reveal Module Pattern is used
    return {
      getImageUrl: getImageUrl,
      doLogin: doLogin,
      getUserInfo: getUserInfo,
      getKeyword: getKeyword,
      getKeywordsByCategoryName: getKeywordsByCategoryName,
      getKeywordById: getKeywordById,
      getKeywordBySearchKey: getKeywordBySearchKey,
      getKeywordCategoryWise: getKeywordCategoryWise,
      saveKeyword: saveKeyword,
      // logKeywordSearch: logKeywordSearch,
      findContractor: findContractor,
      findBuyer: findBuyer,
      getPackage: getPackage,
      getPackageById: getPackageById,
      savePackage: savePackage,
      getPackageListing: getPackageListing,
      getPackageListingById: getPackageListingById,
      savePackageListing: savePackageListing,
      getCategories: getCategories,
      getCategoryByID: getCategoryByID,
      saveCategories: saveCategories,
      saveBuyers: saveBuyers,
      saveBuyerInformation: saveBuyerInformation,
      getBuyerInformation: getBuyerInformation,
      saveContactUs: saveContactUs,
      saveContractors: saveContractors,
      getCount: getCount,
      register: register,
      addCategoryKeywords: addCategoryKeywords,
      addContractorPackage: addContractorPackage,
      getContractor: getContractor,
      getContractorProfile: getContractorProfile,
      getContractorProgress: getContractorProgress,
      verifyEmail: verifyEmail,
      getFacilities: getFacilities,
      verifyPhone: verifyPhone,
      verifyPhoneCode: verifyPhoneCode,
      forgetPassword: forgetPassword,
      resetPassword: resetPassword,
      getUser: getUser,
      getLoggedInUser: getLoggedInUser,
      imageUpload: imageUpload,
      getImage: getImage,
      updateCategories : updateCategories,
      deleteCategories : deleteCategories,
      getAllKeywords: getAllKeywords,
      getKeywordDetails : getKeywordDetails,
      updateKeyword : updateKeyword,
      deleteKeyword : deleteKeyword,
      getPricing : getPricing,
      updatePricing : updatePricing,
      getTrendingKeywordsData : trendingKeywordsData,
      getContractorRatings : getContractorRatings,
      saveContractorRatings : saveContractorRatings,
      updatePackage : saveEditPackage,
      deletePackage : deletePackage,
      contactMeInfo: contactMeInfo,
      logContractorListing: logContractorListing,
      getContractorPackage: getContractorPackage,
      getKeywordsUsageData : getKeywordsUsageData,
      getCompaniesContacted : getCompaniesContacted,
      getKeywordsPrice : getKeywordsPrice,
      getPackageSummary : getPackageSummary,
      getCompaniesContactedByRatings : getCompaniesContactedByRatings,
      getLocationExternal : getLocationExternal,
      setLocation : setLocation,
      getLocation : getLocation,
      saveFacility : saveFacility,
      editFacility : editFacility,
      deleteFacility: deleteFacility,
      getAffiliations : getAffiliations,
      editAffiliation : editAffiliation,
      saveAffiliation : saveAffiliation,
      deleteAffiliation : deleteAffiliation,
      getUsersCountryWise : getUsersCountryWise,
      getCountrywiseWebsiteHits : getCountrywiseWebsiteHits,
      getRemindersList : getRemindersList,
      reviewReminder : reviewReminder,
      editUser: editUser,
      // service created by praveen
      checkBalance: checkBalance,
      sendSMS: sendSMS,
      changePassword: changePassword,
      getTopServices: getTopServices,
      imageSave : imageSave,
      uploadVideo: uploadVideo,
      getVideo : getVideo,
      getAllCertificates: getAllCertificates,
      uploadCertificate : uploadCertificate,
      editCertificate : editCertificate,
      deleteCertificate : deleteCertificate,
      saveCertificate : saveCertificate,
      getContractors: getContractors,
      getBuyers: getBuyers,
      getServices: getServices,
      setTopService: setTopService,
      getTopContractors: getTopContractors,
      setTopContractor: setTopContractor,
      getTopCompanies: getTopCompanies,
      setBasePrice: setBasePrice,
      
      getImageSlider : getImageSlider,
      uploadSliderImage : uploadSliderImage,
      deleteSliderImage : deleteSliderImage,
      editSliderImage : editSliderImage,
      saveSliderImage : saveSliderImage,

      sendRatingMail : sendRatingMail
    };

    // INTERNAL FUNCTIONS

    function makeLoginData(dataToSend) {
      var data = {
        username: dataToSend.userId,
        password: dataToSend.userPassword,
        grant_type: "password",
        scope: "ocAPI openid profile email"
      };
      var body = "";
      for (var key in data) {
        if (body.length) {
          body += "&";
        }
        body += key + "=";
        body += encodeURIComponent(data[key]);
      }
      console.log(body);
      return body;
    }

    // END INTERNAL FUNCTIONS

    // API SERVICE EXPOSED FUNCTIONS

    /*function doLogin(dataToSend) {
      return $http({
        method: 'POST',
        url: tokenUrl,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic b3V0Y2hlbS53ZWJzaXRlOm91dGNoZW1zZWNyZXQ='
        },
        transformRequest: function () {
          return makeLoginData(dataToSend);
        },
        data: ''
      }).then(function (response) {
        console.log(response);
        if (response.status === 200 && response.data.access_token) {
          return response.data;
        }
      }, getAvengersFailed);
    }*/

    function doLogin(loginData,onSuccess,onError){
     
     return $http({
      method: 'POST',
      url: BASE_URL+BASE_URL_API+'login/check',
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      },
      data:loginData
    }).then(function (response) {
      onSuccess(response.data);
    }, getAvengersFailed); 
  }

  function getImageUrl() {
    // return "https://s3.ap-south-1.amazonaws.com/outchem/images/";
    // return "https://s3.ap-south-1.amazonaws.com/outchem-new/images/";
    return "assets/images/profiles/";
  }

  function getTopCompanies(){
    var imageUrl = getImageUrl();
    return $http.get(BASE_URL + BASE_URL_API + 'getTopCompanies', {
      params : {
        imageUrl: imageUrl
      }
    });
  }

  /*function getUserInfo(accessToken) {
    return $http({
      method: 'GET',
      url: BASE_URL + BASE_URL_API + 'userservice',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + accessToken
      },
      data: ''
    }).then(function (response) {
      if (response.status === 200) {
        return response.data;
      }
    }, getAvengersFailed);
  }*/

  function getUserInfo(userId, onSuccess, onError) {
    return $http({
      method: 'GET',
      url: BASE_URL + BASE_URL_API + 'login/'+userId,
    }).then(function (response) {
      onSuccess(response.data);
    }, getAvengersFailed);
  }

  function register(registrationData) {
    return $http.post(registrationUrl, registrationData);
  }

  function getAvengersFailed(response) {
    toastr.error("Something Went Wrong. Check Console Log.", "Error");
    console.log(response);
    bsLoadingOverlayService.stop();
  }

  function getKeyword(unCategorized) {
    return $http.get(BASE_URL + BASE_URL_API + 'keyword', {
      params: {
        unCategorized: unCategorized ? unCategorized : false
      }
    });
  }

  function getKeywordsByCategoryName(cName) {
    return $http.get(BASE_URL + BASE_URL_API + 'keyword/GetKeywordsByCategoryName', {
      params: {
        cName: cName
      }
    });
  }

  function getKeywordById(kId) {
    return $http.get(BASE_URL + BASE_URL_API + 'keyword/GetKeywordById', {
      params: {
        kId: kId
      }
    });
  }

  /*function verifyEmail(userId, code) {
    return $http.post(BASE_URL + BASE_URL_API + 'userservice/ConfirmEmail?userId=' + userId +
      "&code=" + window.encodeURIComponent(code));
    }*/

    function verifyEmail(userId, code) {
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/ConfirmEmail?userId=' + userId +
        "&code=" + code);
    }

    

    function getKeywordBySearchKey(searchKey, cId) {
      return $http.get(BASE_URL + BASE_URL_API + 'keyword', {
        params: {
          searchKey: searchKey,
          cId: cId ? cId : '0'
        }
      });
    }



    function getKeywordCategoryWise(searchKey, select, kId) {
      return $http.get(BASE_URL + BASE_URL_API + 'keyword/GetKeywordCategoryWise', {
        params: {
          searchKey: searchKey,
          select: select ? select : false,
          _kId: kId
        }
      });
    }

    function saveKeyword(data) {
      console.log(data);
      return $http.post(BASE_URL + BASE_URL_API + 'admin/keyword/save', data);
    }

    function getPackage() {
      return $http.get(BASE_URL + BASE_URL_API + 'contractor/package');
    }

    function getPackageById(pId) {
      return $http.get(BASE_URL + BASE_URL_API + 'package/GetPackageById', {
        params: {
          pId: pId
        }
      });
    }

    function savePackage(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'admin/package', data);
    }

    function getPackageListing() {
      return $http.get(BASE_URL + BASE_URL_API + 'packageListing');
    }

    function getPackageListingById(UserID) {
      return $http.get(BASE_URL + BASE_URL_API + 'packageListing/GetListingPackageByUserID', {
        params: {
          UserID: UserID
        }
      });
    }

    function savePackageListing(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'packageListing', data);
    }

    function getCategories() {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/categories');
    }

    function getCategoryByID(id) {
      return $http.get(BASE_URL + BASE_URL_API + 'categories/' + id);
    }

    function saveCategories(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'admin/categories/save', data);
    }

    function saveBuyers(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'buyers', data);
    }

    function saveBuyerInformation(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'buyer/buyerinformation', data);
    }

    function saveContactUs(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'contactus', data);
    }

    function getContractor(contractorId) {
      return $http.get(BASE_URL + BASE_URL_API + 'contractors/'+contractorId);
    }

    function getContractorProfile(cId) {
      return $http.get(BASE_URL + BASE_URL_API + 'login/contractors/GetContractorProfile', {
        params: {
          cId: cId
        }
      });
    }

    function saveContractors(data) {
      console.log(data);
      return $http.post(BASE_URL + BASE_URL_API + 'contractor/saveData', data);
    }

    function addCategoryKeywords(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractor/AddContractorCategoryKeywords', data);
    }

    function addContractorPackage(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractor/addContractorPackage', data);
    }

    function getContractorProgress(id) {
      return $http.get(BASE_URL + BASE_URL_API + 'contractor/getContractorProgress/'+id);
    }

    function getCount() {
      return $http.get(BASE_URL + BASE_URL_API + 'count/GetCount');
    }

    function findContractor(searchKey, isPaid, location) {
      var number;
      console.log(searchKey);
      console.log(isPaid);
      console.log(location);
      if(isPaid == true){
       number = 1;
     }
     else{
       number = 0;
     }
     return $http.get(BASE_URL + BASE_URL_API + 'keyword/FindContractor', {
      params: {
        searchKey: searchKey,
        paid: isPaid || false,
        location: location || 0,
        number: number
      }
    });
   }

   function findBuyer(searchKey) {
    return $http.get(BASE_URL + BASE_URL_API + 'admin/keyword/FindBuyer',{
      params:{
        searchKey : searchKey
      }
    });
  }

  function getBuyerInformation(id) {
    return $http.get(BASE_URL + BASE_URL_API + 'buyer/buyerinformation/'+id);
  }

  function getFacilities() {
    return $http.get(BASE_URL + BASE_URL_API + 'contractor/facilities');
  }

  function verifyPhone(phone, accessToken) {
      // $http.defaults.headers.common.Authorization = 'Bearer ' + accessToken;
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/VerifyPhone', {
        params: {phoneNumber: phone},
        headers: {Authorization: 'Bearer ' + accessToken}
      });
    }

    function verifyPhoneCode(phone, otp, accessToken) {
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/VerifyPhoneCode', {
        params: {phoneNumber: phone, code: otp},
        headers: {Authorization: 'Bearer ' + accessToken}
      });
    }

    function getUser(contId) {
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/GetUser', {
        params: {
          userId: contId
        }
      });
    }

    function getLoggedInUser(accessToken) {
      // $http.defaults.headers.common.Authorization = 'Bearer ' + accessToken;
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/getLoggedInUser', {
        headers: {Authorization: 'Bearer ' + accessToken}
      });
    }

    function getImage(url, accessToken) {
      return $http.get(url, {
        headers: {Authorization: 'Bearer ' + accessToken}
      });
    }

    function forgetPassword(userID) {
      // return $http.post(BASE_URL + BASE_URL_API + 'userservice/ForgotPassword?email=' + userID);
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/ForgotPassword?userId=' + userID);
    }

    /*function editUser(objUser) {
      return $http.post(BASE_URL + BASE_URL_API + 'userservice/edit' , objUser, {
        headers: {Authorization: 'Bearer ' + objUser.accessToken}
      });
    }*/

    function editUser(objUser) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractor/userservice/edit' , objUser);
    }

    /*function resetPassword(userID, code, password) {
      return $http.post(BASE_URL + BASE_URL_API + 'userservice/ResetPassword?userId=' + userID +
        '&code=' + window.encodeURIComponent(code) + '&password=' + window.encodeURIComponent(password));
      }*/

      function resetPassword(resetPassobj) {
        return $http.post(BASE_URL + BASE_URL_API + 'userservice/ResetPassword',resetPassobj);
      }

    /*function logKeywordSearch(kId) {
      return $http.post(BASE_URL + BASE_URL_API + 'keyword/LogKeywordSearch?kId=' + kId);
    }*/

    function imageUpload(data) {
      console.log(data);
      var dataAsFormData = new FormData();
      dataAsFormData.append("attachment", data);
      console.log(dataAsFormData);
      return $http({
        url: BASE_URL + BASE_URL_API + 'imageservice',
        method: 'POST',
        data: dataAsFormData,
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      });
    }

    function uploadCertificate(data,certiname, certiId) {
      console.log(data);
      var dataAsFormData = new FormData();
      dataAsFormData.append("attachment", data);
      console.log(dataAsFormData);
      return $http({
        url: BASE_URL + BASE_URL_API + 'uploadCertificate/'+certiname+'/'+certiId,
        method: 'POST',
        data: dataAsFormData,
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      });
    }

    function uploadSliderImage(data,slidename,ext,slideId) {
      console.log(data);
      var dataAsFormData = new FormData();
      dataAsFormData.append("attachment", data);
      console.log(dataAsFormData);
      return $http({
        url: BASE_URL + BASE_URL_API + 'uploadSliderImage/'+slidename+'/'+ext+'/'+slideId,
        method: 'POST',
        data: dataAsFormData,
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      });
    }

    function editCertificate(data){
      return $http.post(BASE_URL + BASE_URL_API + 'editCertificate',data);

    }

    function editSliderImage(data){
      return $http.post(BASE_URL + BASE_URL_API + 'editSliderImage',data);

    }

    function deleteCertificate(data){
      return $http.post(BASE_URL + BASE_URL_API + 'deleteCertificate',data);

    }

    function deleteSliderImage(data){
      return $http.post(BASE_URL + BASE_URL_API + 'deleteSliderImage',data);

    }

    function saveCertificate(data){
      return $http.post(BASE_URL + BASE_URL_API + 'addCertificate',data);

    }

    function saveSliderImage(data){
      return $http.post(BASE_URL + BASE_URL_API + 'addSliderImage',data);

    }



    function imageSave(data){

      return $http.post(BASE_URL + BASE_URL_API + 'imagesave',data);

    }

    function deleteCategories(cid){
      return $http.get(BASE_URL + BASE_URL_API + 'admin/categories' + '/Delete',{
        params : {
          cId : cid
        }
      });
    }

    function updateCategories(data){
      return $http.post(BASE_URL + BASE_URL_API + 'admin/categories' + '/Edit',data);
    }

    function getAllKeywords(){
      return $http.get(BASE_URL + BASE_URL_API + 'admin/keyword');
    }

    function getKeywordDetails(){
      return $http.get(BASE_URL + BASE_URL_API + 'admin/keyword' + '/GetKeywordAdmin',{
        // params : {
        //   top : 30
        // }
      });
    }

    function updateKeyword (data) {
      console.log(data);
      return $http.post(BASE_URL + BASE_URL_API + 'admin/keyword' + '/Edit', data);
    }

    function deleteKeyword (kid) {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/keyword' + '/Delete',{
        params : {
          kId : kid
        }
      });
    }

    function getPricing () {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/PriceModel');
    }

    function updatePricing(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'admin/PriceModel', data);
    }

    function trendingKeywordsData (format, search_by, fromDate,cId) {
      return $http.get(BASE_URL + BASE_URL_API + 'contractor/Count/GetTrendingKeyword',{
        params: {
          graphType: format,
          search_by: search_by,
          graphDate: fromDate,
          cId: cId
        }
      });
    }

    function getContractorRatings (contId) {
      return $http.get(BASE_URL + BASE_URL_API + 'contractor/getrating?cId=' + contId);
    }

    function saveContractorRatings (rating) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractor/rating', rating);
    }

    function saveEditPackage (data) {
      return $http.post(BASE_URL + BASE_URL_API + 'admin/package/edit', data);
    }

    function deletePackage (data) {
      return $http.post(BASE_URL + BASE_URL_API + 'admin/package/delete', data);
    }

    /*function logKeywordSearch(kId) {
      return $http.post(BASE_URL + BASE_URL_API + 'keyword' + '/LogKeywordSearch?kid=' + kId);
    }*/

    function contactMeInfo(contactData) {
      return $http.post(BASE_URL + BASE_URL_API + 'buyer/ContactMeInfo', contactData);
    }

    function logContractorListing (data) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractor/ContractorListingLog', data);
    }

    function getContractorPackage () {
      return $http.get(BASE_URL + BASE_URL_API + 'contractors/GetContractorPackage');
    }

    function getKeywordsUsageData (keywordId, startDate, endDate) {
      return $http.get(BASE_URL + BASE_URL_API + 'count/GetContractorListing',{
        params:{
          kId: keywordId,
          startDt : startDate,
          EndDt: endDate
        }
      });
    }

    function getCompaniesContacted () {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/count/TopContactedContractor');
    }

    function getKeywordsPrice (keywordId) {
      return $http.get(BASE_URL + BASE_URL_API + 'contractor/keyword/GetKeywordsPrice',{
        params:{
          kId: keywordId
        }
      })
    }

    function getPackageSummary () {
      return $http.get(BASE_URL + BASE_URL_API + 'count/PackageSummaryAllContractor');
    }

    function getCompaniesContactedByRatings () {
      return $http.get(BASE_URL + BASE_URL_API + 'contractor/GetTopContractorRating');
    }

    function getLocationExternal() {
      return $http.get('http://ip-api.com/json');
    }

    function setLocation(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'Location',data);
    }

    function getLocation(cCode, rCode) {
      return $http.get(BASE_URL + BASE_URL_API + 'Location',{
        params: {
          countryCode : cCode || null,
          regionCode : rCode || null
        }
      })
    }

    function saveFacility (facilityToSave) {
      return $http.post(BASE_URL + BASE_URL_API + 'admin/facility/save', facilityToSave);
    }

    function editFacility (facilityToEdit) {
      return $http.post(BASE_URL + BASE_URL_API + 'admin/facility/edit', facilityToEdit);
    }

    function deleteFacility (data) {
      return $http.post(BASE_URL + BASE_URL_API + 'admin/facility/delete',data);
    }

    function getAffiliations () {
      return $http.get(BASE_URL + BASE_URL_API + 'contractor/AffAssociation');
    }

    function editAffiliation (affilitationToEdit) {
      return $http.post(BASE_URL + BASE_URL_API + 'admin/AffAssociation/edit', affilitationToEdit);
    }

    function saveAffiliation (affiliationToSave) {
      return $http.post(BASE_URL + BASE_URL_API + 'admin/AffAssociation/save', affiliationToSave);
    }

    function deleteAffiliation (aId) {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/AffAssociation/delete?aId=' + aId);
    }

    function getUsersCountryWise() {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/userservice/UserCountCountrywise');
    }

    function getCountrywiseWebsiteHits () {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/count/CountrywiseWebsiteHits');
    }

    function getRemindersList() {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/count/PendingReviewList');
    }

    function reviewReminder(reminderToSend) {
      return $http.post(BASE_URL + BASE_URL_API + 'buyer/ReviewRemainder',reminderToSend);
    }
    // END API SERVICE EXPOSED FUNCTIONS


    function getAllCertificates() {
      return $http.get(BASE_URL + BASE_URL_API + 'certificates');
    }
    function getImageSlider() {
      return $http.get(BASE_URL + BASE_URL_API + 'sliderimages');
    }

    function checkBalance(){
      return $http.get('http://www.webpostservice.com/sendsms/checkbalance.php?username=amexsInt&password=435187');
    }

    function sendSMS(msg,mobile){
      return $http.get('http://www.webpostservice.com/sendsms/sendsms.php?username=amexsInt&password=435187&type=TEXT&sender=ITLHGO&mobile=' + mobile + '&message=' + msg);
    }

    function changePassword(CPdata) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractor/changePassword',CPdata);
    }

    function getServices() {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/getServices');
    }

    function setTopService(service) {
      console.log("here");
      return $http.get(BASE_URL + BASE_URL_API + 'admin/setTopService', {
        params : {
          serviceId : service.KeywordId
        }
      });
    }

    function setTopContractor(contractor) {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/setTopContractor', {
        params : {
          contractorId: contractor.ContractorId
        }
      });
    }

    function getTopContractors() {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/getTopContractors')
    }

    // function topServices() {
    //   return $http.get(BASE_URL + BASE_URL_API + 'topServices');
    // }

    function getTopServices(){
      return $http.get(BASE_URL + BASE_URL_API + 'topServices');
    }

    function uploadVideo(video)
    {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/video',{
        params : {
          video : video
        }
      });
    }
    
    function getContractors() {
      return $http.get(BASE_URL + BASE_URL_API + 'getContractors');
    }

    function getBuyers() {
      return $http.get(BASE_URL + BASE_URL_API + 'getBuyers');
    }

    function getVideo()
    {
      // console.log($http.get(BASE_URL + BASE_URL_API + 'admin/getVideo'));
      return $http.get(BASE_URL + BASE_URL_API + 'admin/getVideo');
    }

    function sendRatingMail(buyersData){
      return $http.post(BASE_URL + BASE_URL_API + 'buyer/sendRatingMail',buyersData);

    }

    function setBasePrice(basePrice) {
      return $http.get(BASE_URL + BASE_URL_API + 'admin/setBasePrice', {
        params: {
          basePrice: basePrice
        }
      });
    }

  }
})();

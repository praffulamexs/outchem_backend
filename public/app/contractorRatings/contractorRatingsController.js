(function() {
    'use strict';

    angular
        .module('app')
        .controller('contractorRatingsController', contractorRatingsController);

    contractorRatingsController.$inject = ['$scope', '$uibModal', '$state', '$location', 'bsLoadingOverlayService', 'dataService', 'UserService', 'toastr'];

    function contractorRatingsController($scope, $uibModal, $state, $location, bsLoadingOverlayService, dataService, UserService, toastr) {
        var vm = this;
        vm.max = 5;
        vm.rate = undefined;
        vm.isReadonly = false;
        vm.overStar = 0;
        vm.contractorId = $location.search().contractorId;

        activate();

        vm.hoveringOver = function(value) {

            vm.overStar = value;
            vm.percent = 100 * (value / vm.max);
            if (vm.percent <= 40) {
                vm.spell = "Bad";
            } else if (vm.percent > 40 && vm.percent <= 80) {
                vm.spell = "Average";
            } else if (vm.percent > 80) {
                vm.spell = "Good";
            }
        };


        function activate() {
            bsLoadingOverlayService.start();
            vm.user = UserService.getCurrentUser();
            console.log(vm.user);
            if (vm.user) {
                /*dataService.getLoggedInUser(vm.user.accessToken).then(function(response) {
                    if (response.status === 200) {

                        updateBuyerInfo();
                        updateContractorInfo();
                        bsLoadingOverlayService.stop();
                    }
                });*/
                dataService.getUserInfo(vm.user.id,
                    function(resp){
                        console.log(resp.data);
                        updateBuyerInfo();
                        updateContractorInfo();
                        bsLoadingOverlayService.stop();
                    },function(err){
                        console.log(err)
                        bsLoadingOverlayService.stop();
                    })

            } else {

                bsLoadingOverlayService.stop();
                $uibModal.open({
                    animation: true,
                    templateUrl: '/app/login/loginModal.html',
                    controller: "LoginController",
                    controllerAs: "loginController"
                }).result.then(function (data) {
                    if (data !== 'close') {
                        $state.go('contractorRatings', {contractorId: $location.search().contractorId}, {reload: true});
                    }
                }, function () {
                    // $location.path('/login')
                });
            }

        }

        function updateBuyerInfo() {
            dataService.getBuyerInformation(vm.user.id).then(function(resp) {
                console.log(resp.data);
                vm.buyerInfo = resp.data;
            }, function(err) {
                console.log('Error In getContractor', err);
            });
        }

        function updateContractorInfo() {
            dataService.getContractorProfile(vm.contractorId).then(function(resp) {
                console.log(resp.data);
                vm.contractorInfo = resp.data[0];
            });
        }

        vm.save = function(ratings, reviews) {
            if (!ratings || !reviews) {
                toastr.warning("Either Ratings or Reviews or both are missing.");
                return;
            }
            if (reviews && reviews.length < 5) {
                toastr.warning("Please elaborate more on Reviews. Reviews should exceed 5 characters.");
                return;
            }
            if (!vm.user) {
                $uibModal.open({
                    animation: true,
                    templateUrl: '/app/login/loginModal.html',
                    controller: "LoginController",
                    controllerAs: "loginController"
                });
            }else{
                bsLoadingOverlayService.start();
                var rating = {
                    contractorId: vm.contractorInfo.contractorId,
                    buyerId: vm.buyerInfo.buyerId,
                    contractorRatings: ratings,
                    contractorReview: reviews
                };
                console.log(rating);
                dataService.saveContractorRatings(rating).then(function (response) {
                    if(response.status === 200){
                        toastr.info("Thank you for your ratings");
                        vm.rate = null;
                        vm.rateReviews = "";
                        bsLoadingOverlayService.stop();
                        $state.go('app');

                    }else{
                        toastr.error("Something went wrong!! " + response.status);
                        bsLoadingOverlayService.stop();
                    }
                });
            }
        }
    }
})();

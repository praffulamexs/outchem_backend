(function() {
    'use strict';

    angular
        .module('app')
        .controller('ChangePasswordController', ChangePasswordController);

    ChangePasswordController.$inject = ['$state', '$window', '$scope', '$rootScope', 'dataService', 'bsLoadingOverlayService', 'toastr', 'UserService'];

    function ChangePasswordController($state, $window, $scope, $rootScope, dataService, bsLoadingOverlayService, toastr, UserService) {
        var vm = this;

        console.log('inside changePassword controller')
        activate();

        function activate() {
            vm.user = UserService.getCurrentUser();
            
        }

        function doReset() {
            bsLoadingOverlayService.start();
            var CPdata = {
                oldPassword : vm.oldPassword,
                newPassword : vm.newPassword,
                userId : vm.user.id
            }
            console.log(CPdata);
            dataService.changePassword(CPdata).then(function(response) {
                if (response.status === 200) {
                    toastr.success("Password Changed Successfully");
                    // Since we cannot bind Alert Ok Click Event and 
                    // it would be too heavy to use UIB Modal just for dialog.
                    // So hack is use window.confirm and logout the user
                    var result = $window.confirm("Please login again with the new Password.");
                    if (result) {
                        UserService.signOutUser();
                        $rootScope.$broadcast('signOut');
                        $state.go("app");
                    } else {
                        UserService.signOutUser();
                        $rootScope.$broadcast('signOut');
                        $state.go("app");
                    }
                }
                bsLoadingOverlayService.stop();
            });
        }

        vm.validateForm = function(_input) {
            // var passwrdRgx = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;

            if (_input === 'oldPasswrd') {
                if (vm.oldPassword) {
                    vm.oldPasswordError = false;
                } else {
                    vm.oldPasswordError = true;
                }
            }
            if (_input === 'newPasswrd') {
                if (vm.newPassword.length > 7) {
                    vm.newPasswordError = false;
                    vm.samePasswordError = false;
                    if (vm.newPassword === vm.oldPassword) {
                        vm.samePasswordError = true;
                    }
                } else {
                    vm.newPasswordError = true;
                }
            }
            if (_input === 'cnfrmNewPass') {
                if (vm.confNewPassword === vm.newPassword) {
                    vm.confNewPasswordError = false;
                } else {
                    vm.confNewPasswordError = true;
                }
            }
        };

        vm.submitForm = function() {
            if (angular.isUndefined(vm.newPassword) || vm.newPassword === null || vm.newPassword === '') {
                vm.newPasswordError = true;
            } else {
                vm.newPasswordError = false;
            }
            if (angular.isUndefined(vm.confNewPassword) || vm.confNewPassword === null || vm.confNewPassword === '') {
                vm.confNewPasswordError = true;
            } else {
                vm.confNewPasswordError = false;
            }

            if (angular.isUndefined(vm.oldPassword) || vm.oldPassword === null || vm.oldPassword === '') {
                vm.oldPasswordError = true;
            } else {
                vm.oldPasswordError = false;
            }

            if (!vm.newPasswordError && !vm.confNewPasswordError && !vm.oldPasswordError && vm.newPassword != vm.oldPassword) {
                doReset();
            }
        };
    }
})();
(function() {
    'use strict';

    angular
    .module('app')
    .controller('CompanyDashboardController', CompanyDashboardController);

    CompanyDashboardController.$inject = ['$scope', 'dataService', 'UserService', 'Utils', '$uibModal'];

    function CompanyDashboardController($scope, dataService, UserService, Utils, $uibModal) {
        var vm = this;
        vm.graphType = "W";
        vm.selectedRating = {};
        vm.itemsPerPage = 4;
        vm.activeSlide = 0;
        vm.trendingChartOptions = {
            chart: {
                type: "lineChart",
                height: 450,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 40,
                    left: 55
                },
                useInteractiveGuideline: true,

                x: function(d) {
                    return d.x;
                },
                y: function(d) {
                    return d.y;
                },

                xAxis: {
                    axisLabel: "Time",
                    tickFormat: function(d) {
                        var dx = vm.trendingChartData[0].values[d];
                        if (vm.graphType === "D") {
                            return moment.duration(parseInt(dx.label) + 1, 'hours').humanize();
                        }
                        return dx.label;
                    }
                },
                yAxis: {
                    axisLabel: "Listings",
                    axisLabelDistance: -10
                }
            }
        };

        vm.keywordUsageChartOptions = {
            chart: {
                type: "lineChart",
                height: 300,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 40,
                    left: 40
                },
                showLegend: false,
                x: function(d) {
                    return d.x;
                },
                y: function(d) {
                    return d.y;
                },
                xAxis: {
                    axisLabel: "Time(Days)",
                    tickFormat: function(d) {
                        var dx = vm.keywordUsageChartData[0].values[d - 1];
                        // var dx = vm.keywordUsageChartData[0].values[d];
                        return moment(dx.label).format("MM/DD");
                    }
                },
                yAxis: {
                    axisLabel: "Listings",
                    axisLabelDistance: -10
                }
            }
        };

        activate();

        function activate() {
            vm.user = UserService.getCurrentUser();
            if (vm.user) {
                /*dataService.getLoggedInUser(vm.user.accessToken).then(function(response) {
                    if (response.status === 200) {
                        vm.contractorId = response.data;
                        updateRatingsData();
                        Utils.isImage(dataService.getImageUrl() + response.data + '.jpg').then(function(result) {
                            if (result) {
                                $scope.tempUrl = dataService.getImageUrl() + response.data + '.jpg';
                            }
                        });
                        vm.currentPage = 1;
                        updateTrendingChartData();
                        packageListingDetails();
                    }
                });*/
                vm.contractorId = vm.user.id;
                updateRatingsData();
                Utils.isImage(dataService.getImageUrl() + vm.user.image + '.jpg').then(function(result) {
                    if (result) {
                        $scope.tempUrl = dataService.getImageUrl() + vm.user.image + '.jpg';
                    }
                });
                vm.currentPage = 1;
                updateTrendingChartData();
                packageListingDetails();
                dataService.getContractorProgress().then(function(response) {

                    vm.progress = response.data === 100.0;
                    // vm.progress = 42;
                    if (vm.progress === 100) {
                        vm.enbPackageTab = false;
                    } else {
                        vm.enbPackageTab = true;
                    }
                }, function(err) {
                    console.log('Error In getContractorProgress', err);
                });
            }
        };

        vm.range = function(count) {
            return (new Array(count));
        }

        function packageListingDetails() {
            dataService.getContractorPackage().then(function(response) {
                vm.packageSummary = response.data;
                vm.packageSummary.map(function(pkg) {
                    var packageExpiryDate = moment(pkg.dateCreated).add(pkg.packageDays, 'days');
                    pkg.dateExpiry = packageExpiryDate.toDate();
                    if (moment() > packageExpiryDate) {
                        pkg.packageDaysRemaining = moment(pkg.dateCreated).diff(packageExpiryDate, "days");
                    } else {
                        pkg.packageDaysRemaining = packageExpiryDate.diff(moment().startOf('day'), "days");
                    }
                });
                vm.keywordListing = vm.keywordListing || {};
                vm.keywordListing.keyword = vm.packageSummary.length > 0 ? vm.packageSummary[0] : {};
                configureDateRange(vm.keywordListing.keyword);
                updateKeywordUsageChartData();
            });
        }

        function configureDateRange(keyword) {
            vm.date = vm.date || {};
            vm.date.startDate = moment(keyword.dateCreated);
            vm.date.endDate = moment(keyword.dateExpiry) > moment() ? moment() : moment(keyword.dateExpiry);
        }

        function updateKeywordUsageChartData() {

            dataService.getKeywordsUsageData(vm.keywordListing.keyword.keywordId, moment(vm.date.startDate).format("MM/DD/YYYY"), moment(vm.date.endDate).format("MM/DD/YYYY"))
            .then(function(response) {
                vm.keywordUsageChartData = [];
                vm.keywordUsageChartData.push(response.data);
                vm.keywordUsageChartData.map(function(el) {
                    el.key = el.keywordName;
                });
            });
        }

        function updateRatingsData() {
            console.log(vm.contractorId);
            dataService.getContractorRatings(vm.contractorId).then(function(response) {
                vm.contractorRatingsData = response.data;
                console.log(vm.contractorRatingsData);
                vm.contractorRatingsData.map(function(element) {
                    // element.url = 'assets/images/header-img1.png';
                    var url = dataService.getImageUrl() + element.buyerImage + '.jpg';
                    console.log(url);
                    Utils.isImage(url).then(function(result) {
                        if (result) {
                            element.url = url;
                        } else {
                            element.url = 'assets/images/header-img1.png';
                        }
                    });
                });
                console.log(vm.currentPage);
                setPagingData(vm.currentPage);
            });
        }

        function updateTrendingChartData(search_by) {
            dataService.getTrendingKeywordsData(vm.graphType, search_by, new Date().toLocaleDateString(), UserService.getCurrentUser().id).then(function(response) {
                vm.trendingChartData = convertToChartData(response.data);
            });
        }

        vm.openRatingModal = function(contractorRating) {
            console.log(contractorRating);
            vm.selectedRating = contractorRating;
            vm.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/companyDashboard/DisplayRatingsModal.html',
                scope: $scope
            });
        }

        vm.cancelRatingsModal = function() {
            vm.selectedRating = {};
            vm.modalInstance.dismiss();
        }

        function convertToChartData(d) {
            var chartData = [];
            angular.forEach(d, function(data) {
                var obj = {};
                obj.key = data.keywordName;
                obj.values = [];
                var start = 0;
                if (data.value) {

                    angular.forEach(data.value, function(val, key) {
                        var point = {
                            label: key,
                            x: start,
                            y: val
                        };
                        this.push(point);
                        start++;
                    }, obj.values);
                }

                this.push(obj);
            }, chartData);
            return chartData;
        };

        vm.graphTypeChanged = function(search_by) {
            if (vm.graphType) {
                updateTrendingChartData(search_by);
            }
        };

        vm.nextSlide = function() {
            vm.activeSlide = ((vm.activeSlide + 1) % vm.packageSummary.length);
        };

        vm.previousSlide = function() {
            if (vm.activeSlide == 0) {
                vm.activeSlide = vm.packageSummary.length - 1;
            } else {
                vm.activeSlide = ((vm.activeSlide - 1) % vm.packageSummary.length);
            }
        };

        $scope.$watch("companyDashboard.currentPage", function() {
            // console.log("Page Changed");
            if (vm.currentPage) {

                setPagingData(vm.currentPage);
            }
        });

        function setPagingData(page) {
            if (vm.contractorRatingsData && vm.contractorRatingsData.length > 0) {
                var pagedData = vm.contractorRatingsData.slice(
                    (page - 1) * vm.itemsPerPage,
                    page * vm.itemsPerPage
                    );
                vm.contractorRatingsPagedData = pagedData;
            }
        }

        $scope.$watch('companyDashboard.keywordListing.keyword', function(newVal, oldVal) {
            if (newVal === oldVal) return;
            configureDateRange(newVal);
            updateKeywordUsageChartData();
        }, true);

        $scope.$watch('companyDashboard.date', function(newVal, oldVal) {
            if (newVal === oldVal) return;
            updateKeywordUsageChartData();
        }, true);
    }
})();

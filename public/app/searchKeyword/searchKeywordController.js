(function () {
    'use strict';

    angular
        .module('app')
        .controller('searchKeywordController', searchKeywordController);

    searchKeywordController.$inject = ['$scope', '$filter', '$uibModal', '$state', 'dataService', 'CATEGORY', '$location', 'UserService', 'Utils'];

    function searchKeywordController($scope, $filter, $uibModal, $state, dataService, categoryConst, $location, UserService, Utils) {
        var vm = this;
        vm.const = categoryConst;
        vm.randomisedPaidContr = [];
        vm.randomisedUnpaidContr = [];
        vm.user = {
            name: 'James Cadmen',
            email: 'dummyemail@hotmail.com',
            phone: '+1-613-555-01366',
            country: 'India'
        };

        vm.getData = function (serchTxt) {
            /*try {
                dataService.logKeywordSearch(serchTxt).then(function (resp) {
                    console.log('Keyword Logged..!');
                }, function (err) {
                    throw new Error("Failed Logging Searched " + serchTxt, err);
                });
            } catch (e) {
                throw new Error("Failed Logging Searched " + serchTxt, e);
            }*/
            dataService.findContractor(serchTxt, true).then(function (response) {
                vm.paidContractors = response.data;
                vm.paidContractors.sort(function () {
                    return 0.5 - Math.random();
                });
                prepareContractorData(vm.paidContractors);
                vm.randomisedPaidContr = vm.paidContractors.slice(0, 2);
                logContractorListings(vm.randomisedPaidContr, "P");
            }, function (err) {
                console.log('Error In getPaidContractor', err);
            });
            dataService.findContractor(serchTxt, false).then(function (resp) {
                vm.unpaidContractors = resp.data;
                vm.unpaidContractors.sort(function () {
                    return 0.5 - Math.random();
                });
                prepareContractorData(vm.unpaidContractors);
                vm.randomisedUnpaidContr = vm.unpaidContractors;
                // logContractorListings(vm.randomizedUnpaidContr, "U");
            }, function (err) {
                console.log('Error In getUnpaidContractor', err);
            });
        };

        function logContractorListings(contractors, type) {
            var dataToSend = {};
            dataToSend.data = [];
            angular.forEach(contractors, function (contractor) {
                var obj = {
                    keywordId: $location.search().searchTxt,
                    contractorId: contractor.contractorId,
                    type: type
                };
                dataToSend.data.push(obj);
            });
            try {
                console.log(dataToSend)
                dataService.logContractorListing(dataToSend).then(function (response) {
                    console.log(response.data);
                }, function (err) {
                    throw new Error("Failed Logging Contractor Listing " + $location.search().searchTxt, err);
                });
            } catch (e) {
                throw new Error("Failed Logging Contractor Listing  " + $location.search().searchTxt, e);
            }
        }

        function prepareContractorData(contractors) {
            console.log(contractors);
            contractors.forEach(function (e) {
                // console.log(e);
                e.perRating = (e.avgRating / 5) * 100;
                console.log(e.perRating);
                Utils.isImage(dataService.getImageUrl() + e.image + '.jpg').then(function (result) {
                    if (result) {
                        e.tempUrl = dataService.getImageUrl() + e.image + '.jpg';
                    }
                });
                if (e.keywords.length > 0) {
                    var selectedService = $filter('filter')(e.keywords, {categoryId: categoryConst.service});
                    var selectedProduct = $filter('filter')(e.keywords, {categoryId: categoryConst.product});
                    
                    if (selectedService.length > 0) {
                        e.services = selectedService.map(function (elem) {
                            return elem.keywordName;
                        });
                    }
                    if (selectedProduct.length > 0) {
                        e.products = selectedProduct.map(function (elem) {
                            return elem.keywordName;
                        });
                    }
                }
            });
        }

        vm.getKeywords = function (key) {
            return dataService.getKeywordBySearchKey(key).then(function (resp) {
                return resp.data.keywords.map(function (item) {
                    return item;
                });
            }, function (err) {
                console.log('error', err);
                return [];
            });
        };

        vm.onSelect = function ($item) {
            vm.getData($item.keywordId);
        };

        vm.getData($location.search().searchTxt);

        vm.viewProfile = function (_contractor) {
            var user = UserService.getCurrentUser();
            if (user) {
                $state.go('viewContractorProfile', {contractorId: _contractor.contractorId, keywordId: $location.search().searchTxt});
            } else {
                $uibModal.open({
                    animation: true,
                    templateUrl: '/app/login/loginModal.html',
                    controller: "LoginController",
                    controllerAs: "loginController"
                }).result.then(function (data) {
                    if (data !== 'close') {
                        $state.go('viewContractorProfile', {contractorId: _contractor.contractorId, keywordId: $location.search().searchTxt});
                    }
                }, function () {
                    // $location.path('/login')
                });
            }
        };
    }
})();

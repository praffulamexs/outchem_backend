(function() {
    'use strict';

    angular
    .module('app')
    .controller('AdminProfileController', AdminProfileController);

    AdminProfileController.$inject = ['$scope', '$uibModal', '$window', '$state', 'dataService', '$timeout', 'toastr', 'bsLoadingOverlayService'];

    function AdminProfileController($scope, $uibModal, $window, $state, dataService, $timeout, toastr, bsLoadingOverlayService) {
        var vm = this;

        var BASE_URL_API = "api/v1/";
        // var BASE_URL = "http://localhost:8010/";
        var BASE_URL = "http://ec2-13-126-14-209.ap-south-1.compute.amazonaws.com/";

        vm.categoryToEdit = {}
        vm.keywordToEdit = {};
        vm.packageToEdit = {};
        vm.pricingToEdit = {};
        vm.facilityToEdit = {};
        vm.affAssociationToEdit = {};
        vm.certificateToEdit = {};
        vm.sliderToEdit = {};

        vm.isPriceDirty = false;

        vm.getCategoryTemplate = function(category) {
            if (category.id === vm.categoryToEdit.id) return 'editCategory';
            else return 'displayCategory';
        };

        vm.getKeywordTemplate = function(keyword) {
            if (keyword.keywordId === vm.keywordToEdit.keywordId) return 'editKeyword';
            else return 'displayKeyword';
        };

        vm.getPackageTemplate = function(pkg) {
            if (pkg.pId === vm.packageToEdit.pId) return 'editPackage';
            else return 'displayPackage';
        };

        vm.getPricingTemplate = function(pricing) {
            if (pricing.from === vm.pricingToEdit.from) return 'editPricing';
            else return 'displayPricing';
        };

        vm.getFacilitiesTemplate = function (facility) {
            if (facility.facilityId === vm.facilityToEdit.facilityId) return 'editFacility';
            else return 'displayFacility';
        }

        vm.getCertificatesTemplate = function (certificate) {
            if (certificate.id === vm.certificateToEdit.id) return 'editCertificate';
            else return 'displayCertificate';
        }

        vm.getSlidersTemplate = function (slider) {
            if (slider.id === vm.sliderToEdit.id) return 'editSlider';
            else return 'displaySlider';
        }

        vm.getAffAssociationTemplate = function (affAssociation) {
            if(affAssociation.affAssociationID === vm.affAssociationToEdit.affAssociationID) return 'editAffAssociation';
            else return 'displayAffAssociation';
        }

        vm.categoryHeaders = ["Name", "Description", "<", ">"];
        vm.keywordHeaders = ["Keyword", "Category", "<", ">"];
        vm.packageListingHeaders = ["Name", "Days", "<", ">"];
        vm.pricingHeaders = ["Keyword", "Rank", "Price", ""];
        vm.keywordDetailsHeaders = ["Keyword Name", "Category Name", "Rank", "Price", "Count", "Contractors", "Buyers"];
        vm.facilitiesHeaders = ["Name", "<", ">"];
        vm.affAssociationHeaders = ["Name", "<", ">"];
        vm.remindersHeader = ["Buyer Company", "Contractor Company", "Keyword", "Action"];
        vm.certificatesHeader = ["Title","Image","Delete"];
        vm.slidersHeader = ["Title","Image","Delete"];
        activate();

        bsLoadingOverlayService.start();
        function activate() {


            // call data services method to get data

            updateServices();            

            updateContractors();

            updateKeywordData()
            
            updateCateoryData();


           /* updateKeywordData();

            updatePackageData();

            updatePricingData();

            getKeywordDetailsData();

            updateFacilitiesData();

            updateAffAssociationData();*/

            

            // updateRemindersData();

            /*vm.pricingData = dummyPricingData.pricingModel;
            vm.startingFromRank = Math.max.apply(Math, vm.pricingData.map(function(o) {
                return o.to;
            })) + 1;
            vm.basePrice = parseInt(dummyPricingData.basePrice);
            console.log(vm.startingFromRank);*/

            // bsLoadingOverlayService.stop();
        }

        function updateServices() {
            dataService.getServices().then(function (response) {
                // console.log(response);
                vm.Services=response.data;
            });
        }

        function updateContractors() {
            dataService.getTopContractors().then(function (response) {
                if (response.status == 200)
                {
                    vm.Contractors = response.data;                    
                }
                else
                {
                    updateContractors();
                }
            });
        }

        vm.setTopContractor = function (contractor) {
            dataService.setTopContractor(contractor).then(function (response) {
                console.log(response);
                if(response.status == 200)
                {
                    if (response.data.set == 1 || response.data.set == 2) {
                        toastr.success(response.data.message);
                    }
                    else {
                        toastr.warning(response.data.message);
                    }
                    updateContractors();
                }
                else
                {
                    toastr.error("Failed To Add");
                }
                updateContractors();
            });
        }

        vm.setTopService = function(service) {
            console.log(service);
            dataService.setTopService(service).then(function (response) {
                if(response.status == 200)
                {
                    if (response.data.set == 1 || response.data.set == 2) {
                        toastr.success(response.data.message);
                    }
                    else
                    {
                        toastr.warning(response.data.message);
                        
                    }
                    updateServices();
                }
                else
                {
                    toastr.error("Failed To Add");
                }
                updateServices();
                
            });
        }

        function updateCateoryData() {
            dataService.getCategories().then(function(response) {

                vm.categoryData = response.data.categories;
                // updateKeywordData();
                updatePackageData();
            });
        }

        function updateKeywordData() {
            dataService.getAllKeywords().then(function(response) {
                vm.keywordData = response.data.keywords;
                console.log(vm.keywordData);
                // updatePackageData();
            });
        }

        function updatePackageData() {
            dataService.getPackage().then(function(response) {
                console.log(response.data);
                vm.packageListingData = response.data;
                updatePricingData();

            });
        }

        function updatePricingData() {
            dataService.getPricing().then(function(response) {
                console.log(response.data);
                vm.pricingData = response.data.pricingModel;
                vm.basePrice = vm.pricingData.basePrice;
                /*if(vm.pricingData.length > 0){
                    vm.basePrice = parseInt(response.data.basePrice) || vm.pricingData[vm.pricingData.length - 1].basePrice || vm.pricingData[vm.pricingData.length - 1].price;
                    vm.pricingData.splice(-1,1);
                    vm.startingFromRank = Math.max.apply(Math, vm.pricingData.map(function(o) {
                        return o.to;
                    })) + 1;
                    vm.pricing = vm.pricing || {};
                    vm.base = vm.base || {};
                    vm.base.basePrice = vm.basePrice;
                    vm.pricing.from = vm.startingFromRank;
                }*/
                getKeywordDetailsData();
            });
        }

        function getKeywordDetailsData() {
            dataService.getKeywordDetails().then(function(response) {
                vm.keywordDetailsData = response.data;
                var i =1;
                vm.keywordDetailsData.map(function (element) {
                    element.rank = i;
                    i++;
                });
                updateFacilitiesData();


            })
        }

        function updateFacilitiesData() {
            dataService.getFacilities().then(function(response) {
                console.log(response.data)
                if(response.status === 200){
                    vm.facilitiesData = response.data.facilities;
                }else{
                    vm.facilitiesData = [];
                }
                updateAffAssociationData();

                console.log(vm.facilitiesData);
            })
        }

        function updateAffAssociationData() {
            dataService.getAffiliations().then(function(response) {
                console.log(response.data)
                if(response.status === 200){
                    vm.affAssociationsData = response.data.affAsso;
                }else{
                    vm.affAssociationsData = [];
                }
                video();
                
            })
            
        }

        function video()
        {
            dataService.getVideo().then(function (response) {
                console.log(response);
                if (response.status === 200) {
                    $scope.video = response.data;
                } else {
                }
                updateCertificatesData()

            });
        }

        function updateCertificatesData(){
            dataService.getAllCertificates().then(function(response){
                if(response.status == 200){
                    console.log(response.data);
                    vm.certificatesData = response.data;
                    for (var i = 0; i < vm.certificatesData.length; i++) {
                        var img = vm.certificatesData[i].url.split('/');
                        vm.certificatesData[i].name = img[3];
                    // vm.certificatesData[i].name = imgname.substring(imgname.lastIndexOf("-")+1,imgname.lastIndexOf("."));
                }
                console.log(vm.certificatesData);

            }else{
                console.log('error retreiving certificates data');
                console.log(response)
            }
            updateSlidersData();
        })
        }

        function updateSlidersData(){
            dataService.getImageSlider().then(function(response){
                console.log(response.data);
                if(response.status == 200){
                    vm.slidersData = response.data;
                    for (var i = 0; i < vm.slidersData.length; i++) {
                        var img = vm.slidersData[i].url.split('/');
                        vm.slidersData[i].name = img[3];
                    // vm.certificatesData[i].name = imgname.substring(imgname.lastIndexOf("-")+1,imgname.lastIndexOf("."));
                }   
            }
            else{
                console.log('error retreiving slider data');
                console.log(response);

            }

            // bsLoadingOverlayService.stop();

            updateRemindersData();
        })
        }

        function updateRemindersData() {
            dataService.getRemindersList().then(function (response) {
                if (response.status === 200) {
                    console.log(response.data);
                    vm.remindersData = response.data;
                }else{
                    vm.remindersData = [];
                }
                
            })
            bsLoadingOverlayService.stop();
        }

        vm.openCategoryModal = function(categoryNameToAdd, categoryResults) {
            if (categoryResults.length === 1 && categoryNameToAdd.name === categoryResults[0].name) {
                toastr.warning("Cannot Create a new Category as it already Exists");
                return;
            }
            if (categoryNameToAdd && categoryNameToAdd.name) {
                vm.modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/adminProfile/CategoryAddModal.html',
                    scope: $scope
                });

            } else {
                toastr.warning("Category Name should not be blank");
            }
        }


        vm.cancelCategoryModal = function(categoryToCancel) {
            vm.modalInstance.dismiss();
            categoryToCancel.description = "";
        }

        vm.cancelCertificateModal = function(certificateToCancel) {
            console.log(certificateToCancel);
            vm.modalInstance.dismiss();
            certificateToCancel.file = "";
        }

        vm.cancelSliderModal = function(sliderToCancel) {
            console.log(sliderToCancel);
            vm.modalInstance.dismiss();
            sliderToCancel.file = "";
        }

        vm.addCategory = function(categoryNameToAdd, categoryForm) {
            bsLoadingOverlayService.start();
            categoryNameToAdd.description = categoryNameToAdd.description || "No Description";
            var newCategory = {
                categoryName: categoryNameToAdd.name,
                categoryDescription: categoryNameToAdd.description
            };
            dataService
            .saveCategories(newCategory)
            .then(function(response) {

                if (response.status === 201) {
                    updateCateoryData();
                    updateKeywordData();
                    getKeywordDetailsData();

                    vm.modalInstance.dismiss();
                    toastr.success("Added New Category");
                    vm.category = {};
                    console.log(categoryForm);
                    categoryForm.$setPristine();
                } else {
                    vm.modalInstance.dismiss();
                    toastr.error("Something went wrong !! " + response.status);
                }
                bsLoadingOverlayService.stop();
            });
            bsLoadingOverlayService.stop();
        }

        vm.categoryEdit = function(row) {
            debugger;
            vm.categoryToEdit = angular.copy(row);

        }

        vm.categoryDelete = function(categoryToDelete) {
            var answer = $window.confirm("Are you sure you want to delete " + categoryToDelete.name);
            if (answer) {
                bsLoadingOverlayService.start();

                dataService.deleteCategories(categoryToDelete.id).then(function(response) {

                    if (response.status === 200) {
                        updateCateoryData();
                        updateKeywordData();
                        getKeywordDetailsData();
                        toastr.success("Deleted the Category");

                    } else {
                        toastr.error("Something went wrong !! " + response.status);
                    }
                    bsLoadingOverlayService.stop();
                });
            }

        }

        vm.categorySave = function(categoryToSave) {
            bsLoadingOverlayService.start();
            var saveCategory = {
                categoryId: categoryToSave.id,
                categoryName: categoryToSave.name,
                categoryDescription: categoryToSave.description,
                activeFlag: categoryToSave.activeFlag
            };
            console.log(saveCategory);
            dataService.updateCategories(saveCategory).then(function(response) {

                if (response.status === 200) {
                    updateCateoryData();
                    updateKeywordData();
                    getKeywordDetailsData();
                    vm.categoryToEdit = {};
                    toastr.success("Saved the Category");
                } else {
                    toastr.error("Something went wrong !! " + response.status);
                }
                bsLoadingOverlayService.stop();
            })
        }

        vm.categoryCancel = function(categoryToCancel) {
            vm.categoryToEdit = {};
        }

        vm.openKeywordModal = function(keywordNameToAdd, keywordResult, input1, input2) {
            if (input1) {
                toastr.error("Keyword Name cannot be blank");
                return;
            }
            if (input2) {
                toastr.error("Keyword Name's Category cannot be blank");
                return;
            }
            if (keywordResult.length === 1 && keywordResult[0].keywordName === keywordNameToAdd.name && keywordResult[0].categoryId === keywordNameToAdd.category) {
                toastr.warning("Cannot Create a new Keyword as the same name already exists.");
                return;
            }
            if (keywordNameToAdd && keywordNameToAdd.name && keywordNameToAdd.category) {
                vm.modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/adminProfile/KeywordAddModal.html',
                    scope: $scope
                });

            } else {
                toastr.warning("Keyword Name/ Category should not be blank");
            }
        }

        vm.cancelKeywordModal = function(keywordToCancel) {
            vm.modalInstance.dismiss();
            keywordToCancel.description = "";
        }

        vm.addKeyword = function(keywordToAdd, keywordForm) {
            bsLoadingOverlayService.start();
            var newKeyword = {
                keywordName: keywordToAdd.name,
                keywordDescription: keywordToAdd.description || "No Description",
                categoryId: keywordToAdd.category
            };

            dataService.saveKeyword(newKeyword).then(function(response) {

                if (response.status === 200) {
                    toastr.success("Added New Keyword");
                    updateKeywordData();
                    getKeywordDetailsData();
                    vm.modalInstance.dismiss();
                    keywordForm.$setPristine();
                    vm.keyword = {};
                } else {
                    vm.modalInstance.dismiss();
                    toastr.error("Something went wrong !! " + response.status);
                }
                bsLoadingOverlayService.stop();
            });
        }

        vm.keywordEdit = function(row) {
            vm.keywordToEdit = angular.copy(row);
        }

        vm.keywordSave = function(keywordToSave) {
            bsLoadingOverlayService.start();
            var dataToSave = {
                keywordId: keywordToSave.keywordId,
                categoryId: keywordToSave.categoryId,
                keywordName: keywordToSave.keywordName
                // keywordDescription: keywordToSave.description
            };
            dataService.updateKeyword(dataToSave).then(function(response) {

                if (response.status === 200) {
                    vm.keywordToEdit = {};
                    updateKeywordData();
                    getKeywordDetailsData();
                    toastr.success("Saved Keyword");
                } else {
                    toastr.error("Something went wrong " + response.status);
                }
                bsLoadingOverlayService.stop();
            });
        }

        vm.keywordCancel = function() {
            vm.keywordToEdit = {};
        }

        vm.keywordDelete = function(keywordToDelete) {
            var answer = $window.confirm("Are you sure you want to delete " + keywordToDelete.keywordName);
            console.log(keywordToDelete);
            if (answer) {
                bsLoadingOverlayService.start();
                dataService.deleteKeyword(keywordToDelete.keywordId).then(function(response) {

                    if (response.status === 200) {
                        updateKeywordData();
                        getKeywordDetailsData();
                        toastr.success("Deleted Keyword");
                    } else {
                        toastr.error("Something Went Wrong !! " + response.status);
                    }
                    bsLoadingOverlayService.stop();
                })
            }
        }

        vm.openListPackageModal = function(listPackage, packageResult, input1) {
            if (input1) {
                toastr.error("Package Name cannot be blank");
                return;
            }
            if (packageResult.length === 1 && packageResult[0].name === listPackage.name) {
                toastr.warning("Cannot Create a new package. A package with same name already exists.");
                return;
            }
            if (listPackage && listPackage.name) {
                vm.modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/adminProfile/PackageAddModal.html',
                    scope: $scope
                });

            } else {
                toastr.warning("Package Name should not be blank");
            }

        }

        vm.cancelListPackageModal = function(listPackage) {
            vm.modalInstance.dismiss();
            keywordToCancel.days = 0;
        }

        vm.addListPackage = function(listPackage, packageForm) {
            bsLoadingOverlayService.start();
            if (listPackage && listPackage.name && parseInt(listPackage.days) > 0) {
                var newPackage = {
                    packageName: listPackage.name,
                    packageDays: parseInt(listPackage.days)
                };

                dataService.savePackage(newPackage).then(function(response) {
                    if (response.status === 200) {
                        updatePackageData();
                        vm.package = {};
                        vm.modalInstance.dismiss();
                        if (response.data == "Invalid Package")
                        {
                            toastr.warning(response.data);
                        }
                        else
                        {
                            toastr.success(response.data);
                        }
                        packageForm.$setPristine();
                    } else {
                        vm.modalInstance.dismiss();
                        toastr.error("Something went wrong !! " + response.status);
                    }
                    bsLoadingOverlayService.stop();
                })
            }
        }

        vm.editPackage = function(pkg) {
            vm.packageToEdit = angular.copy(pkg);
        }

        vm.savePackage = function(pkgToSave) {
            // console.log(pkgToSave);
            bsLoadingOverlayService.start();
            var dataToSave = {
                packageId: pkgToSave.pId,
                packageName: pkgToSave.name,
                packageDays: pkgToSave.days,
                activeFlag: pkgToSave.activeFlag
            };
            dataService.updatePackage(dataToSave).then(function(response) {
                if (response.status === 200) {
                    vm.packageToEdit = {};
                    updatePackageData();
                    if (response.data == "Name Already Exists") {
                        toastr.warning(response.data);
                    }
                    else {
                        toastr.success(response.data);
                    }
                } else {
                    toastr.error("Something went wrong");
                }
                bsLoadingOverlayService.stop();
            });

        }

        vm.cancelPackage = function(pkg) {
            vm.packageToEdit = {};
        }

        vm.deletePackage = function(pkgToDelete) {
            console.log(pkgToDelete);
            var answer = $window.confirm("Are you sure you want to delete " + pkgToDelete.keywordName);
            if (answer) {
                bsLoadingOverlayService.start();
                dataService.deletePackage(pkgToDelete).then(function(response) {

                    if (response.status === 200) {
                        updatePackageData();
                        toastr.success("Deleted Package");
                    } else {
                        toastr.error("Something Went Wrong !! " + response.status);
                    }
                    bsLoadingOverlayService.stop();
                })
            }
        }

        vm.openPriceModal = function() {
            vm.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/adminProfile/PricingSaveModal.html',
                scope: $scope
            });
        }

        vm.cancelPriceModal = function() {
            vm.modalInstance.dismiss();
        }

        vm.addPrice = function(priceToAdd) {

            if(priceToAdd.to < priceToAdd.from){
                toastr.error("Please check the Range", "Incorrect Range");
                return;
            }
            if (angular.isUndefined(vm.pricingData)) {
                console.log(vm.pricing.from)
                console.log(vm.startingFromRank)
                if (vm.pricing.from === vm.startingFromRank) {
                    if (vm.pricing.price > vm.basePrice) {
                        var newPrice = {
                            from: parseInt(vm.pricing.from),
                            to: parseInt(priceToAdd.to),
                            price: parseInt(priceToAdd.price),
                            basePrice: vm.basePrice
                        };

                        vm.pricingData.push(newPrice);

                        toastr.success("Saved Locally. Please click on update price in order to sync the changes with server.");
                        vm.pricing = {};

                        vm.pricing.from = parseInt(priceToAdd.to) + 1;
                        vm.startingFromRank = vm.pricing.from;
                        vm.isPriceDirty = true;
                    } else {
                        toastr.error("Price should be greater than Base Price " + vm.base.basePrice);
                    }
                } else {
                    toastr.error("Cannot Save Data. Please verify the From Days.");
                }

            } else {
                if (vm.pricing.from === vm.startingFromRank) {
                    if (!parseInt(vm.pricing.from) && !parseInt(priceToAdd.to) && !parseInt(priceToAdd.price)) {
                        toastr.error("Incorrect Data. Please check your data before inserting in the table.");
                        return;
                    }
                    if (priceToAdd.price > vm.pricingData[vm.pricingData.length - 1].price) {
                        toastr.error("Price cannot be greater than " + vm.pricingData[vm.pricingData.length - 1].price);
                        return;
                    }
                    if (priceToAdd.price < vm.basePrice) {
                        toastr.error("Price should be greater than Base Price : " + vm.basePrice);
                        return;
                    }
                    var newPrice = {
                        from: parseInt(vm.pricing.from),
                        to: parseInt(priceToAdd.to),
                        price: parseInt(priceToAdd.price),
                        basePrice: 100
                    };

                    vm.pricingData.push(newPrice);
                    toastr.success("Saved Locally. Please click on update price in order to sync the changes with server.");
                    vm.pricing = {};

                    vm.pricing.from = parseInt(priceToAdd.to) + 1;
                    vm.startingFromRank = vm.pricing.from;
                    vm.isPriceDirty = true;
                } else {
                    toastr.error("Cannot Save Data. Please verify the From Days.");
                }
            }
        }

        vm.editPrice = function(priceToEdit) {
            vm.pricingToEdit = angular.copy(priceToEdit);

        }

        vm.savePrice = function(priceToSave, index) {
            if (priceToSave.price < 0) {
                toastr.error("Price cannot be empty or zero");
                return;
            }
            if (vm.pricingData.length < (index + 1)) {

                if (priceToSave.price < vm.basePrice) {
                    toastr.error("Price updated for range " + priceToSave.from + "-" + priceToSave.to + " should be greater than Base Price : " + vm.basePrice);
                    return;
                }

                var upperPriceLimit = priceToSave.price > vm.pricingData[index + 1].price;

                if (priceToSave.price < vm.pricingData[index - 1].price && upperPriceLimit) {
                    vm.pricingData[index] = angular.copy(priceToSave);
                    var tempData = vm.pricingData;

                    toastr.success("Saved locally. Please click on update price in order to sync the changes with server.");
                    vm.isPriceDirty = true;
                    vm.pricingToEdit = {};
                } else {
                    toastr.error("Updated Price is invalid. Please give it a check.");
                }
            } else {
                if (priceToSave.price < vm.basePrice) {
                    toastr.error("Price updated for range " + priceToSave.from + "-" + priceToSave.to + " should be greater than Base Price : " + vm.basePrice);
                    return;
                }
                if (index > 0 && priceToSave.price < vm.pricingData[index - 1].price) {
                    vm.pricingData[index] = angular.copy(priceToSave);
                    toastr.success("Saved locally. Please click on update price in order to sync the changes with server.");
                    vm.isPriceDirty = true;
                    vm.pricingToEdit = {};
                } else if (index === 0) {
                    if (priceToSave.price < vm.basePrice) {
                        toastr.error("Price updated for range " + priceToSave.from + "-" + priceToSave.to + " should be greater than Base Price : " + vm.basePrice);
                        return;
                    }

                    vm.pricingData[index] = angular.copy(priceToSave);

                    toastr.success("Saved locally. Please click on update price in order to sync the changes with server.");
                    vm.isPriceDirty = true;
                    vm.pricingToEdit = {};

                } else {
                    toastr.error("Updated Price is invalid. Please give it a check.");
                }
            }
        }

        vm.cancelPrice = function() {
            vm.pricingToEdit = {};
        }

        vm.deletePrice = function() {
            var answer = $window.confirm("Are you sure you want to Delete all Pricing and start again ?");
            if (answer) {
                vm.pricingData = [];
                vm.pricing.from = 1;
                vm.startingFromRank = 1;
                vm.isPriceDirty = false;
            }
        }

        vm.updatePrice = function(basePrice) {
            console.log(vm.pricingData);
            console.log(vm.startingFromRank);
            if (basePrice === null || basePrice < 1){
                toastr.error("Base Price cannot be empty of zero");
                return;
            }
            if (basePrice > vm.pricingData[vm.pricingData.length -1].price) {
                toastr.error("Base Price is Invalid");
                return;
            }
            var answer = $window.confirm("Are you sure you want to update the pricing ?");
            if (answer) {
                if (parseInt(basePrice) > vm.pricingData[vm.pricingData.length - 1].price) {
                    toastr.error("Base Price cannot be greater than " + vm.pricingData[vm.pricingData.length - 1].price);
                    return;
                }
                bsLoadingOverlayService.start();

                var lastPricing = {
                    from: vm.startingFromRank,
                    to : 0,
                    price : basePrice
                };
                vm.pricingData.push(lastPricing);

                var dataToSave = {
                    lstPricingModel : vm.pricingData
                };
                console.log(dataToSave);
                dataService.updatePricing(dataToSave).then(function(response) {
                    if (response.status === 200) {
                        updatePricingData();
                        toastr.success("Updated Pricing Models");
                        vm.isPriceDirty = false;
                    } else {
                        toastr.error("Something went wrong !! " + response.status);
                    }
                    vm.modalInstance.dismiss();
                    bsLoadingOverlayService.stop();
                })
            }
        }

        vm.openContractorListModal = function (data) {
            if (data.contractorCount > 0) {
                dataService.findContractor(data.keywordId).then(function(response) {
                    if (response.status === 200) {
                        vm.contractorList = response.data;
                        vm.modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'app/adminProfile/ContractorListModal.html',
                            scope: $scope
                        });

                    }
                })
            }
        }

        vm.openBuyerListModal = function (data) {
            if(data.buyerCount > 0){
                dataService.findBuyer(data.keywordId).then(function (response) {
                    if (response.status === 200) {
                        vm.buyerList = response.data;
                        vm.modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'app/adminProfile/BuyerListModal.html',
                            scope: $scope
                        });
                    }
                })
            }
        }

        vm.addFacility = function (result, input1) {
            if (input1) {
                toastr.error("Invalid Input","Please Check inputs.");
                return;
            }
            if(result.length === 1 && result[0].facilityName === vm.facility.name){
                toastr.error("Facility Already Exists.");
                return;
            }
            var dataToSave = {
                facilityName : vm.facility.name
            };
            dataService.saveFacility(dataToSave).then(function (response) {
                if (response.status === 200) {
                    toastr.success("Added new Facility.");
                    updateFacilitiesData();
                    vm.facility = {};
                }else{
                    toastr.error("Something went wrong ! " + response.status);
                }
            });
        }

        vm.editFacilty = function (facilityToEdit) {
            vm.facilityToEdit = angular.copy(facilityToEdit);
        }

        vm.deleteFacility = function (facilityToDelete) {
            var answer = $window.confirm("Are you sure you want to delete " + facilityToDelete.facilityName);
            if (answer) {
                dataService.deleteFacility(facilityToDelete).then(function (response) {
                    if (response.status === 200) {
                        vm.facility = {};
                        updateFacilitiesData();
                        toastr.success("Deleted Facility Successfully.");
                    } else{
                        toastr.error("Something went wrong ! " + response.status);
                    }
                });
            }            
        }

        vm.saveFacility = function (facilityToSave) {
            if (facilityToSave.facilityName.length > 0) {
                console.log(facilityToSave);
                dataService.editFacility(facilityToSave).then(function (response) {
                    if(response.status === 200){
                        toastr.success("Successfully updated Facility.");
                        vm.facilityToEdit = {};
                        vm.facility = {}; 
                        updateFacilitiesData();
                    } else{
                        toastr.error("Something went wrong ! " + response.status);
                    }
                })
            }else{
                toastr.error("Facility Name cannot be blank.");
            }
        }

        vm.cancelFacility = function (facilityToCancel) {
            vm.facilityToEdit = {};
        }


        vm.openCertificateModal = function(certificateTitleToAdd, ceritificateResults) {
            console.log(certificateTitleToAdd);
            console.log(ceritificateResults);

            if (ceritificateResults.length === 1 && certificateTitleToAdd.title === ceritificateResults[0].title) {
                toastr.warning("Cannot Create a new Certificate as it already Exists");
                return;
            }
            if (certificateTitleToAdd && certificateTitleToAdd.title) {
             vm.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/adminProfile/CertificateAddModal.html',
                scope: $scope
            })

         } else {
            toastr.warning("Certificate Name should not be blank");
        }
    }

    vm.openSliderModal = function(sliderTitleToAdd, sliderResults) {
        console.log(sliderTitleToAdd);
        console.log(sliderResults);

        if (sliderResults.length === 1 && sliderTitleToAdd.title === sliderResults[0].title) {
            toastr.warning("Cannot Create a new Slide as it already Exists");
            return;
        }
        if (sliderTitleToAdd && sliderTitleToAdd.title) {
         vm.modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/adminProfile/SliderAddModal.html',
            scope: $scope
        })

     } else {
        toastr.warning("Image Name should not be blank");
    }
}



vm.addCertificate = function(result, input1){
    console.log(result);
        // console.log(input1);
        var certiImage = document.getElementById('certiPic').files[0];
        console.log(certiImage);
        var title = result.title;
        var certiName = 'certi-'+title.toLowerCase().replace(/[^A-Z0-9]+/ig, "_");
        console.log(certiName);
        if(!angular.isUndefined(certiImage)){
            dataService.uploadCertificate(certiImage,certiName,null).then(function(resp){
                console.log(resp.data);
                if(resp.status == 200){
                    var filename = resp.data;
                    var uri = 'assets/images/certificates/'+filename;
                    save(uri);
                }
                else{
                    toastr.error('Something Went wrong uploading certificate');
                    console.log('error uploading certi image');
                }
            });    
        }
        else{
            toastr.error('Please Upload File');
            // save(certificateToSave.url);
        }

        function save(url){
            var certiObj = {
                // id : certificateToSave.id,
                title : result.title,
                url : url
            }
            console.log(certiObj)

            dataService.saveCertificate(certiObj).then(function(resp){
                console.log(resp.data);

                if(resp.status == 200){
                    vm.certificateToEdit = {};
                    vm.certificate = {}; 
                    updateCertificatesData();
                    vm.modalInstance.dismiss();

                    toastr.success("Successfully updated Certificate.");
                }
                else{
                    toastr.error('Something went wrong')
                    console.log('Something went wrong updating certificate');
                }

            })

        }
    }


    vm.addSlider = function(result, input1){
        console.log(result);
        // console.log(input1);
        var slideImage = document.getElementById('slidePic').files[0];
        console.log(slideImage);
        var title = result.title;
        // var slideName = 'certi-'+title.toLowerCase().replace(/[^A-Z0-9]+/ig, "_");
        if(slideImage){

            var slideName = slideImage.name;
            var slideOnlyName = slideName.split('.');
        }
        console.log(slideName);
        console.log(slideOnlyName);
        if(!angular.isUndefined(slideImage)){
            dataService.uploadSliderImage(slideImage,slideOnlyName[0],slideOnlyName[1],null).then(function(resp){
                console.log(resp.data);
                if(resp.status == 200){
                    var filename = resp.data;
                    var uri = 'assets/images/homepageslider/'+filename;
                    save(uri);
                }
                else{
                    toastr.error('Something Went wrong uploading slider');
                    console.log('error uploading slide image');
                    console.log(resp);
                }
            });    
        }
        else{
            toastr.error('Please Upload File');
            // save(certificateToSave.url);
        }

        function save(url){
            var slideObj = {
                // id : certificateToSave.id,
                title : result.title,
                url : url
            }
            console.log(slideObj)

            dataService.saveSliderImage(slideObj).then(function(resp){
                console.log(resp.data);

                if(resp.status == 200){
                    vm.sliderToEdit = {};
                    vm.slider = {}; 
                    updateSlidersData();
                    vm.modalInstance.dismiss();

                    toastr.success("Successfully updated Slider.");
                }
                else{
                    toastr.error('Something went wrong')
                    console.log('Something went wrong updating Slider');
                }

            })

        }
    }


    vm.editCertificate = function(certificateToEdit){
        console.log(certificateToEdit);
        vm.certificateToEdit = angular.copy(certificateToEdit);

    }

    vm.editSlider = function(sliderToEdit){
        console.log(sliderToEdit);
        vm.sliderToEdit = angular.copy(sliderToEdit);

    }

    vm.deleteCertificate = function(certificateToDelete){
        console.log(certificateToDelete);
        var answer = $window.confirm("Are you sure you want to delete " + certificateToDelete.title);
        if (answer) {
            bsLoadingOverlayService.start();
            dataService.deleteCertificate(certificateToDelete).then(function (response) {
            bsLoadingOverlayService.stop();
                
                if (response.status === 200) {
                    vm.certificate = {};
                    updateCertificatesData();
                    toastr.success("Deleted Certificate Successfull.");
                } else{
                    toastr.error("Something went wrong ! " + response.status);
                }
            });
        }
    }

    vm.deleteSlider = function(sliderToDelete){
        console.log(sliderToDelete);
        var answer = $window.confirm("Are you sure you want to delete " + sliderToDelete.title);
        if (answer) {
            dataService.deleteSliderImage(sliderToDelete).then(function (response) {
                if (response.status === 200) {
                    vm.slider = {};
                    updateSlidersData();
                    toastr.success("Deleted Slider Image Successfull.");
                } else{
                    toastr.error("Something went wrong ! " + response.status);
                }
            });
        }
    }

    vm.saveCertificate = function(certificateToSave){
        console.log(certificateToSave);
        var certiImage = document.getElementById('certiPic').files[0];
        var title = certificateToSave.title;
        var certiName = 'certi-'+title.toLowerCase().replace(/[^A-Z0-9]+/ig, "_");
        console.log(certiName);
        console.log(certiImage);
        if(!angular.isUndefined(certiImage)){
            dataService.uploadCertificate(certiImage,certiName,certificateToSave.id).then(function(resp){
                console.log(resp.data);
                if(resp.status == 200){
                    var filename = resp.data;
                    var uri = 'assets/images/certificates/'+filename;
                    save(uri);
                }
                else{
                    toastr.error('Something Went wrong uploading certificate');
                    console.log('error uploading certi image');
                }
            });    
        }
        else{
            save(certificateToSave.url);
        }

        function save(url){
            var certiObj = {
                id : certificateToSave.id,
                title : certificateToSave.title,
                url : url
            }
            console.log(certiObj)

            dataService.editCertificate(certiObj).then(function(resp){
                console.log(resp.data);

                if(resp.status == 200){
                    toastr.success("Successfully updated Certificate.");
                    vm.certificateToEdit = {};
                    vm.certificate = {}; 
                    updateCertificatesData();
                }
                else{
                    toastr.error('Something went wrong')
                    console.log('Something went wrong updating certificate');
                }

            })

        }
    }

    vm.saveSlider = function(sliderToSave){
        console.log(sliderToSave);
        var slideImage = document.getElementById('slidePic').files[0];
        var title = sliderToSave.title;
        // var certiName = 'certi-'+title.toLowerCase().replace(/[^A-Z0-9]+/ig, "_");
        if(slideImage){

            var slideName = slideImage.name;
            var slideOnlyName = slideName.split('.');
        }
        console.log(slideName);
        console.log(slideOnlyName);
        if(!angular.isUndefined(slideImage)){
            dataService.uploadSliderImage(slideImage,slideOnlyName[0],slideOnlyName[1],sliderToSave.id).then(function(resp){
                console.log(resp.data);
                if(resp.status == 200){
                    var filename = resp.data;
                    var uri = 'assets/images/homepageslider/'+filename;
                    save(uri);
                }
                else{
                    toastr.error('Something Went wrong uploading slider');
                    console.log('error uploading slider image');
                }
            });    
        }
        else{
            save(sliderToSave.url);
        }

        function save(url){
            var slideObj = {
                id : sliderToSave.id,
                title : sliderToSave.title,
                url : url
            }
            console.log(slideObj)

            dataService.editSliderImage(slideObj).then(function(resp){
                console.log(resp.data);

                if(resp.status == 200){
                    toastr.success("Successfully updated Slider.");
                    vm.sliderToEdit = {};
                    vm.slider = {}; 
                    updateSlidersData();
                }
                else{
                    toastr.error('Something went wrong')
                    console.log('Something went wrong updating slider');
                }

            })

        }
    }

    vm.cancelCertificate = function(certificateToCancel){
        console.log(certificateToCancel);
        vm.certificateToEdit = {};
    }

    vm.cancelSlider = function(sliderToCancel){
        console.log(sliderToCancel);
        vm.sliderToEdit = {};
    }



    vm.addAffAssociation = function (result, input1) {
        if (input1) {
            toastr.error("Invalid Input","Please Check inputs.");
            return;
        }
        if(result.length === 1 && result[0].affAssociationName === vm.affAssociation.name){
            toastr.error("Affiliation already exists.");
            return;
        }
        var dataToSave = {
            affAssociationName : vm.affAssociation.name
        };
        dataService.saveAffiliation(dataToSave).then(function (response) {
            if (response.status === 200) {
                toastr.success("Added new Affiliation.");
                updateAffAssociationData();
                vm.affAssociation = {};
            }else{
                toastr.error("Something went wrong ! " + response.status);
            }
        });
    }

    vm.editAffAssociation = function (affAssociationToEdit) {
        vm.affAssociationToEdit = angular.copy(affAssociationToEdit);
    }

    vm.deleteAffAssociation = function (affAssociationToDelete) {
        var answer = $window.confirm("Are you sure you want to delete " + affAssociationToDelete.affAssociationName);
        if (answer) {
            dataService.deleteAffiliation(affAssociationToDelete.affAssociationID).then(function (response) {
                if (response.status === 200) {
                    vm.affAssociation = {};
                    updateAffAssociationData();
                    toastr.success("Deleted Affiliation Successfully.");
                } else{
                    toastr.error("Something went wrong ! " + response.status);
                }
            });
        }
    }

    vm.saveAffAssociation = function (affAssociationToSave) {
        if (affAssociationToSave.affAssociationName.length > 0) {
            console.log(affAssociationToSave);
            dataService.editAffiliation(affAssociationToSave).then(function (response) {
                if(response.status === 200){
                    toastr.success("Successfully updated Affiliation.");
                    vm.affAssociationToEdit = {};
                    vm.affAssociation = {}; 
                    updateAffAssociationData();
                }else{
                    toastr.error("Something went wrong ! " + response.status);
                }
            })
        }else{
            toastr.error("Affiliation name cannot be blank.");
        }
    }

    vm.cancelAffAssociation = function (affAssociationToCancel) {
        vm.affAssociationToEdit = {};
    }

    vm.sendReminder = function (reminder) {
        var answer = $window.confirm("Are you sure you want to send a reminder? ");
        if (answer) {
            bsLoadingOverlayService.start();
            var dataToSend = {
                buyerId: reminder.buyerId,
                contractorId: reminder.contractorId,
                keywordId : reminder.keywordId
            };
            dataService.reviewReminder(dataToSend).then(function (response) {
                bsLoadingOverlayService.stop();
                
                if (response.status === 200) {

                    updateRemindersData();
                    toastr.success("Successfully Sent Review Reminder to " + reminder.buyerCompanyName);
                }else{
                    toastr.error("Something went wrong! " + response.status);
                }
            })
        }
    }

    vm.uploadVideo = function(video) {
        dataService.uploadVideo(video).then(function (response) {
            if (response.status === 200) {
                toastr.success(response.data);
            } else {
                toastr.error("Something went wrong! " + response.status);
            }
        })
    }

    vm.editBasePrice = function(){
        console.log(vm.basePrice);
        dataService.setBasePrice(vm.basePrice).then(function (response) {
            if(response.status === 200)
            {
                toastr.success("Updated Successfully");
                
            }
            else
            {
                toastr.error("Unable to Update");
            }
        })
    }

    

    $scope.apiPath = BASE_URL+BASE_URL_API+'buyer/searchBuyer';
    $scope.selectedList = '';
    $scope.sendmail = function(){
        console.log($scope.selectedList)
        bsLoadingOverlayService.start();
        var obj = {};
        obj.data = $scope.selectedList;
        dataService.sendRatingMail(obj).then(function(resp){
            console.log(resp.data)
            bsLoadingOverlayService.stop();
        }, function(err){
            bsLoadingOverlayService.stop();
            console.log(err);
        })
        $scope.selectedList = '';
    }


}

var myApp = angular.module('app');

myApp.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;

          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
            });
         });
      }
  };
}]);

})();

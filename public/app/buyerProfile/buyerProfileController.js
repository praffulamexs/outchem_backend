(function () {
  'use strict';

  angular
  .module('app')
  .controller('BuyerProfileController', BuyerProfileController);

  BuyerProfileController.$inject = ['$scope', '$filter', '$state', 'dataService', '$timeout', 'toastr', 'bsLoadingOverlayService', 'UserService', 'Utils','$rootScope'];

  function BuyerProfileController($scope, $filter, $state, dataService, $timeout, toastr, bsLoadingOverlayService, UserService, Utils,$rootScope) {
    var vm = this;
    vm.buyer = {};
    vm.selectedKeywords = [];
    vm.saveModel = {};
    vm.countries = [
    {
      countryID: 1, name: "India"
    }
    ];

    function bindData(_data) {
      vm.buyer.company = _data.companyName;
      vm.buyer.address = _data.addressOfBusiness;
      if (_data.addedKeywords) {
        vm.selectedKeywords = _data.addedKeywords;
      }
    }

    vm.getData = function () {
      bsLoadingOverlayService.start();
      dataService.getBuyerInformation(vm.user.id).then(function (resp) {
        bindData(resp.data);
        bsLoadingOverlayService.stop();
      }, function (err) {
        bsLoadingOverlayService.stop();
        console.log('Error In getContractor', err);
      });
    };

    activate();

    function activate() {
      bsLoadingOverlayService.start();
      

      vm.user = UserService.getCurrentUser();
            dataService.getUserInfo(vm.user.id,
              function(data){
                console.log(data);
                vm.user = data;
                
                Utils.isImage(dataService.getImageUrl() + vm.user.image + '.jpg').then(function(result) {
                    if (result) {
                        $scope.tempUrl = dataService.getImageUrl() + vm.user.image + '.jpg';
                        console.log($scope.tempUrl);
                    }
                });
            }, function(err){
                console.log(err);
            })


      // vm.tempUrl = vm.user.profileImagePath;
      vm.user.countryName = $filter('filter')(vm.countries, {countryID: parseInt(vm.user.country, 16)});
      vm.user.countryName = vm.user.country;
      /*if (vm.user.countryName.length !=0) {
        vm.user.countryName = vm.user.countryName[0].name;
      }*/
      vm.getData();
      bsLoadingOverlayService.stop();
    }

    vm.editProfile = function(field) {
      console.log(field);
      if(field === 'phone')
      {
       vm.edtPhone = false;
       bsLoadingOverlayService.start();
       dataService.editUser(vm.user).then(function (resp) {
        console.log(resp.data);
        if (resp.status === 200) {
         toastr.success("Phone Updated Successful");
         vm.user = UserService.getCurrentUser();
       }
       bsLoadingOverlayService.stop();
     }, function () {
       toastr.error("Error");
       bsLoadingOverlayService.stop();
     });
     } else {
       vm.edtName = false;
       bsLoadingOverlayService.start();
         $rootScope.firstName = vm.user.firstName;       
         $rootScope.lastName = vm.user.lastName;				
       dataService.editUser(vm.user).then(function (resp) {
        console.log(resp.data);
        if (resp.status === 200) {
         toastr.success("Name Updated Successful"); 
       }
       bsLoadingOverlayService.stop();
     }, function () {
       toastr.error("Error");
       bsLoadingOverlayService.stop();
     });
     }		
   };

   vm.validateForm = function () {
    if (angular.isUndefined(vm.buyer.company) || vm.buyer.company === null || vm.buyer.company === '') {
      vm.companyError = true;
    } else {
      vm.companyError = false;
    }
    if (angular.isUndefined(vm.buyer.address) || vm.buyer.address === null || vm.buyer.address === '') {
      vm.addressError = true;
    } else {
      vm.addressError = false;
    }

    if (!vm.companyError && !vm.addressError) {
      vm.saveModel.companyName = vm.buyer.company;
      vm.saveModel.addressOfBusiness = vm.buyer.address;
      vm.saveModel.profilePicture = '';
      vm.saveModel.keywords = vm.selectedKeywords;
      vm.saveModel.buyerId = vm.user.id;
      bsLoadingOverlayService.start();
      dataService.saveBuyerInformation(vm.saveModel).then(function (result) {
        console.log(result.data);
        console.log(vm.saveModel);
        bsLoadingOverlayService.stop();
        vm.buyerSuccess = true;
        $timeout(function () {
          vm.buyerSuccess = false;
        }, 5000);
        vm.enbCompanyName = false;
        vm.enbCompanyAdd = false;
        console.log(result);
      }, function (err) {
        bsLoadingOverlayService.stop();
        console.log(err);
      });
    }
  };

  vm.getKeywords = function (key) {
    return dataService.getKeywordBySearchKey(key).then(function (resp) {
      console.log(resp.data);
      return resp.data.keywords.map(function (item) {
        return item;
      });
    }, function (err) {
      console.log('Error', err);
      return [];
    });
  };

  vm.addKeyword = function (item) {
    vm.selectedKeywords.push(item);
    vm.buyerDetail.Keyword = '';
  };

  vm.removeKeyword = function (indx) {
    vm.selectedKeywords.splice(indx, 1);
  };

    /*vm.verifyMobile = function () {
      vm.showOTP = true;
      dataService.verifyPhone('091' + vm.user.phone, vm.user.accessToken).then(function (resp) {
        if (resp.status === 200) {
          vm.verifyMobileClk = true;
          toastr.success("OTP Sent Successful");
        } else {
          toastr.error("Error In Sending OTP");
        }
      }, function (err) {
        toastr.error("Error In Sending OTP");
        console.log('validate OTP error', err);
      });
    };

    vm.validOTP = function () {
      vm.showOTP = false;
      dataService.verifyPhoneCode('091' + vm.user.phone, vm.OTP, vm.user.accessToken).then(function (resp) {
        if (resp.status === 200) {
          vm.user.phoneNumberConfirmed = true;
          toastr.success("OTP Validated Successful");
        } else {
          toastr.error("OTP Validation error");
        }
      }, function (err) {
        toastr.error("OTP Validation error");
        console.log('OTP Validation error', err);
      });
    };

    vm.resendOTP = function () {
      console.log('resend');
    };*/
    vm.verifyMobile = function() {
      vm.showOTP = true;

      dataService.checkBalance().then(
        function(resp){
          console.log(resp);
          if (resp.status === 200) {

            vm.verifyMobileClk = true;
            if(resp.data < 0){
              toastr.error("SMS API balance end");
            }
            else{
              $scope.myotp = Math.floor(100000 + Math.random() * 9000);
              console.log($scope.myotp);
              sendsms($scope.myotp);
            }

          } else {
            toastr.error("Error In Sending OTP");
          }
        }, function(err) {
          toastr.error("Error In Sending OTP");
          console.log('checking balance error : ', err);
        })
    };

    function sendsms(OTP) {
      var msg = "Welcome to OUTCHEM: Your One Time Password (OTP) is "+OTP+". Thank You";
      console.log(vm.user.phone);
      dataService.sendSMS(msg,vm.user.phone).then(
        function(resp){
          if (resp.data.includes("SUBMIT_SUCCESS")) {
            vm.verifyMobileClk = true;
            toastr.success("OTP Sent Successful");
          } else {
            sendsms(OTP);
          }
        }, function(err){
          toastr.error("Error In Sending OTP");
          console.log('Sending OTP error', err);            
        })
    };

    vm.validOTP = function() {

      console.log(vm.OTP);
      if($scope.myotp == vm.OTP)
      {
      vm.showOTP = false;
        vm.user.phoneNumberConfirmed = 1;
        toastr.success("OTP Validated Successful");
        console.log(vm.user);
        dataService.editUser(vm.user).then(function (resp) {
          if (resp.status === 200) {
            console.log(resp.data);
            dataService.getUserInfo(vm.user.id,
              function(data){
                console.log(data);
                vm.user = data;
              }, function(err){
                console.log(err);
              })
          }
        });
      }
      else{
      vm.showOTP = true;

        toastr.error("OTP Validation error");
      }
    };

    vm.resendOTP = function() {
      console.log('resend');
      $scope.myotp = Math.floor(100000 + Math.random() * 9000); 
      console.log($scope.myotp);
      sendsms($scope.myotp); 
    };

    $scope.$watch('model.profPic', function (newFile) {
      if (angular.isUndefined(newFile) === false) {
        $scope.tempUrl = URL.createObjectURL(newFile);
        dataService.imageUpload($scope.model.profPic).then(function (resp) {
          console.log(resp);
          var imagename = resp.data;
          var img = imagename.split('.');
          if (resp.status === 200) {
                        $rootScope.tempUrl = 'assets/images/profiles/'+img[0]+'.jpg';

           vm.imageData = {
            image : img[0],
            userId: vm.user.id
          };
          dataService.imageSave(vm.imageData).then(function(resp){

            if(resp.status == 200){
              toastr.success("Profile Pic Saved Successful");
/*              dataService.getUserInfo(vm.user.id,
                function(data){
                  console.log(data);
                  vm.user = data;
                }, function(err){
                  console.log('user retreiving failed')
                })*/

            }else{
              toastr.error("Profile Pic Saving Failed");

            }

          }) 
        } else {
          toastr.error("Profile Pic Upload Failed");
        }
      }, function (err) {
        console.log('Image Upload error', err);
        toastr.error("Profile Pic Saving Failed");
      });
      } else {
        vm.tempUrl = '';
      }
    });
  }
})();
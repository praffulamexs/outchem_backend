(function () {
  'use strict';
  angular
    .module('app')
    .controller('ResetPasswordController', ResetPasswordController);

  ResetPasswordController.$inject = ['$state', '$scope', 'dataService', 'bsLoadingOverlayService', 'toastr', '$location'];

  function ResetPasswordController($state, $scope, dataService, bsLoadingOverlayService, toastr, $location) {
    var vm = this;

    console.log('code', $location.search().code);
    console.log('userID', $location.search().userId);
    vm.code = $location.search().code;
    vm.userId = $location.search().userId;

    function doReset() {
      bsLoadingOverlayService.start();

      vm.resetPassobj = {
        userId : vm.userId,
        code : vm.code,
        password: vm.password
      };
      console.log(vm.resetPassobj);
      /*dataService.resetPassword(vm.userId, vm.code, vm.password).then(function (response) {*/
        dataService.resetPassword(vm.resetPassobj).then(function (response) {
        console.log(response.data);
        if (response.status === 200) {
          toastr.success("Password Changed Successfully");
          $state.go('app');
        }
        bsLoadingOverlayService.stop();
      });
    }

    vm.validateForm = function (_input) {
      // var passwrdRgx = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
      if (_input === 'passwrd') {
          if (vm.password.length > 7) {
          vm.passwordError = false;
        } else {
          vm.passwordError = true;
        }
      }
      if (_input === 'cnfrmPass') {
        if (vm.confPassword === vm.password) {
          vm.confPasswordError = false;
        } else {
          vm.confPasswordError = true;
        }
      }
    };

    vm.submitForm = function () {
      if (angular.isUndefined(vm.password) || vm.password === null || vm.password === '') {
        vm.passwordError = true;
      } else {
        vm.passwordError = false;
      }
      if (angular.isUndefined(vm.confPassword) || vm.confPassword === null || vm.confPassword === '') {
        vm.confPasswordError = true;
      } else {
        vm.confPasswordError = false;
      }

      if (!vm.passwordError && !vm.confPasswordError) {
        doReset();
      }
    };
  }
})();

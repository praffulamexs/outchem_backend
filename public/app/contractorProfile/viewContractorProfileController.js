(function () {
  'use strict';

  angular
  .module('app')
  .controller('viewContractorProfileController', viewContractorProfileController);

  viewContractorProfileController.$inject = ['$scope', '$state', '$filter', 'bsLoadingOverlayService', 'dataService', 'CATEGORY', '$location', 'Utils', 'UserService', 'toastr'];

  function viewContractorProfileController($scope, $state, $filter, bsLoadingOverlayService, dataService, categoryConst, $location, Utils, UserService, toastr) {
    var vm = this;
    vm.const = categoryConst;
    console.log($state.params);
    vm.contractorID = $location.search().contractorId;
    // vm.contractorID = $state.params.contractorId;
    vm.keywordId = $state.params.keywordId;
    dataService.getUserInfo(vm.contractorID, 
      function(data){
        console.log(data);
        vm.contractor = data;
        vm.imageUrl = dataService.getImageUrl() + vm.contractor.image + '.jpg';
        console.log(vm.imageUrl);
      }, function(err){
        console.log(err);
      })
    // vm.imageUrl = dataService.getImageUrl() + vm.contractorID + '.jpg';

    vm.getData = function () {
      bsLoadingOverlayService.start();
      vm.user = UserService.getCurrentUser(); 
      console.log(vm.user);     
      // vm.tempUrl = vm.user.profileImagePath;
      
      dataService.getContractorProfile(vm.contractorID).then(function (resp) {
        console.log(resp.data);
        vm.contractorData = resp.data[0];
        vm.perRating = (vm.contractorData.avgRating / 5) * 100;
        if (vm.contractorData.keywords) {
          vm.services = $filter('filter')(vm.contractorData.keywords, {categoryId: categoryConst.service});
          vm.products = $filter('filter')(vm.contractorData.keywords, {categoryId: categoryConst.product});
        }

        if (vm.contractorData.regulatoryBodies) {
          vm.regulatoryBodies = vm.contractorData.regulatoryBodies;
          console.log(vm.regulatoryBodies);
          console.log(vm.regulatoryBodies.length);
          vm.arrRegulatoryBodies = vm.regulatoryBodies.split(',');

          for (var i = 0; i < vm.arrRegulatoryBodies.length; i++) {
            vm.arrRegulatoryBodies[i] = '/assets/images/certificates/certi-'+vm.arrRegulatoryBodies[i]+'.jpg';
          }

          $scope.checkboxModel = {};
          vm.arrRegulatoryBodies.forEach(function (elem) {
            $scope.checkboxModel[elem] = elem;
          });
        } else {
          vm.regulatoryBodies = '';
        }

        if (vm.contractorData.affiliatedAssociations) {
          vm.affiliatedAssociations = vm.contractorData.affiliatedAssociations.split(',');
        }

        if (vm.contractorData.facilities) {
          vm.facilities = vm.contractorData.facilities.split(',');
        }

        bsLoadingOverlayService.stop();
      }, function (err) {
        bsLoadingOverlayService.stop();
        console.log('Error In getContractor', err);
      });
    };

    vm.getData();

    vm.contactContractor = function () {
      bsLoadingOverlayService.start();

      var objContact = {};       
      objContact.contractorId = vm.contractorID;
      console.log(vm.user);
      if(vm.user.isContractor == 0){
        objContact.buyerId = vm.user.id;
        objContact.keywordId = vm.keywordId;
        console.log(objContact);
        dataService.contactMeInfo(objContact).then(function (resp) {
          console.log(resp.data);
          bsLoadingOverlayService.stop();

          toastr.success("Message Sent to Contractor");
        }, function (err) {
          bsLoadingOverlayService.stop();

          toastr.error("Message Sent Failed");
          throw new Error("Failed Contact Contractor");
        })
      }
      else if(vm.user.isContractor == 1){
      bsLoadingOverlayService.stop();

        toastr.error("Contractor cannot connect to other contractor");
      }
      else if(vm.user.isAdmin == 1){
      bsLoadingOverlayService.stop();
        
        toastr.error("Admin cannot connect to contractor");
      }

    };
  }
})();

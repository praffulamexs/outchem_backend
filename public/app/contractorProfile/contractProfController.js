(function() {
    'use strict';
    angular
    .module('app')
    .controller('ContractProfController', ContractProfController);
    ContractProfController.$inject = ['$scope', 'dataService', 'bsLoadingOverlayService', '$timeout', '$filter', 'CATEGORY', 'UserService', 'toastr', 'Utils','$state','$rootScope'];

    function ContractProfController($scope, dataService, bsLoadingOverlayService, $timeout, $filter, categoryConst, UserService, toastr, Utils,$state,$rootScope) {

        var vm = this;
        vm.progress = 60;
        vm.contractor = {};
        vm.companyDetail = {};
        vm.objPackage = {};
        vm.selectedKeywords = [];
        vm.selectedProductKeywords = [];
        vm.selectedPackageKeywords = [];
        vm.companyDetail.Transportation = 'No';
        vm.companyDetail.PackagingServices = 'No';
        // vm.affiliatedAssociations = ['The Indian Pharmaceutical Association', 'Indian Drug Manufacturers&#39; Association', 'Organisation of Pharmaceutical Producers of India Peninsula Chambers', 'Bulk Drug Manufacturers Association'];
        vm.affiliatedAssociations = [];
        vm.locations = ['Mumbai', 'Pune', 'Nashik', 'Delhi', 'Chandigarh', 'Surat', 'Zansi'];
        // vm.Facilities = ['Schedule M Facility', 'Modular Facility', 'Germfree Facility'];
        vm.Facilities = [];
        vm.companyDetail.location = vm.locations[0];
        // vm.companyDetail.Facilities = ['Facility 2', 'Facility 3'];
        vm.countries = [{
            countryID: 1,
            name: "India"
        }];
        vm.companyDetail.arrRegulatoryBodies = [];

        activate();

        function activate() {
            vm.user = UserService.getCurrentUser();
            dataService.getUserInfo(vm.user.id,
              function(data){
                console.log(data);
                vm.user = data;

                Utils.isImage(dataService.getImageUrl() + vm.user.image + '.jpg').then(function(result) {
                    if (result) {
                        $scope.tempUrl = dataService.getImageUrl() + vm.user.image + '.jpg';
                        console.log($scope.tempUrl);
                    }
                });
            }, function(err){
                console.log(err);
            })
            /*dataService.getLoggedInUser(vm.user.accessToken).then(function(resp) {
                if (resp.status === 200) {
                    Utils.isImage(dataService.getImageUrl() + resp.data + '.jpg').then(function(result) {
                        if (result) {
                            $scope.tempUrl = dataService.getImageUrl() + resp.data + '.jpg';
                        }
                    });
                }
                bsLoadingOverlayService.stop();
            }, function() {
                bsLoadingOverlayService.stop();
            });*/


            /*vm.user.countryName = $filter('filter')(vm.countries, { countryID: parseInt(vm.user.country, 16) });
            if (vm.user.countryName) {
                vm.user.countryName = vm.user.countryName[0].name;
            }*/
            bsLoadingOverlayService.start();
            dataService.getPackage().then(function(resp) {
                console.log(resp.data);
                vm.packages = resp.data;
                vm.objPackage.packageId = vm.packages[0].pId;
                bsLoadingOverlayService.stop();
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('Error In getPackage', err);
                vm.packages = [];
            });
            dataService.getFacilities().then(function(resp) {
                // resp.data.facilities.forEach(function (element) {
                //   vm.Facilities.push(element.facilityName);
                // });
                console.log(resp.data);
                var facilityNameArr = resp.data.facilities.map(function(el) {
                    return el.facilityName;
                });
                vm.Facilities = facilityNameArr;
                bsLoadingOverlayService.stop();
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('Error In getFacility', err);
                // vm.packages = [];
            });
            dataService.getAffiliations().then(function (resp) {
                console.log(resp.data);
                var affAssociationArr = resp.data.affAsso.map(function (el) {
                    return el.affAssociationName;
                });
                vm.affiliatedAssociations = affAssociationArr;
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('Error In getAffiliations', err);
            });

            dataService.getAllCertificates().then(
              function(response){
                console.log(response.data);
                vm.certificates = response.data;

                for (var i = 0; i < vm.certificates.length; i++) {
                    var img = vm.certificates[i].url.split('/');
                    var imgname = img[3];
                    vm.certificates[i].name = imgname.substring(imgname.lastIndexOf("-")+1,imgname.lastIndexOf("."));
                    vm.certificates[i].check = 'checkboxModel.'+imgname.substring(imgname.lastIndexOf("-")+1,imgname.lastIndexOf("."));
                }
                console.log(vm.certificates);
            }, function(err){
                console.log(err);
            });

        }

        function bindTabData(_data) {
            vm.companyDetail.companyName = _data.companyName;
            vm.companyDetail.companyAddress = _data.companyAddress;
            vm.companyDetail.companyDesc = _data.companyDescription;
            vm.companyDetail.TechnicalSpecs = _data.technicalSpecs;
            vm.companyDetail.MinCapacity = _data.minCapacity;
            vm.companyDetail.MaxCapacity = _data.maxCapacity;
            vm.companyDetail.Transportation = _data.transportation ? _data.transportation : 'No';
            vm.companyDetail.PackagingServices = _data.packagingServices ? _data.packagingServices : 'No';

            if (_data.keywords) {
                vm.selectedKeywords = $filter('filter')(_data.keywords, { categoryId: categoryConst.service });
                vm.selectedProductKeywords = $filter('filter')(_data.keywords, { categoryId: categoryConst.product });
            }
            if (_data.regulatoryBodies) {
                vm.companyDetail.regulatoryBodies = _data.regulatoryBodies;
                vm.companyDetail.arrRegulatoryBodies = vm.companyDetail.regulatoryBodies.split(',');
                $scope.checkboxModel = [];
                vm.companyDetail.arrRegulatoryBodies.forEach(function(elem,i) {
                    $scope.checkboxModel[i] = elem;
                });
                $scope.checked=[];
                check($scope.checkboxModel,$scope.checked);
                
                /* vm.companyDetail.arrRegulatoryBodies.forEach(function(elem) {
                    $scope.checkboxModel[elem] = elem;
                }); */
            } else {
                $scope.checked = [];
                vm.companyDetail.regulatoryBodies = '';
            }
            if (_data.affiliatedAssociations) {
                vm.companyDetail.affiliatedAssociations = _data.affiliatedAssociations.split(',');
            }
            if (_data.facilities) {
                vm.companyDetail.Facilities = _data.facilities.split(',');
            }
            if (_data.packages) {
                vm.addedPackages = _data.packages;
            }
            setTimeout(sort($scope.checkboxModel, vm.certificates), 2000);
            
        }

        vm.getTabData = function() {
            bsLoadingOverlayService.start();
            dataService.getContractor(vm.user.id).then(function(resp) {
                console.log(resp.data);
                bindTabData(resp.data);
                bsLoadingOverlayService.stop();
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('Error In getContractor', err);
            });
        };
        
        vm.editProfile = function(field) {
            console.log(field);
            if(field === 'phone')
            {
                vm.edtPhone = false;
                bsLoadingOverlayService.start();
                console.log(vm.user);
                dataService.editUser(vm.user).then(function (resp) {
                    if (resp.status === 200) {
                        toastr.success("Phone Updated Successful");
                        vm.user = UserService.getCurrentUser();
                    }
                    bsLoadingOverlayService.stop();
                }, function () {
                    toastr.error("Error");
                    bsLoadingOverlayService.stop();
                });
            } else if(field == 'name'){
                console.log('inside else')
                vm.edtName = false;
                bsLoadingOverlayService.start();
                console.log(vm.user);
                $rootScope.firstName = vm.user.firstName;       
                $rootScope.lastName = vm.user.lastName;
                dataService.editUser(vm.user).then(function (resp) {
                    if (resp.status === 200) {
                        toastr.success("Name Updated Successful");                  
                    }
                    bsLoadingOverlayService.stop();
                }, function () {
                    toastr.error("Error");
                    bsLoadingOverlayService.stop();
                });
            }       
        };

        vm.updateProgress = function() {
            bsLoadingOverlayService.start();
            dataService.getContractorProgress(vm.user.id).then(function(response) {
                bsLoadingOverlayService.stop();
                vm.progress = response.data;
                console.log(response.data);
                // vm.progress = 100;
                if (vm.progress == 100) {
                    vm.enbPackageTab = false;
                } else {
                    vm.enbPackageTab = true;
                }
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('Error In getContractorProgress', err);
            });
        };
        vm.getTabData();
        vm.updateProgress();
        vm.getKeywords = function(key) {
            console.log(categoryConst.service)
            return dataService.getKeywordBySearchKey(key, categoryConst.service).then(function(resp) {
                return resp.data.keywords.map(function(item) {
                    return item;
                });
            }, function(err) {
                console.log('Error', err);
                return [];
            });
        };

        vm.getProductKeywords = function(key) {
            // console.log(categoryConst.product)
            console.log(key)
            return dataService.getKeywordBySearchKey(key, categoryConst.product).then(function(resp) {
             console.log(resp.data);
             return resp.data.keywords.map(function(item) {
                return item;
            });
         }, function(err) {
            console.log('Error', err);
            return [];
        });
        };

        vm.getPackages = function(key) {
            // return dataService.getPackage(key).then(function (resp) {
                return dataService.getKeywordBySearchKey(key).then(function(resp) {
                    return resp.data.keywords.map(function(item) {
                        return item;
                    });
                }, function(err) {
                    console.log('Error', err);
                    return [];
                });
            };

            vm.validateForm = function() {
                if (angular.isUndefined(vm.companyDetail.companyName) || vm.companyDetail.companyName === null || vm.companyDetail.companyName === '') {
                    vm.errCompanyName = true;
                } else {
                    vm.errCompanyName = false;
                }

                if (angular.isUndefined(vm.companyDetail.companyAddress) || vm.companyDetail.companyAddress === null || vm.companyDetail.companyAddress === '') {
                    vm.errCompanyAddress = true;
                } else {
                    vm.errCompanyAddress = false;
                }
                if (angular.isUndefined(vm.companyDetail.companyDesc) || vm.companyDetail.companyDesc === null || vm.companyDetail.companyDesc === '') {
                    vm.errCompanyDesc = true;
                } else {
                    vm.errCompanyDesc = false;
                }
                if (!vm.errCompanyName || !vm.errCompanyAddress || !vm.errCompanyDesc) {
                    activate();
                    // vm.contractor.contractorId = "";
                    vm.contractor.contractorId = vm.user.id;
                    vm.contractor.companyLogo = "";
                    vm.contractor.companyName = vm.companyDetail.companyName;
                    vm.contractor.companyAddress = vm.companyDetail.companyAddress;
                    vm.contractor.companyDescription = vm.companyDetail.companyDesc;
                    vm.contractor.businessPicture = "";
                // vm.contractor.review = "";
                // vm.contractor.rating = 0;
                // vm.contractor.regulatoryBodies = "";
                // vm.contractor.affiliatedAssociations = "";
                bsLoadingOverlayService.start();
                console.log(vm.contractor);
                dataService.saveContractors(vm.contractor).then(function(result) {
                    vm.contactorSuccess = true;
                    bsLoadingOverlayService.stop();
                    $timeout(function() {
                        vm.contactorSuccess = false;
                    }, 5000);
                    vm.updateProgress();
                    console.log(result.data);
                }, function(err) {
                    bsLoadingOverlayService.stop();
                    console.log(err);
                });
                vm.updateProgress();
            }
        };

        vm.submitCategories = function() {
            $scope.checkedNames=[];
            for (var i=0;i<$scope.checked.length;i++)
            {
                $scope.checkedNames.push($scope.checked[i].name);
            }
            vm.contractor = {};
            vm.contractor.keywords = vm.selectedKeywords;
            vm.contractor.keywords = vm.contractor.keywords.concat(vm.selectedProductKeywords);
            if ($scope.checkedNames.length > 0) {
                vm.contractor.regulatoryBodies = $scope.checkedNames.join(',');
                // vm.contractor.regulatoryBodies = vm.companyDetail.arrRegulatoryBodies.join(',');
            } else {
                vm.contractor.regulatoryBodies = '';
            }
            if (vm.companyDetail.affiliatedAssociations) {
                vm.contractor.affiliatedAssociations = vm.companyDetail.affiliatedAssociations.join(',');
            } else {
                vm.contractor.affiliatedAssociations = '';
            }
            if (vm.companyDetail.Facilities) {
                vm.contractor.Facilities = vm.companyDetail.Facilities.join(',');
            } else {
                vm.contractor.Facilities = '';
            }
            vm.contractor.TechnicalSpecs = vm.companyDetail.TechnicalSpecs;
            vm.contractor.Transportation = vm.companyDetail.Transportation;
            vm.contractor.PackagingServices = vm.companyDetail.PackagingServices;
            vm.contractor.MaxCapacity = vm.companyDetail.MaxCapacity;
            vm.contractor.MinCapacity = vm.companyDetail.MinCapacity;
            vm.contractor.contractorId = vm.user.id;
            bsLoadingOverlayService.start();
            console.log(vm.contractor);
            dataService.addCategoryKeywords(vm.contractor).then(function(result) {
                vm.contactorSuccess = true;
                bsLoadingOverlayService.stop();
                $timeout(function() {
                    vm.contactorSuccess = false;
                }, 5000);
                window.scrollTo(0, 0);
                vm.updateProgress();
                console.log(result.data);
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log(err);
            });
        };

        vm.addKeyword = function(item) {
            var isPresent = false;
            angular.forEach(vm.selectedKeywords, function(product){
                if(item.keywordId === product.keywordId){
                    isPresent = true;
                }
            });
            if (isPresent) {
                toastr.warning("Service is already added.");
            } else {
                vm.selectedKeywords.push(item);
            }
            vm.companyDetail.Keyword = '';
        };

        vm.removeKeyword = function(indx) {
            vm.selectedKeywords.splice(indx, 1);
        };

        vm.addProductKeyword = function(item) {
            var isPresent = false;
            angular.forEach(vm.selectedProductKeywords, function(product){
                if(item.keywordId === product.keywordId){
                    isPresent = true;
                }
            });
            if (isPresent) {
                toastr.warning("Product is already added.");
            } else {
                vm.selectedProductKeywords.push(item);
            }
            vm.companyDetail.productKey = '';
            
        };

        vm.addPackageKeyword = function(item) {
         console.log(item);
            var isPresent = false;
            // console.log(vm.selectedPackageKeywords)
            angular.forEach(vm.selectedPackageKeywords, function(product){
                if(item.keywordId === product.keywordId){
                    isPresent = true;
                }
            });
            if (isPresent) {
                toastr.warning("Product is already added.");
            } else {
                vm.selectedPackageKeywords.push(item);
            }
            // console.log(vm.selectedPackageKeywords);
            vm.companyDetail.packages = '';
            
        };

        vm.removeProductKeyword = function(indx) {
            vm.selectedProductKeywords.splice(indx, 1);
        };

        vm.removePackageKeyword = function(indx) {
            vm.selectedPackageKeywords.splice(indx, 1);
        };

        vm.reguBodyChange = function(reguBody, imgName) {
            console.log(reguBody);
            console.log(imgName);
            // if (!vm.companyDetail.arrRegulatoryBodies.includes(imgName)) {
            //     vm.companyDetail.arrRegulatoryBodies.push(imgName);
            // }
            if (!reguBody) {
                vm.companyDetail.arrRegulatoryBodies.push(imgName);
            }
            else {
                var index = vm.companyDetail.arrRegulatoryBodies.indexOf(imgName);
                if (index > -1) {
                    console.log(vm.companyDetail.arrRegulatoryBodies[index]);
                    vm.companyDetail.arrRegulatoryBodies.splice(index, 1);
                }
            }
            console.log(vm.companyDetail.arrRegulatoryBodies);
            console.log(vm.contractor.regulatoryBodies);
        };

        vm.changePackage = function(packageId) {
            vm.objPackage.savedTotalPrice = vm.objPackage.savedPrice * parseInt($filter('filter')(vm.packages, { pId: packageId })[0].days);
        }

        vm.addPackage = function(item) {
            // console.log
            vm.objPackage.keywordId = item.keywordId;
            getKeywordPrice(item.keywordId);
        };

        function getKeywordPrice(keywordId) {
            dataService.getKeywordsPrice(keywordId).then(function(response) {
                vm.objPackage.savedPrice = parseInt(response.data);
                vm.objPackage.savedTotalPrice = vm.objPackage.savedPrice * parseInt($filter('filter')(vm.packages, { pId: vm.objPackage.packageId })[0].days);
            })
        }

        vm.submitPackage = function() {
            vm.objPackage.contractorId = vm.user.id;
            vm.objPackage.days = $filter('filter')(vm.packages, { pId: vm.objPackage.packageId })[0].days;
            vm.objPackage.location = vm.companyDetail.location;
            vm.objPackage.keywords = vm.selectedPackageKeywords;
            bsLoadingOverlayService.start();
            console.log(vm.objPackage);
            dataService.addContractorPackage(vm.objPackage).then(function(result) {
                vm.contactorSuccess = true;
                bsLoadingOverlayService.stop();
                $timeout(function() {
                    vm.contactorSuccess = false;
                }, 5000);
                vm.getTabData();
                vm.updateProgress();
                vm.packageEdit = false;
                vm.objPackage.packageId = vm.packages[0].pId;
                vm.companyDetail.location = vm.locations[0];
                vm.companyDetail.packages = '';
                vm.objPackage.contractorPackageId = 0;
                vm.objPackage.delete = false;
                console.log(result);
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('contractor Package Error', err);
            });
        };

        vm.editPackage = function(pckg) {
            console.log(pckg);
            vm.packageEdit = true;
            vm.objPackage.packageId = pckg.packageId;
            vm.companyDetail.location = pckg.location;
            vm.objPackage.contractorPackageId = pckg.contractorPackageId;
            vm.companyDetail.packages = pckg.keywordName;
            if (pckg.keywordId) {
                getKeywordPrice(pckg.keywordId);
            }
        };

        vm.deletePackage = function(pckg) {
            console.log(pckg);
            vm.objPackage.contractorPackageId = pckg.contractorPackageId;
            vm.objPackage.delete = true;
            vm.submitPackage();
        };

        vm.cancelEdit = function() {
            vm.packageEdit = false;
            vm.objPackage.packageId = vm.packages[0].pId;
            vm.companyDetail.location = vm.locations[0];
            vm.companyDetail.packages = '';
        };

        vm.verifyMobile = function() {
            vm.showOTP = true;

            dataService.checkBalance().then(
                function(resp){
                    console.log(resp);
                    if (resp.status === 200) {

                        vm.verifyMobileClk = true;
                        if(resp.data < 0){
                            toastr.error("SMS API balance end");
                        }
                        else{
                            $scope.myotp = Math.floor(100000 + Math.random() * 9000);
                            console.log($scope.myotp);
                            sendsms($scope.myotp);
                        }

                    } else {
                        toastr.error("Error In Sending OTP");
                    }
                }, function(err) {
                    toastr.error("Error In Sending OTP");
                    console.log('checking balance error : ', err);
                })

            /*dataService.verifyPhone('091' + vm.user.phone, vm.user.accessToken).then(function(resp) {
                if (resp.status === 200) {
                    vm.verifyMobileClk = true;
                    toastr.success("OTP Sent Successful");
                } else {
                    toastr.error("Error In Sending OTP");
                }
            }, function(err) {
                toastr.error("Error In Sending OTP");
                console.log('validate OTP error', err);
            });*/
        };

        function sendsms(OTP) {
            var msg = "Welcome to OUTCHEM: Your One Time Password (OTP) is "+OTP+". Thank You";
            console.log(vm.user.phone);
            dataService.sendSMS(msg,vm.user.phone).then(
                function(resp){
                    if (resp.data.includes("SUBMIT_SUCCESS")) {
                        vm.verifyMobileClk = true;
                        toastr.success("OTP Sent Successful");
                    } else {
                        sendsms(OTP);
                    }
                }, function(err){
                    toastr.error("Error In Sending OTP");
                    console.log('Sending OTP error', err);            
                })
        };

        vm.validOTP = function() {
/*            dataService.verifyPhoneCode('091' + vm.user.phone, vm.OTP, vm.user.accessToken).then(function(resp) {
                if (resp.status === 200) {
                    vm.user.phoneNumberConfirmed = true;
                    toastr.success("OTP Validated Successful");
                } else {
                    toastr.error("OTP Validation error");
                }
            }, function(err) {
                toastr.error("OTP Validation error");
                console.log('OTP Validation error', err);
            });*/
            console.log(vm.OTP);
            if($scope.myotp == vm.OTP)
            {
                vm.showOTP = false;

                vm.user.phoneNumberConfirmed = 1;
                toastr.success("OTP Validated Successful");
                console.log(vm.user);
                dataService.editUser(vm.user).then(function (resp) {
                    if (resp.status === 200) {
                        console.log(resp.data);
                        dataService.getUserInfo(vm.user.id,
                            function(data){
                                console.log(data);
                                vm.user = data;
                            }, function(err){
                                console.log(err);
                            })
                        vm.updateProgress();
                    }
                });
            }
            else{
                vm.showOTP = true;

                toastr.error("OTP Validation error");
            }
        };

    /*    $scope.apiPath = BASE_URL+BASE_URL_API+'buyer/searchBuyer';
    $scope.selectedList = '';*/


        /*$scope.optionsList = [
        {id: 1,  name : "Java"},
        {id: 2,  name : "C"},
        {id: 3,  name : "C++"},
        {id: 4,  name : "AngularJs"},
        {id: 5,  name : "JavaScript"}
        ];*/

        vm.resendOTP = function() {
            console.log('resend');
            $scope.myotp = Math.floor(100000 + Math.random() * 9000); 
            console.log($scope.myotp);
            sendsms($scope.myotp); 
        };

        $scope.$watch('model.profPic', function(newFile) {
            if (angular.isUndefined(newFile) === false && newFile.size <= 2000000) {
                console.log(newFile);
                $scope.tempUrl = URL.createObjectURL(newFile);
                console.log($scope.model.profPic);
                console.log($scope.tempUrl);
                dataService.imageUpload($scope.model.profPic).then(function(resp) {
                    console.log(resp);
                    var imagename = resp.data;
                    var img = imagename.split('.');
                    console.log(img);
                    if (resp.status === 200) {
                        $rootScope.tempUrl = 'assets/images/profiles/'+img[0]+'.jpg';
                        vm.imageData = {
                            image : img[0],
                            userId: vm.user.id
                        };
                        dataService.imageSave(vm.imageData).then(function(resp){

                            if(resp.status == 200){
                                toastr.success("Profile Pic Saved Successful");
                                // $state.reload();

                            }else{
                                toastr.error("Profile Pic Saving Failed");

                            }

                        }) 


                    } else {
                        toastr.error("Profile Pic Uploading Failed");
                    }
                }, function(err) {
                    console.log('Image Upload error', err);
                    toastr.error("Profile Pic Saving Failed");
                });
            } else {
                $scope.tempUrl = '';
                // service call
                console.log('file size is greater than 2mb or else it is not defined')
                // toastr.error('File is undefined or should not exceed 2MB size limit');
            }
        });

        function check(old_array, check_array)
        {
            console.log(old_array);
            console.log(check_array);
            for(var i=0;i<old_array.length;i++)
            {
                for (var j = 0; j < vm.certificates.length;j++)
                {
                    if(old_array[i]==vm.certificates[j].name)
                    {
                        check_array.push(vm.certificates[j]);
                    }
                }
            }
        }

        function sort(checked,all)
        {
            $scope.unchecked = all;
            for(var i=0;i<$scope.checked.length;i++)
            {
                for (var j = 0; j < $scope.unchecked.length;j++)
                {
                    if (checked[i] == $scope.unchecked[j].name)
                    {
                        $scope.unchecked.splice(j,1);
                    }
                }
            }
        }

        vm.uncheckCertificate = function(certi) {
            for (var i = 0; i < $scope.checked.length; i++) {
                if ($scope.checked[i].id == certi.id) {
                    $scope.unchecked.push($scope.checked[i]);
                    $scope.checked.splice(i, 1);
                }
            }
        };

        vm.checkCertificate = function(certi) {
            console.log(certi);
            for (var i = 0; i < $scope.unchecked.length; i++) {
                if ($scope.unchecked[i].id == certi.id) {
                    $scope.checked.push($scope.unchecked[i]);
                    $scope.unchecked.splice(i, 1);
                }
            }
        };

    }

    angular
    .module('app')
    .directive('ngConfirmClick', ngConfirmClick);

    function ngConfirmClick() {
        return {
            link: function(scope, element, attr) {
                var msg = attr.ngConfirmClick;
                var clickAction = attr.confirmedClick;
                element.bind('click', function() {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction);
                    }
                });
            }
        };
    }

    angular
    .module('app')
    .directive('focusMe', focusMe);

    focusMe.$inject = ['$timeout', '$parse'];

    function focusMe($timeout, $parse) {
        return {
            // scope: true,   // optionally create a child scope
            link: function(scope, element, attrs) {
                var model = $parse(attrs.focusMe);
                scope.$watch(model, function(value) {
                    if (value === true) {
                        $timeout(function() {
                            element[0].focus();
                        });
                    }
                });
                // on blur event:
                element.bind('blur', function() {
                    scope.$apply(model.assign(scope, false));
                });
            }
        };
    }

    angular
    .module('app')
    .directive('fileModel', fileModel);

    fileModel.$inject = ['$parse'];

    function fileModel($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function() {
                    scope.$apply(function() {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }
})();

(function () {
  'use strict';
  angular
  .module('app')
  .controller('emailVerificationController', emailVerificationController);

  emailVerificationController.$inject = ['$rootScope', '$state', '$scope', 'dataService', '$timeout', '$location','UserService'];

  function emailVerificationController($rootScope, $state, $scope, dataService, $timeout, $location, UserService) {
    var vm = this;
    vm.shwWarning = false;
    vm.shwExpError = false;
    vm.shwSuccess = false;
    vm.shwError = false;

    console.log('code', $location.search().code);
    console.log('userID', $location.search().userId);
    vm.userId = $location.search().userId;
    dataService.verifyEmail($location.search().userId, $location.search().code).then(function (resp) {
      console.log(resp);
      if (resp) {
        if (resp.status === 200) {
          vm.shwSuccess = true;
          vm.shwWarning = false;
          
          $timeout(function () {
            dataService.getUserInfo(vm.userId,
              function(response){
                console.log(response);
                UserService.setCurrentUser(response);
                $rootScope.$broadcast('authorized');
                if (response.isContractor == 1) {
                  $state.go('contractorProfile');
                } else if(response.isContractor == 0) {
                  $state.go('buyerProfile');
                }
              }, function(data){
                alert('error getting UserInfo')
              })
          }, 5000);
        } else {
          vm.shwError = true;
          vm.shwWarning = false;
        }
      }
      //  if (resp.data.expired) {
      //  vm.shwExpError = true;
      //  vm.shwWarning = false;
      //  vm.newVerifyLink = '';
      //  }
    }, function (err) {
      vm.shwError = true;
      vm.shwWarning = false;
      console.log('Error In getContractor', err);
    });
  }
})();

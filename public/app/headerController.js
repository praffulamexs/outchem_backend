(function() {
    'use strict';

    angular
    .module('app')
    .controller('HeaderController', HeaderController);

    HeaderController.$inject = ['$uibModal', 'UserService', '$rootScope', '$state', 'dataService', 'Utils', '$location', '$anchorScroll', '$document'];

    function HeaderController($uibModal, UserService, $rootScope, $state, dataService, Utils, $location, $anchorScroll, $document) {
        var vm = this;
        vm.data = {};
        activate();

        function activate() {
            var user = UserService.getCurrentUser();
            if (user) {
                vm.data.user = user;
                vm.data.isLoggedIn = true;
                $rootScope.firstName = vm.data.user.firstName;
                $rootScope.lastName = vm.data.user.lastName;
                /*dataService.getLoggedInUser(vm.data.user.accessToken).then(function(resp) {
                    if (resp.status === 200) {
                        Utils.isImage(dataService.getImageUrl() + resp.data + '.jpg').then(function(result) {
                            if (result) {
                                vm.tempUrl = dataService.getImageUrl() + resp.data + '.jpg';
                            }
                        });
                    }
                });*/

                dataService.getUserInfo(vm.data.user.id,
              function(data){
                // console.log(data);
                vm.data.user = data;
                
                Utils.isImage(dataService.getImageUrl() + vm.data.user.image + '.jpg').then(function(result) {
                    if (result) {
                        // vm.tempUrl = dataService.getImageUrl() + vm.data.user.image + '.jpg';
                        $rootScope.tempUrl = dataService.getImageUrl() + vm.data.user.image + '.jpg';
                        // console.log(vm.tempUrl);
                    }
                });
            }, function(err){
                console.log(err);
            })

                /*Utils.isImage(dataService.getImageUrl() + vm.data.user.image + '.jpg').then(function(result) {
                   console.log(result);
                    if (result) {
                        vm.tempUrl = dataService.getImageUrl() + vm.data.user.image + '.jpg';
                    }
                });*/
            }
        }

        vm.trmCondPopUp = function(){
         vm.modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/misc/terms.html'          
        });
     };

     vm.privacyPolPopUp = function(){
         vm.modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/misc/privacy.html'          
        });
     }; 

     vm.openMenu = function($mdMenu, ev) {
        $mdMenu.open(ev);
    };

    vm.clkAbtUs = function() {
        $document.scrollTo(document.getElementById("aboutuscontent"), "", 1000);
    };

    vm.clkHowItWorks = function() {
        $document.scrollTo(document.getElementById("howitworkscontent"), "", 1000);
    };


    $rootScope.$on('authorized', function() {
        activate();
    });

    $rootScope.$on('signOut', function() {
        vm.data.user = null;
        vm.data.isLoggedIn = false;
    });

    vm.login = function() {
        $uibModal.open({
            animation: true,
            templateUrl: '/app/login/loginModal.html',
            controller: "LoginController",
            controllerAs: "loginController"
        });
    };

    vm.signUp = function() {
        $uibModal.open({
            animation: true,
            templateUrl: '/app/signUp/signUpModal.html'
        });
    };

    vm.viewProfile = function() {
        if (vm.data.user.isAdmin == 1) {
            $state.go('adminProfile');
        } else if (vm.data.user.isContractor == 1) {
            $state.go("contractorProfile");
        } else if(vm.data.user.isContractor == 0){
            $state.go("buyerProfile");
        }
    };

    vm.viewDashboard = function() {
        if (vm.data.user.isAdmin == 1) {
            $state.go('adminDashboard');
        } else if (vm.data.user.isContractor == 1) {
            $state.go('companyDashboard');
        }
    }

    vm.goHome = function() {
        $state.go("app");
    };

    vm.viewCompanyDashboard = function() {
        $state.go("companyDashboard");
    }

    vm.viewAdminProfile = function() {
        $state.go("adminProfile");
    }

    vm.changePassword = function () {
        $state.go("changePassword");
    }

    vm.signOut = function() {
        UserService.signOutUser();
        $rootScope.$broadcast('signOut');
        $state.go("app");
    };

        /*vm.showVideo = function() {
            $uibModal.open({
                animation: true,
                size: 'lg',
                templateUrl: '/app/misc/playVideo.html'
            });
        };*/
    }
})();

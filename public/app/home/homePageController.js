(function () {
  'use strict';

  angular
  .module('app')
  .controller('HomePageController', HomePageController);

  HomePageController.$inject = ['$scope', '$state', '$uibModal', 'dataService', '$timeout', 'UserService', 'bsLoadingOverlayService', 'CATEGORY', 'Utils','toastr', '$sce'];


  function HomePageController($scope, $state, $uibModal, dataService, $timeout, UserService, bsLoadingOverlayService, categoryConst, Utils,toastr,$sce) {



    var vm = this;
    vm.showContactUs = true;
    vm.contact = {
      contactId: '00000000-0000-0000-0000-000000000000',
      contactName: '',
      contactEmail: '',
      contactComments: '',
      mobileNo: 0
    };
    vm.stats = {};
    vm.servSlides = [];
    vm.prodSlides = [];
    vm.companies=[];
    vm.contractors;
    vm.buyers;

    dataService.getContractors().then(function (response) {
      if (response.status === 200) {
        vm.contractors = response.data;
      }
      else {
        vm.contractors = [];
      }
    })

    dataService.getBuyers().then(function (response) {
      if (response.status === 200) {
        vm.buyers = response.data;
      }
      else {
        vm.buyers = [];
      }
    })
   /* vm.imgSlides = [{
      image: 'assets/images/homepageslider/homeimage1-new.png',
      id: 1
    },
    {
      image: 'assets/images/homepageslider/homepage2-new.png',
      id: 2
    },
    {
      image: 'assets/images/homepageslider/homepage3-new.png',
      id: 3
    },
    {
      image: 'assets/images/homepageslider/homepage4-new.png',
      id: 4
    }];*/

    dataService.getImageSlider().then(function(response){
      console.log(response.data);
      if(response.status == 200){
        vm.imgSlides = response.data;

      }
      else{
        console.log('error retreiving slider data');
        console.log(response);
      }

    })

    var currentIndex = 0;

    $scope.changeState = function (id) {
      $state.go('viewContractorProfile',{
        contractorId : id
      });
    };

    vm.getCount = function () {
      dataService.getCount().then(function (resp) {
        vm.stats = resp.data;
      }, function (err) {
        console.log('error', err);
      });
    };

    vm.getCompanies = function() {
      dataService.getTopCompanies().then(function (response) {
        if (response.status === 200) {
          console.log(response.data);
          vm.companies=response.data;
        }
        else {
          console.log("Error in loading Top Companies");
        }
      });
    };

 /*   vm.getService = function () {
      dataService.getCategoryByID(categoryConst.service).then(function (resp) {
        vm.serviceData = angular.copy(resp.data);
        console.log(resp.data.splice(0, 5));
        vm.servSlides = createSlides(resp.data.splice(0, 5));
      }, function (err) {
        console.log('error', err);
      });
    };*/

    vm.getService = function () {
      dataService.getTopServices().then(function (resp) {
        vm.serviceData = angular.copy(resp.data);
        vm.servSlides = createSlides(resp.data);
        console.log(vm.servSlides)
      }, function (err) {
        console.log('error', err);
      });
    };



    vm.getProduct = function () {
      dataService.getCategoryByID(categoryConst.product).then(function (resp) {
        vm.productData = angular.copy(resp.data);
        vm.prodSlides = createSlides(resp.data.splice(0, 5));
        console.log(vm.prodSlides);
      }, function (err) {
        console.log('error', err);
      });
    };

    function createSlides(data) {
      var slides = [];
      currentIndex = 0;
      angular.forEach(data, function (_obj) {
        slides.push({
          name: _obj.keywordName,
          description: _obj.keywordDescription,
          id: currentIndex++,
          keywordId: _obj.keywordId
        });
      });
      return slides;
    }

    function activate() {
      bsLoadingOverlayService.start();
      bsLoadingOverlayService.stop();
      vm.submit = submitData;
      vm.getService();
      vm.getCompanies();
      vm.getProduct();
      vm.getCount();
      recordLocation();
    }

    function recordLocation() {
      if(!Utils.isLocationRecorded){
        dataService.getLocationExternal().then(function (response) {
          if (response.status === 200) {
            dataService.setLocation(response.data).then(function (resp) {
              if (resp.status === 200) {
                Utils.setLocationRecorded();
              }
            })
          }
        })
      }
    }

    vm.getKeywords = function (key) {
      return dataService.getKeywordBySearchKey(key).then(function (resp) {
        console.log(resp);
        return resp.data.keywords.map(function (item) {
          return item;
        });
      }, function (err) {
        console.log('error', err);
        return [];
      });
    };

    function submitData() {
      bsLoadingOverlayService.start();
    }

    vm.btnGoSearchClk = function (_srchTxt) {
      if(angular.isUndefined(_srchTxt)){
        console.log('here')
        toastr.warning('Please enter seach text');
      }else{

        $state.go('search', {searchTxt: _srchTxt});
      }
    };

    vm.onSelect = function ($item) {
      vm.btnGoSearchClk($item.keywordId);
   };

   vm.showAllKeywords = function (_type) {
    var _keywords = [];
    if (_type === 'Product') {
      _keywords = vm.productData;
    } else {
      _keywords = vm.serviceData;
    }
    $uibModal.open({
      animation: true,
      templateUrl: '/app/misc/keywordsModal.html',
      resolve: {
        data: function () {
          return {
            type: _type,
            keywords: _keywords
          };
        }
      },
      controller: function ($scope, data) {
        $scope.keywords = data.keywords;
        $scope.type = data.type;
      }
    });
  };

  vm.showUser = function (_type) {
    vm.user = UserService.getCurrentUser();
    if(vm.user)
    {
      var users = [];
      if (_type === 'Contractor') {
        users = vm.contractors;
      } else {
        users = vm.buyers;
      }
      $uibModal.open({
        animation: true,
        templateUrl: '/app/misc/usersModal.html',
        resolve: {
          data: function () {
            return {
              type: _type,
              users: users
            };
          }
        },
        controller: function ($scope, data) {
          $scope.users = data.users;
          $scope.type = data.type;
        }
      });
    }
    else
    {
      $uibModal.open({
        animation: true,
        templateUrl: '/app/login/loginModal.html',
        controller: "LoginController",
        controllerAs: "loginController"
      });
    }
  };

  activate();

    /*vm.validateContact = function () {
      var mobileRgx = /^[0]?[789]\d{9}$/;
      var emailRgx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if (angular.isUndefined(vm.fName) || vm.fName === null || vm.fName === '') {
        vm.fNameError = true;
      } else {
        vm.fNameError = false;
      }
      if (angular.isUndefined(vm.mobile) || vm.mobile === null || vm.mobile === '' || !(mobileRgx.test(vm.mobile))) {
        vm.mobileError = true;
      } else {
        vm.mobileError = false;
      }
      if (angular.isUndefined(vm.email) || vm.email === null || vm.email === '' || !(emailRgx.test(vm.email))) {
        vm.emailError = true;
      } else {
        vm.emailError = false;
      }
      if (!vm.fNameError && !vm.mobileError && !vm.emailError) {
        vm.contact.contactName = vm.fName;
        vm.contact.contactEmail = vm.email;
        vm.contact.mobileNo = vm.mobile;
        vm.contact.contactComments = vm.comment;
        dataService.saveContactUs(vm.contact).then(function (result) {
          vm.contactSuccess = true;
          vm.fName = '';
          vm.email = '';
          vm.mobile = '';
          vm.comment = '';
          $timeout(function () {
            vm.contactSuccess = false;
            vm.showContactUs = false;
          }, 5000);
          console.log(result);
        }, function (err) {
          console.log(err);
        });
      }
    };*/

    $scope.myInterval = 3000;

    dataService.getAllCertificates().then(
      function(response){
        console.log(response.data);
        var certificates = response.data;
        vm.certificates = [];
        var i,j,temparray,chunk = 36;
        for (i=0,j=certificates.length; i<j; i+=chunk) {
          temparray = certificates.slice(i,i+chunk);
          vm.certificates.push(temparray);
        }
        console.log(vm.certificates);
      }, function(err){
        console.log(err);
      });

    $scope.openCertificate = function(){
      $state.go('certificates')
    }


    var video = dataService.getVideo().then(function (response) {
      console.log(response.data);
      if(response.status === 200)
      {
        console.log(response.data);
        $scope.video = response.data;
        if ($scope.video.charAt(0) == "h" && $scope.video.charAt(1) == "t" && $scope.video.charAt(2) == "t" && $scope.video.charAt(3) == "p" && $scope.video.charAt(4) == "s") {
          $scope.video.substr(0);
          $scope.video.substr(1);
          $scope.video.substr(2);
          $scope.video.substr(3);
          $scope.video.substr(4);
          $scope.video.substr(5);
          $scope.video.substr(6);
          $scope.video.substr(7);
          console.log($scope.video);
        }
        if ($scope.video.charAt(0) == "h" && $scope.video.charAt(1) == "t" && $scope.video.charAt(2) == "t" && $scope.video.charAt(3) == "p") {
          $scope.video.substr(0);
          $scope.video.substr(1);
          $scope.video.substr(2);
          $scope.video.substr(3);
          $scope.video.substr(4);
          $scope.video.substr(5);
          $scope.video.substr(6);
          console.log($scope.video);
        }
      }
      else
      {
        $scope.video ="www.youtube.com/embed/4V3uoAu5mXE?rel=0";
      }

      function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
          return match[2];
        } else {
          return 'error';
        }
      }

      // $scope.video = getId('http://www.youtube.com/watch?v=zbYf5_S7oJo');
      $scope.video = getId($scope.video);
      document.getElementById("myCode").innerHTML = '<iframe width="560" height="315" src="//www.youtube.com/embed/' + $scope.video + '" frameborder="0" allowfullscreen></iframe>';
      console.log($scope.video);
      return $scope.video;      
    })
  }
})();

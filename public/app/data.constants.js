(function () {
  'use strict';
  angular
    .module('app')    
    .constant('OFFSETVAL', 20)    
    .constant('CATEGORY', {
      // service: '337269b4-bf38-11e6-8cee-0231a4044d81',
      service: '1',
      // product: 'feb5be67-bf37-11e6-8cee-0231a4044d81'
      product: '2'
    });

})();

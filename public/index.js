angular

.module('app', ['ui.router', 'ui.bootstrap', 'toastr', 'angular-storage', 'bsLoadingOverlay', 'ui.multiselect', 'ngMaterial', 'nvd3', 'daterangepicker', 'ngStorage', 'datamaps', 'duScroll','multipleSelect'])
.controller('MainCtrl', ['$rootScope', '$state', 'UserService', 'dataService', 'toastr', '$sce',
  function ($rootScope, $state, UserService, dataService, toastr, $sce) {
    var main = this;

    $rootScope.$on('authorized', function () {
      main.currentUser = UserService.getCurrentUser();
    });

    $rootScope.$on('unauthorized', function () {
      main.currentUser = UserService.setCurrentUser(null);
    });

    $rootScope.$on('registered', function () {
      toastr.success("Registration Successful");
    });

    $rootScope.$on('registrationFailed', function () {
      toastr.error("Registration Failed");
    });

    $rootScope.trustSrc = function(src)
    {
      return $sce.trustAsResourceUrl(src);
    }

    main.currentUser = UserService.getCurrentUser();
  }
  ])

.run(function (bsLoadingOverlayService) {
  bsLoadingOverlayService.setGlobalConfig({
    templateUrl: 'loading-overlay-template.html'
  });
});

(function () {
  'use strict';

  angular
    .module('app')
    .service('UserService', UserService);

  UserService.$inject = ['store'];

  function UserService(store) {
    var vm = this;
    var currentUser = "";
    vm.getCurrentUser = getCurrentUser;
    vm.setCurrentUser = setCurrentUser;
    vm.signOutUser = signOutUser;

    function getCurrentUser() {
      if (!currentUser) {
        currentUser = store.get('user');
      }      
      return currentUser;
    }

    function setCurrentUser(user) {
      if(user !== null)
      {
        currentUser = user;
        // currentUser.accessToken = accessToken;
        store.set('user', user);
        return currentUser;
      }
      else  
        store.set('user', null);
    }

    function signOutUser() {
      currentUser = null;
      store.remove('user');
    }
  }
})();

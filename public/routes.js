angular
  .module('app')
  .config(routesConfig);

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
  // $locationProvider.html5Mode(true);
  $stateProvider
    .state('app', {
      url: '/',
      templateUrl: '/app/home/homePage1.html',
      controller: 'HomePageController as vm'
    })
    .state('register', {
      url: '/register',
      templateUrl: '/app/login/signupOpt.html'
     // controller: 'LoginController as lg'
    })
    .state('buyerRegistration', {
      url: '/buyerRegistration',
      templateUrl: '/app/buyerRegistration/buyerRegistration.html',
      controller: 'BuyerRegController as vm'
    })
    .state('contractRegistration', {
      url: '/contractRegistration',
      templateUrl: '/app/contractRegistration/contractRegistration.html',
      controller: 'ContractRegController as vm'
    })
    .state('buyerProfile', {
      url: '/buyerProfile',
      templateUrl: '/app/buyerProfile/buyerProfileView.html',
      controller: 'BuyerProfileController as vm'
    })
    .state('contractorProfile', {
      url: '/contractorProfile',
      templateUrl: '/app/contractorProfile/contractorProfileMain.html',
      controller: 'ContractProfController as vm'
    })
    .state('adminDashboard', {
      url: '/adminDashboard',
      templateUrl: '/app/adminDashboard/adminDashboard.html',
      controller: 'AdminDashboardController as adminDashboardController'
    })
    .state('adminProfile', {
      url: '/adminProfile',
      templateUrl: '/app/adminProfile/adminProfile.html',
      controller: 'AdminProfileController as adminProfileController'
    })
    .state('search', {
      url: '/search?searchTxt',
      templateUrl: '/app/searchKeyword/searchKeyword.html',
      controller: 'searchKeywordController as vm'
    })
    .state('emailVerification', {
      url: '/emailVerification',
      templateUrl: '/app/emailVerification/emailVerification.html',
      controller: 'emailVerificationController as vm'
    })
    .state('terms', {
        url: '/terms',
        templateUrl: '/app/misc/terms.html'
    })
    .state('privacy', {
        url: '/privacy',
        templateUrl: '/app/misc/privacy.html'
    })
    .state('forgotPassword', {
      url: '/forgotPassword',
      templateUrl: '/app/resetPassword/resetPassword.html',
      controller: 'ResetPasswordController as vm'
    })
    .state('changePassword', {
      url: '/changePassword',
      templateUrl: '/app/changePassword/changePassword.html',
      controller: 'ChangePasswordController as vm'
    })
    .state('viewContractorProfile', {
      url: '/viewContractorProfile?contractorId',
      params: {
        keywordId: null
      },
      templateUrl: '/app/contractorProfile/viewContractorProfile.html',
      controller: 'viewContractorProfileController as vm'
    })
    .state('companyDashboard', {
      url: '/companyDashboard',
      templateUrl: '/app/companyDashboard/companyDashboard.html',
      controller: 'CompanyDashboardController as companyDashboard'
    })
    .state('contractorRatings',{
      url : '/contractorRatings?contractorId',
      templateUrl : '/app/contractorRatings/contractorRatings.html',
      controller: 'contractorRatingsController as contractorRatings'
    })
      .state('certificates',{
      url : '/certificates',
      templateUrl : '/app/certificates/certificates.html',
      controller: 'certificatesController'
    });
  $urlRouterProvider.otherwise('/');

  $httpProvider.interceptors.push('InterceptorFactory');
}
